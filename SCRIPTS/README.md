# KISSME Data analysis scripts

---

## Mooring processing
 
 - `calc_density_all_moorings.py` : Calculates the mean density profile for all moorings. Produces the file `KISSME_Density_Combined_BestFit.nc`
 - `fit_buoyancy_modes_allmoorings.py` : Fits the modal amplitudes to all of the moorings using the consistent density field from `calc_density_all_moorings.py`. Produces the file `KISSME_Fitted_Buoyancy_wout_motion.nc`
 - `calc_iw_direction_bmodes.py` : Determines IW directionality using lagged correlation of the buoyancy modal amplitudes. Produces csv tables of directions (need cleaning though...)

## Internal wave plotting

 - `plot_iw_amplitude.py` : time series of amplitude w/ two example time periods
 - `plot_iw_conditions.py` : plots the steepening time, etc

 - 
