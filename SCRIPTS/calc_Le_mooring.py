"""
Calculate Le using the mooring data
"""
import mycurrents.oceanmooring as om

from soda.utils.timeseries import timeseries
from soda.utils import harmonic_analysis
from soda.utils.othertime import datetime64todatetime, datetimetodatetime64

from scipy.interpolate import PchipInterpolator
import scipy.linalg as la
from scipy.optimize import newton_krylov

import xray
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
import matplotlib.dates as mdates

import pdb

GRAV = 9.81
RHO0 = 1024.

ncfile = 'Data/NetCDF/KISSME_Gridded_Mooring_T_SBE56.nc'
varname = 'Temperature'
ncgroup = 'SP250'

#t1 = datetime(2017,4,2,4,0,0)
#t2 = datetime(2017,4,5,4,0,0)

#t1 = datetime(2017,4,20,4,0,0)
#t2 = datetime(2017,4,23,4,0,0)

#t1 = datetime(2017,4,29,4,0,0)
#t2 = datetime(2017,5,2,4,0,0)

t1 = datetime(2017,5,5,4,0,0)
t2 = datetime(2017,5,8,4,0,0)


rmswindow = 7*60.
dt = 0.5
outinterval = 60.
#####


#T = om.from_netcdf(ncfile, varname, group=ncgroup)
#
#Tshort = T.clip(t1,t2)
#
#Tshort.running_mean(Tshort.y, Tshort.dt, windowlength=7*60.)

# Use xray (pandas) to do the running average

data = xray.open_dataset(ncfile, group=ncgroup)

# In[45]:

# Convert to my timeseries class
#TS = timeseries(data.time.values, data['temperature'].values)
#trms = TS.running_rms(windowlength=10*60.)

#...needs too much memory

tempall = data['Temperature']
temp = tempall.sel(time=slice(t1,t2))

window = int(rmswindow/dt)
subsample = int(outinterval//dt)

#timeout = temp.time[::subsample]
#nt = timeout.shape[0]
#nz = temp.shape[0]
#Tmean = np.zeros((nt,nz))
#Tmean = np.zeros((nt,nz))

# Remove nan layers
idx = np.where(~np.any(np.isnan(temp.values),axis=1))[0]
temp = temp.sel(depth=temp.depth[idx])



print 'Calculating the mean...'
Tbar = temp.to_pandas().rolling(window, center=True, axis=1).mean()
Tpr = temp - Tbar

print 'Calculating the rms...'
# Standard deviation is equivalent to rms of T'
#Trms = temp.to_pandas().rolling(window, center=True).std(axis=0)
Trms = Tpr.to_pandas().rolling(window, center=True, axis=1).std()

# Put it back into an oceanmooring object

Trms_om = om.OceanMooring(Trms.columns[::subsample],\
        Trms.values[:,::subsample], Trms.index.values)
Tbar_om = om.OceanMooring(Tbar.columns[::subsample],\
        Tbar.values[:,::subsample], Tbar.index.values)

temp_om = om.OceanMooring(temp.time.values,\
        temp.values, temp.depth.values)

# Interpolate the mean temp onto a constant grid, calculate the gradient and interpolate it
# back
F = PchipInterpolator(Tbar_om.Z, Tbar_om.y.data, axis=0)
zout = np.linspace(Tbar_om.Z[0], Tbar_om.Z[-1],50)
Tequal = F(zout)

dTdz_eq = np.gradient(Tequal, zout, axis=0)
F = PchipInterpolator(zout, dTdz_eq, axis=0)
dTdz = F(Tbar_om.Z)

# Calculate the gradient
#dTdz = Tbar_om.grad_z()
Le = Trms_om.y / dTdz
#Le[dTdz<1e-6] = 0.

Le_om = om.OceanMooring(Tbar_om.t, Le, Tbar_om.Z)
dTdz_om = om.OceanMooring(Tbar_om.t, dTdz, Tbar_om.Z)

plt.figure()

Le_om.contourf(np.arange(0, 10, 1.0), cmap='afmhot_r', extend='max')
#Tbar_om.contourf(np.arange(12,32,1), filled=False, cbar=False, colors='k', linewdiths=0.5)
temp_om.contourf(np.arange(12, 32, 1.0), filled=False, cbar=False, colors='0.5', linewdiths=0.5)

plt.figure()
plt.hist(Le.data[Le.mask==False], bins=np.arange(0,20,0.2))

plt.show()
