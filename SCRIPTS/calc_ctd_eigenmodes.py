
# coding: utf-8

# In[1]:

from mycurrents import seabird
import xray
import matplotlib.pyplot as plt
import glob
import numpy as np
from scipy.interpolate import interp1d
import xray

from iwaves.utils.imodes import IWaveModes
from iwaves.utils.fitting import FitDensity

#get_ipython().magic(u'matplotlib inline')


# In[6]:

#sbefiles = glob.glob('../Data/Livewire CTD/BRD200*.cnv')
#outfile = '../Figures/CTD_Station_BRD200_20170331.png'

sbefiles = glob.glob('../KISSME2017BROWSEBASIN/Data/Livewire CTD/SP250_12hr_0*.cnv')
outfile = '../Figures/CTD_Station_SP25012hr_20170403.png'

#sbefiles = glob.glob('../Data/Livewire CTD/UWAB*.cnv')
#outfile = '../Figures/CTD_Station_UWAB_20170404.png'

#sbefiles = glob.glob('../Data/Livewire CTD/UWAB-Transect*.cnv')
#outfile = '../Figures/CTD_Transect_UWAB_20170405.png'

#sbefiles = glob.glob('../Data/Livewire CTD/SP250_12HR_NEAP*.cnv')
#outfile = '../Figures/CTD_Station_SP250_20170406.png'

#sbefiles = glob.glob('../Data/Livewire CTD/UWAD_transect*.cnv')
#outfile = '../Figures/CTD_Transect_UWAD_20170407.png'

print sbefiles

tempvar = 'potemp090C'

#tempvar = 'potemp190C'# Channel 2


# In[3]:

def process_cnv(sbefile):
    sbe = seabird.CnvFile(sbefile, edit=False)

    #data = seabird.parse_seabird_cnv(sbefile)
    data = {}
    for ii, name in enumerate(sbe.names):
        print name
        data.update({name:sbe.array[:,ii]})
    
    data.update({'lon':sbe.lon})
    data.update({'lat':sbe.lat})
    data.update({'time':sbe.datetime})
    
    return data


# In[4]:

ctds = []
for sbefile in sbefiles:
    print sbefile
    sbe = process_cnv(sbefile)
    ctds.append(sbe)


# In[7]:

# Plot the profiles
#offset = 0.
#plt.figure(figsize=(6,8))
#
#for ctd in ctds:
#    plt.plot(ctd[tempvar]+offset, -ctd['depSM'], 'k')
#    offset+=2
#
#plt.show()

# In[6]:

depth = np.arange(-250,0,0.5)
nfiles = len(ctds)
temp = np.zeros((depth.shape[0], nfiles))
salt = np.zeros((depth.shape[0], nfiles))
chla = np.zeros((depth.shape[0], nfiles))

lat = np.zeros((nfiles,))
time = []

for ii, ctd in enumerate(ctds):
    F = interp1d(-ctd['depSM'], ctd[tempvar], bounds_error=False)
    temp[:,ii] = F(depth)
    
    F = interp1d(-ctd['depSM'], ctd['sal00'], bounds_error=False)
    salt[:,ii] = F(depth)
    
    
    lat[ii] = ctd['lat']
    time.append(ctd['time'])
    

Tbar = np.nanmean(temp, axis=1)
Sbar = np.nanmean(salt, axis=1)

idx = ~np.isnan(Tbar)

iw = IWaveModes(Tbar[idx], depth[idx], salt=Sbar[idx], density_class=FitDensity,\
        density_func='double_tanh')
iw(-250, 1.5, 0)

print iw.calc_nonlin_params()

iw.plot_modes();plt.show()

"""


# In[7]:

clevsF = np.arange(0., 2.4, .2)
#clevsS = np.arange(1.8, 4.8, 0.1)
clevsS = np.arange(3.8, 4.8, 0.1)


clevsT = np.arange(11,32,1.)

plt.figure(figsize=(14,14))

plt.subplot(211)
plt.title('Fluorescence')
plt.contourf(time, depth, chla, clevsF, cmap='Greens', extend='max')
#plt.contourf(lat, depth, temp, clevsT, cmap='Spectral_r')
plt.colorbar()
c = plt.contour(time, depth, temp, clevsT, colors='k', linewidths=0.5)
plt.clabel(c, fmt='%3.0f')
#plt.plot(lat, 0*lat-5,'kd')
plt.ylim(-260,0)
plt.plot(time,-5*np.ones((nfiles,)),'kd') # CTD Locations
plt.ylabel('Depth [m]')

plt.subplot(212)
plt.title('Transmissometer')
plt.contourf(time, depth, tran, clevsS, cmap='YlOrBr_r', extend='min')
#plt.contourf(lat, depth, temp, clevsT, cmap='Spectral_r')
plt.colorbar()
plt.plot(time,-5*np.ones((nfiles,)),'kd') # CTD Locations

c = plt.contour(time, depth, temp, clevsT, colors='k', linewidths=0.5)
plt.clabel(c, fmt='%3.0f')
plt.xlabel
plt.ylim(-260,0)
plt.xlabel('Time')
plt.ylabel('Depth [m]')

plt.tight_layout()
#plt.savefig(outfile, dpi=150)


# In[37]:

# Save an average TS profile
temp_bar = temp.mean(axis=1)

plt.figure(figsize=(5,8))
plt.plot(temp_bar, depth)


# In[47]:

xray.DataArray({'temp':temp_bar}, coords={'depth':depth},dims=('depth'))


# In[44]:

temp_bar.shape


# In[ ]:


"""
