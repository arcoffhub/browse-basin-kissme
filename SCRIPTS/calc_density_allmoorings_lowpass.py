"""
Calculate the mean density profile using all of the moorings combined
"""

import mycurrents.oceanmooring as om

from soda.utils.timeseries import timeseries
from soda.utils import harmonic_analysis
from soda.utils.othertime import datetime64todatetime, datetimetodatetime64

from iwaves import IWaveModes
from iwaves.utils.density import FitDensity, InterpDensity
import gsw

from scipy.interpolate import PchipInterpolator
import scipy.linalg as la
from scipy.optimize import newton_krylov

import xray
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
import matplotlib.dates as mdates

import pdb

GRAV = 9.81
RHO0 = 1024.


def get_tmean(T, t1, t2):
    """
    Initialise the modal structure function class based on the mean temperature
    for the period
    Steps are:
    ---
        - Load and clip the data
        - Remove bad layers containing any nans
        - Find the time mean depth of each good instrument
        - Interpolate onto these mean depths


    """
    Ttmp = T.clip(t1,t2)

    # Remove any bad layers
    idx = np.where(~np.any(np.isnan(Ttmp.y.data),axis=1))[0]

    # Remove instruments at the same height (WP250)
    Zu, Zidx = np.unique(Ttmp.Z[idx], return_index=True)
    is_unique = np.zeros(idx.shape, np.bool)
    is_unique[Zidx] = True
    
    #idx = (idx) & (is_unique)
    idx = idx[is_unique]

    Tshort = om.OceanMooring(Ttmp.t, Ttmp.y[idx,:], Ttmp.Z[idx], zvar=Ttmp.zvar[idx,:])

    #Tmean = Tshort.y.mean(axis=1)
    Z = Tshort.zvar.mean(axis=1)

    Tmean=[]
    for zz in Z:
        Tz = Tshort.interp_z(zz)
        Tmean.append(Tz.mean())
        #print zz, Tz.mean()


    return Z, np.array(Tmean)

    ## Calculate the modes
    #iw = IWaveModes(Tmean, Z, salt=34.6*np.ones_like(Tmean),\
    #        density_class=FitDensity, density_func=density_func)

    #return iw

#######

ncfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Gridded_Mooring_TP_60sec_linear.nc'

#outfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Density_Combined_BestFit_ButterFilt.nc'

outfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Density_Combined_BestFit_UnevenFilt.nc'


ncgroups = ['WP250','SP250','NP250']

varname = 'temperature'

#time = [ datetime(2017,4,1,6,0,0)+timedelta(hours=6*ii) for ii in range(4*37)]
#tstart = datetime(2017,4,1,8)
time = [ datetime(2017,4,2,16,0,0)+timedelta(hours=6*ii) for ii in range(4*36)]
tstart = datetime(2017,4,2,6)
tend = datetime(2017,5,7,6)

density_func='double_tanh'

zmin = -250.
Nz = 100

modes = np.arange(2)

########

# Load the data
T1 = om.from_netcdf(ncfile, varname, group=ncgroups[0])
T2 = om.from_netcdf(ncfile, varname, group=ncgroups[1])
T3 = om.from_netcdf(ncfile, varname, group=ncgroups[2])

# NaN out start
def qc_temp(T, fac=5):
    mask = T.y.mask.copy()
    tmean = T.y.mean(axis=1)
    tstd = T.y.std(axis=1)
    idx = (T.y.T > tmean + tstd*fac) & (T.y.T > tmean - tstd*fac) 
    mask[idx.T] = True
    T.y[mask] = np.nan
    T.y = np.ma.MaskedArray(T.y, mask=mask)

#qc_temp(T1)
#qc_temp(T2)
#qc_temp(T3)
    
# Method 2) Stack-Filter-Fill
t1 = time[0]
t2 = time[-1]

#T1c = T1.resample(600.).clip(t1,t2)
#T2c = T2.resample(600.).clip(t1,t2)
#T3c = T3.resample(600.).clip(t1,t2)

T1c = T1.clip(t1,t2)
T2c = T2.clip(t1,t2)
T3c = T3.clip(t1,t2)


# Weave them all back together
Zall = np.hstack([T1c.Z, T2c.Z, T3c.Z])
Tall = np.vstack([T1c.y, T2c.y, T3c.y])

## No WP
#Zall = np.hstack([T2c.Z, T3c.Z])
#Tall = np.vstack([T2c.y, T3c.y])

## No SP
#Zall = np.hstack([T1c.Z, T3c.Z])
#Tall = np.vstack([T1c.y, T3c.y])

## SP only
#Zall = T2c.Z
#Tall = T2c.y

## No NP
#Zall = np.hstack([T1c.Z, T2c.Z])
#Tall = np.vstack([T1c.y, T2c.y])

idx = np.argsort(Zall)[::-1]
Zout = Zall[idx]
Tout = np.ma.MaskedArray(Tall[idx, :], np.isnan(Tall[idx,:]))

T = om.OceanMooring(T1c.t, Tout, Zout)

# Fill in the gaps
Tf = T.fill_gaps_z(kind='linear')
Tf = Tf.clip(tstart, tend)

# Filter
cutoff_t = 34*3600.
#Tlow = om.OceanMooring(Tf.t, Tf.filt(cutoff_t, btype='low', order=5), Tf.Z)
Tlow = om.OceanMooring(Tf.t, Tf.filt_uneven(cutoff_t), Tf.Z)

#
plt.figure()
plt.subplot(211)
Tf.contourf(np.arange(12,32,1))
Tf.contourf(np.arange(12,32,1), linestyles=':' , filled=False, cbar=False)
Tlow.contourf(np.arange(12,32,1), filled=False, cbar=False)


plt.subplot(212)
Tlow.contourf(np.arange(12,32,1));plt.show()
Tlow_x = Tlow.to_xray()

## Method 1) Fill-Filter-Stack
## Fill in the vertical gaps
#T1g = T1.fill_gaps_z()
#T2g = T2.fill_gaps_z()
#T3g = T3.fill_gaps_z()
#
## Filter
#cutoff_t = 34*3600.
#T1f = om.OceanMooring(T1g.t, T1g.filt(cutoff_t, btype='low'), T1g.Z)
#T2f = om.OceanMooring(T2g.t, T2g.filt(cutoff_t, btype='low'), T2g.Z)
#T3f = om.OceanMooring(T3g.t, T3g.filt(cutoff_t, btype='low'), T3g.Z)
#
## Interpolate onto a constant time grid
#tt, y = T1f.interp(time)
#T1i = om.OceanMooring(time, y, T1f.Z)
#tt, y = T2f.interp(time)
#T2i = om.OceanMooring(time, y, T2f.Z)
#tt, y = T3f.interp(time)
#T3i = om.OceanMooring(time, y, T3f.Z)
#
## Weave them all back together
#Zall = np.hstack([T1i.Z, T2i.Z, T3i.Z])
#Tall = np.vstack([T1i.y, T2i.y, T3i.y])
#
#idx = np.argsort(Zall)[::-1]
#Zout = Zall[idx]
#Tout = Tall[idx, :]
#
#T = om.OceanMooring(time, Tout, Zout)


#t1s = [t0 - timedelta(hours=24) for t0 in time]
#t2s = [t0 + timedelta(hours=24) for t0 in time]

# define the dimensions
time = Tlow.t[::360] # 6 hours
Nchunk = len(time)
Nz = Nz

#tstart,tend = T1.get_tslice(time[0]-timedelta(days=1), time[-1]+timedelta(days=1))
#Nt = tend-tstart
Nt = len(time)
Nmode = len(modes)

# Define the coordinates:
coords = {
    'timeslow': datetimetodatetime64(time),
    'modes':np.array(modes),
    'z': np.linspace(zmin,0, Nz)[::-1], # top to bottom
    }

# Create the output variable as xray.DataArray objects
rho_t = xray.DataArray(np.zeros((Nchunk, Nz)),
        dims = ('timeslow','z'),
        coords = {'timeslow':coords['timeslow'], 'z':coords['z']},
        attrs = {'long_name':'Background density',
                'units':'kg m^-3',
                }
       )

N2_t = xray.DataArray(np.zeros((Nchunk, Nz)),
        dims = ('timeslow','z'),
        coords = {'timeslow':coords['timeslow'], 'z':coords['z']},
        attrs = {'long_name':'Backgrouna buoyancy frequency squared',
                'units':'s^-2',
                }
       )

cn_t = xray.DataArray(np.zeros((Nchunk, Nmode)),
        dims = ('timeslow','modes'),
        coords = {'timeslow':coords['timeslow'], 'modes':coords['modes']},
        attrs = {'long_name':'Linear phase speed',
                'units':'m s^-1',
                }
       )

r10_t = xray.DataArray(np.zeros((Nchunk, Nmode)),
        dims = ('timeslow','modes'),
        coords = {'timeslow':coords['timeslow'], 'modes':coords['modes']},
        attrs = {'long_name':'Nonlinear steepening parameter',
                'units':'m^-1',
                }
       )

phi_t = xray.DataArray(np.zeros((Nchunk, Nmode, Nz)),
        dims = ('timeslow','modes','z'),
        coords = {'timeslow':coords['timeslow'], 'modes':coords['modes'], 'z':coords['z']},
        attrs = {'long_name':'Modal structure function',
                'units':'',
                }
       )

ii=-1

for tt in time:

    ii+=1
    Zout = Tlow.Z + zmin
    Tnow = Tlow_x.sel(time=tt).values

    Zout, idx = np.unique(Zout, return_index=True)

    iw = IWaveModes(Tnow[idx], Zout, salt=34.6*np.ones_like(Tnow[idx]),\
        density_class=FitDensity, density_func=density_func,
        #density_class=InterpDensity,
        )

    Z = np.linspace(zmin, 0, Nz)
    dz = np.mean(np.diff(Z))

    c1 = []
    r10 = []
    phi = []

    for mode in modes:
        # Use the mode class to create the profile
        phin, cn, he, znew = iw(zmin, dz, mode)
        rn0, _, _, _ = iw.calc_nonlin_params()

        c1.append(cn)
        r10.append(rn0)
        phi.append(phin)

    print tt, c1[0], r10[0]

    rho_t[ii,:] = iw.rhoZ
    N2_t[ii,:] = iw.N2
    phi_t[ii,:,:] = np.array(phi)

    r10_t[ii,...] = np.array(r10)
    cn_t[ii,...] = np.array(c1)


    ## Compare fitting vs interpolated density
    #plt.figure(figsize=(12,6))
    #plt.subplot(131)
    #plt.plot(iw.N2, iw.Z)
    ##plt.plot(iw2.N2, Z2,'r')
    #plt.xlabel('$N^2$ [s$^{-2}$]')

    #ax = plt.subplot(132)
    #plt.plot(iw.phi, iw.Z)

    #plt.text(0.1,0.1, \
    #        'c1 = %3.2f [m/s]\nr10 = %1.2e [m$^{-1}$]'%(iw.c1,r10[0]),\
    #        transform=ax.transAxes)
    #plt.xlabel('$\phi(z)$')
    #ax.set_yticklabels([])

    #ax = plt.subplot(133)
    #plt.plot(iw.rhoZ, iw.Z)
    ##plt.plot(iw2.rhoZ, Z2 ,'r')
    #plt.plot(iw.rho, iw.z ,'yd')
    #plt.xlabel(r'$\rho(z)$ [kg m$^{-3}$]')
    #ax.set_yticklabels([])
    #plt.show()



ds = xray.Dataset({
        'rhobar':rho_t,
        'N2':N2_t,
        'phi':phi_t,
        'r10':r10_t,
        'cn':cn_t,
},
attrs={
    'ncfile':ncfile,
    #'group':ncgroup,
    'Description':'Linear vertical mode fit to combined KISSME mooring data',
})


ds.to_netcdf(outfile, mode='w', format='NETCDF4')
print 'Done'


