"""
Calculate the mean density profile using all of the moorings combined
"""

import mycurrents.oceanmooring as om

from soda.utils.timeseries import timeseries
from soda.utils import harmonic_analysis
from soda.utils.othertime import datetime64todatetime, datetimetodatetime64

from iwaves import IWaveModes
from iwaves.utils.density import FitDensity, double_tanh_rho
import gsw

from scipy.interpolate import PchipInterpolator
import scipy.linalg as la
from scipy.optimize import newton_krylov

import xray
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
import matplotlib.dates as mdates

import pdb

GRAV = 9.81
RHO0 = 1024.


def get_tmean(T, t1, t2):
    """
    Initialise the modal structure function class based on the mean temperature
    for the period
    Steps are:
    ---
        - Load and clip the data
        - Remove bad layers containing any nans
        - Find the time mean depth of each good instrument
        - Interpolate onto these mean depths


    """
    Ttmp = T.clip(t1,t2)

    # Remove any bad layers
    idx = np.where(~np.any(np.isnan(Ttmp.y.data),axis=1))[0]

    # Remove instruments at the same height (WP250)
    Zu, Zidx = np.unique(Ttmp.Z[idx], return_index=True)
    is_unique = np.zeros(idx.shape, np.bool)
    is_unique[Zidx] = True
    
    #idx = (idx) & (is_unique)
    idx = idx[is_unique]

    Tshort = om.OceanMooring(Ttmp.t, Ttmp.y[idx,:], Ttmp.Z[idx], zvar=Ttmp.zvar[idx,:])

    Tmean = Tshort.y.mean(axis=1)
    Z = Tshort.zvar.mean(axis=1)

    #Tmean=[]
    #for zz in Z:
    #    Tz = Tshort.interp_z(zz)
    #    Tmean.append(Tz.mean())
    #    #print zz, Tz.mean()


    return Z, np.array(Tmean)

    ## Calculate the modes
    #iw = IWaveModes(Tmean, Z, salt=34.6*np.ones_like(Tmean),\
    #        density_class=FitDensity, density_func=density_func)

    #return iw

#######

ncfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Gridded_Mooring_TP_60sec_linear.nc'

outfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Density_Combined_BestFit.nc'

ncgroups = ['WP250','SP250','NP250']

varname = 'temperature'

time = [ datetime(2017,4,3,4,0,0)+timedelta(hours=6*ii) for ii in range(4*36)]
#time = [ datetime(2017,4,9,16,0,0)+timedelta(hours=6*ii) for ii in range(4*36)]

density_func='double_tanh'

zmin = -250.
Nz = 100

modes = np.arange(1)

########

# Load some data
T1 = om.from_netcdf(ncfile, varname, group=ncgroups[0])
T2 = om.from_netcdf(ncfile, varname, group=ncgroups[1])
T3 = om.from_netcdf(ncfile, varname, group=ncgroups[2])

#T1g=T1.godinfilt()
#T2g=T2.godinfilt()
#T3g=T3.godinfilt()
#
#T1g.zvar[:] = T1.zvar[:]
#T2g.zvar[:] = T2.zvar[:]
#T3g.zvar[:] = T3.zvar[:]

t1s = [t0 - timedelta(hours=24) for t0 in time]
t2s = [t0 + timedelta(hours=24) for t0 in time]

#t1s = [t0 - timedelta(hours=6) for t0 in time]
#t2s = [t0 + timedelta(hours=6) for t0 in time]


# define the dimensions
Nchunk = len(time)
Nz = Nz

tstart,tend = T1.get_tslice(time[0]-timedelta(days=1), time[-1]+timedelta(days=1))
Nt = tend-tstart
Nmode = len(modes)


ii=-1
for t1,t2 in zip(t1s, t2s):
    ii+=1

    Z1,Tm1=get_tmean(T1,t1,t2)
    Z2,Tm2=get_tmean(T2,t1,t2)
    Z3,Tm3=get_tmean(T3,t1,t2)

    plt.figure()
    plt.plot(Tm1,Z1,'ro')
    plt.plot(Tm2,Z2,'yo')
    plt.plot(Tm3,Z3,'go')

    #########
    # Different configurations
    # All moorings
    Zall = np.hstack([Z1,Z2,Z3])
    Tall = np.hstack([Tm1,Tm2,Tm3])

    # No WP
    #Zall = np.hstack([Z2,Z3])
    #Tall = np.hstack([Tm2,Tm3])

    # No NP
    #Zall = np.hstack([Z1,Z2])
    #Tall = np.hstack([Tm1,Tm2])

    # No SP
    #Zall = np.hstack([Z1,Z3])
    #Tall = np.hstack([Tm1,Tm3])

    ## SP only
    #Zall = Z2
    #Tall = Tm2

    ## WP only
    #Zall = Z1
    #Tall = Tm1

    ## NP only
    #Zall = Z3
    #Tall = Tm3


    #########




    idx = np.argsort(Zall)[::-1]
    Zout = Zall[idx]
    Tout = Tall[idx]

    iw = IWaveModes(Tout, -Zout, salt=34.6*np.ones_like(Tout),\
            density_class=FitDensity, density_func=density_func)

    # Print out the density paramters
    print iw.Fi.f0

    Z = np.linspace(zmin, 0, Nz)
    dz = np.mean(np.diff(Z))


    phin, cn, he, znew = iw(zmin, dz, 0)
    rn0, _, _, _ = iw.calc_nonlin_params()

    plt.figure(figsize=(12,6))
    iw.plot_modes()
    plt.text(0.1,0.9,'r_10 = %e'%rn0, transform=plt.gca().transAxes)
    plt.show()

    break


"""
### Generate 
N = 100
rho0s = np.random.uniform(low=1021.1, high=1021.4, size=N)
drho1s = np.random.uniform(low=1.1, high=1.8, size=N)
drho2s = np.random.uniform(low=3.0, high=4.3, size=N)
z1s = np.random.uniform(low=50.0, high=70, size=N)
z2s = np.random.uniform(low=160, high=176, size=N)
h1s = np.random.uniform(low=20, high=30, size=N)
h2s = np.random.uniform(low=60, high=85, size=N)

plt.figure()
r1=[]
for params in zip(rho0s, drho1s, drho2s, z1s,z2s, h1s, h2s):
    #print params
    rhonew = double_tanh_rho(iw.Z, *params)
    #iw.rhoZ[:] = rhonew
    iw.Fi.f0 = params
    phin, cn, he, znew = iw(zmin, dz, 0)
    rn0, _, _, _ = iw.calc_nonlin_params()
    print rn0
    r1.append(rn0)

    plt.plot(rhonew, iw.Z,'k',lw=0.2)

plt.show()
"""
