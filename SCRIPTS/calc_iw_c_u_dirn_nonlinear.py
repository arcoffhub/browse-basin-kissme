"""
Calculate the internal wave direction from 
"""

import xray
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy import linalg
from scipy.optimize import leastsq

from datetime import timedelta

from iwaves.utils.minmax import get_peak_window

from mycurrents import oceanmooring as om
import soda.utils.mynumpy as mynp
from soda.utils.harmonic_analysis import harmonic_signal

from soda.utils.myproj import MyProj

import pdb


def get_ubar(dsu, time, tide_angle):
    
    # Extract the barotropic tide
    ubar = dsu.Au_n[:,0].values
    vbar = dsu.Av_n[:,0].values

    ## Rotate along the main axis
    tide_angle *= np.pi/180.
    ## Note this is a clockwise rotation matrix
    ubar_pr = ubar*np.cos(tide_angle) + vbar*np.sin(tide_angle)

    ubar_pr[np.abs(ubar_pr)>10.] = 0.

    U = om.OceanMooring(dsu.time.values, ubar_pr, 0.)

    uamp, uphs, frq, umean, _, _ = U.tidefit(['M2','S2','K1','O1','P1','Q1','N2','K2'])

    return harmonic_signal(time, uamp, uphs, umean, frq)

################
#bfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Fitted_Buoyancy_Combined.nc'
#bfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Fitted_Buoyancy_wout_motion.nc'
#bfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Fitted_Buoyancy_wout_motion_newtanh.nc'
bfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Fitted_Buoyancy_wout_motion_unvenfilt.nc'
#bfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Fitted_Buoyancy.nc'

uvfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Fitted_Velocity.nc'

bgroups = ['SP250','NP250','WP250']

#mode = 0
#r2min = 0.90
#ampmin = 40.

mode = 1
r2min = 0.65
ampmin = 25.

window = 6
#window = 12
#window = 2

plotting = True
####

# Load the dataset objects
dsb0 = xray.open_dataset(bfile, group = bgroups[0])
dsb1 = xray.open_dataset(bfile, group = bgroups[1])
dsb2 = xray.open_dataset(bfile, group = bgroups[2])

# Load the velocity data
dsu = xray.open_dataset(uvfile, group='SP')

####
# Get the lat/lon of the moorings
P = MyProj(None, utmzone=51, isnorth=False)

x_NP, y_NP = P.to_xy(dsb1.X, dsb1.Y)
x_SP, y_SP = P.to_xy(dsb0.X, dsb0.Y)
x_WP, y_WP = P.to_xy(dsb2.X, dsb2.Y)

times = pd.date_range(dsb0.time.values[0],dsb0.time.values[-1],freq='1H').to_pydatetime()
#times = pd.date_range('20170506','20170509',freq='1H').to_pydatetime()
#times = pd.date_range('20170403','20170404',freq='1H').to_pydatetime()

# Step for forward hourly
time = []
amp = []
speed = []
dirn = []
ww = window-1

print('time,fit_err,fit_r2,absamp,fitangle,fitspeed,u0,cn,delv,beta,lambda')

tmaxold = times[0]
tmax = times[-1]
for t1, t2 in zip(times[0:-ww], times[ww:]):

    #t1 = timewave - np.timedelta64(window,'h')
    #t2 = timewave + np.timedelta64(window,'h')
    #
    #t1s = pd.to_datetime(str(t1)).strftime('%Y.%m.%d %H:%M:%S')
    #t2s = pd.to_datetime(str(t2)).strftime('%Y.%m.%d %H:%M:%S')
    t1s = t1.strftime('%Y.%m.%d %H:%M:%S')
    t2s = t2.strftime('%Y.%m.%d %H:%M:%S')

    dsbwave0 = dsb0.sel(time=slice(t1s,t2s))
    dsbwave1 = dsb1.sel(time=slice(t1s,t2s))
    dsbwave2 = dsb2.sel(time=slice(t1s,t2s))

    #######
    # Try the lagged correlation method
    w1 = dsbwave0.A_n[:,mode].to_pandas()
    w2 = dsbwave1.A_n[:,mode].to_pandas()
    w3 = dsbwave2.A_n[:,mode].to_pandas()

    dt = (w1.index[1] - w1.index[0]).total_seconds()


    def crosscorr(datax, datay, lag=0):

        return datax.corr(datay.shift(lag))

    lagmax = 32
    lagtime = range(-lagmax,lagmax)
    tau_12 = np.array([crosscorr(w1, w2, ii) for ii in range(-lagmax,lagmax)])
    tau_23 = np.array([crosscorr(w2, w3, ii) for ii in range(-lagmax,lagmax)])
    tau_13 = np.array([crosscorr(w1, w3, ii) for ii in range(-lagmax,lagmax)])

    # Compute the lag time
    T_12 = (np.argmax(tau_12)-lagmax)*dt
    T_23 = (np.argmax(tau_23)-lagmax)*dt
    T_13 = (np.argmax(tau_13)-lagmax)*dt

    r2 = ((tau_12**2).max()+(tau_23**2).max() +(tau_13**2).max())/3.

    # Compute the geometric parameters
    x1, x2, x3 = x_SP, x_NP, x_WP
    y1, y2, y3 = y_SP, y_NP, y_WP

    ###
    # Least-squares solve
    #Axy = np.array([[x1-x2, x3-x2, x1-x3], [y1-y2, y3-y2, y1-y3]]).T
    Axy = np.array([[x1-x2, x1-x3, x3-x2], [y1-y2, y1-y3, y3-y2]]).T
    #Axy = np.array([[x2-x1, x2-x3, x3-x1], [y2-y1, y2-y3, y3-y1]]).T
    b = np.array([T_12, T_13, T_23])
    #C3 = Axy.T.dot(1/b)
    #err = 0
    C3inv,err,_,_ = np.linalg.lstsq(Axy, b)
    C3 = 1/C3inv

    #### Direct solve
    #Axy = np.array([[x1-x2, x1-x3], [y1-y2, y1-y3]]).T
    #b = np.array([T_12, T_13])

    #C3inv = np.linalg.solve(Axy,b)
    #C3 = 1/C3inv

    #err /= b.max()**2. # normalize the error
    #err /= np.linalg.norm(b**2)

    ### Nonlinear solver
    d12 = np.sqrt( (x2-x1)**2 + (y2-y1)**2)
    d13 = np.sqrt( (x3-x1)**2 + (y3-y1)**2)
    d23 = np.sqrt( (x2-x3)**2 + (y2-y3)**2)
    d = np.array([d12,d13,d23])

    def solve_time(c):
        cx,cy = c
        e1 = (b[0]*cx)**2 + (b[0]*cy)**2 - d[0]**2
        e2 = (b[1]*cx)**2 + (b[1]*cy)**2 - d[0]**2
        e3 = (b[2]*cx)**2 + (b[2]*cy)**2 - d[0]**2
        return np.array([e1,e2,e3])

    # Need to search different initial conditions
    err = np.inf
    C3 = [0,0]
    for cx in [-0.1,0.1]:
        for cy in [-0.1,0.1]:
            C3t,_ = leastsq(solve_time, np.array([0.1,-0.1]))
            errt = np.linalg.norm(solve_time(C3t))/np.linalg.norm(d**2.)
            if errt < err:
                err = errt
                C3 = C3t

    #err = np.linalg.norm((Axy.dot(C3inv) - b)**2.)
    sp = np.abs(C3[0]+1j*C3[1])
    angle = np.angle(C3[0]+1j*C3[1])*180/np.pi

    #print t1,t2
    ampmax = w1.abs().max()

    # Get the maximum time
    tidx = np.argwhere(np.abs (dsbwave0.A_n[:,mode].values) == ampmax)[0,0]
    tmax = dsbwave0.time[tidx].values

    #print '%s, %6.2f, %1.2f, %3.2f, %3.2f, %3.2f'%\
    #    ( t1, err, r2, sp, angle, w1.abs().max())

    if (ampmax > ampmin) and (r2 > r2min) and (err < 0.5) and (tmaxold != tmax):
        #print '%s, %6.2f, %1.2f, %3.2f, %3.2f, %3.2f'%\
        #    ( t1, err, r2, sp, angle, w1.abs().max())

        #print 'T: ' ,b, 'C^-1:', C3inv
        #print C3[:,np.newaxis].dot(b[ np.newaxis,:])-Axy.T

        tmaxold = tmax


        u0 = get_ubar(dsu, np.array([tmax]), angle)[0]

        # Get cn, alpha, cnl
        r10 = dsbwave0.r10.sel(timeslow=tmax,modes=mode, method='nearest')
        r01 = dsbwave0.r01.sel(timeslow=tmax,modes=mode, method='nearest')
        cn = dsbwave0.cn.sel(timeslow=tmax,modes=mode, method='nearest')

        alpha = -2*cn*r10
        beta = -1*r01
        #cnl = cn + alpha*ampmax/3.
        delv = sp - u0 - cn
        lambda_x = 2*np.pi*np.sqrt(beta/delv)

        print('%s, %3.2f, %1.2f, %3.2f, %3.2f, %3.2f, %3.2f, %3.2f, %3.2f, %3.2f, %3.2f'%\
            ( tmax.astype(str)[0:-10], err, r2, w1.abs().max(),angle,sp, 
              u0, cn, delv, beta, lambda_x) )


        # Only update the values that meet the criteria
        time.append(t1)
        speed.append(sp)
        dirn.append(angle)
        amp.append(ampmax)

        if plotting:
            plt.figure(figsize=(8,7))
            ax1 = plt.subplot(211)
            dsbwave0.A_n[:,mode].plot(color='y')
            dsbwave1.A_n[:,mode].plot(color='g')
            dsbwave2.A_n[:,mode].plot(color='r')
            plt.legend(('SP','NP','WP'))
            plt.ylim(-70,70)

            plt.subplot(212)
            plt.plot(lagtime, tau_12)
            plt.plot(lagtime, tau_23)
            plt.plot(lagtime, tau_13)
            plt.ylabel(r'r($\tau$)')
            plt.xlabel(r'$\tau$ [min]')
            plt.legend(('SP-NP','NP-WP','SP-WP'), loc='lower left')
            plt.ylim(0.5,1)
            plt.text(-30,0.82,\
                    'c = %1.2f m/s\nr2 = %2.2f\ntheta = %3.1f $\circ$'%(sp, r2, angle))

            plt.tight_layout()
            plt.show()


#plt.figure(figsize=(8,7))
#plt.subplot(211)
#plt.scatter(amp, dirn, c=speed, cmap='Spectral_r',vmin=0,vmax=2)
#cb = plt.colorbar()
#cb.ax.set_title('$c_{nl}$ [m s$^{-1}$]')
#plt.xlabel('Mode - %d Amp [m]'%(mode+1))
#plt.ylabel('Direction [$\circ$ CW from E]')
#plt.ylim(-180,180)
#
#plt.subplot(212)
#plt.scatter(speed, dirn, c=amp, cmap='Reds',vmin=0,vmax=60)
#plt.xlim(0,2)
#cb = plt.colorbar()
#cb.ax.set_title('Amp [m]')
#plt.xlabel('$c_{nl}$ [m s$^{-1}$]')
#plt.ylabel('Direction [$\circ$ CW from E]')
#plt.ylim(-180,180)
#
#plt.tight_layout()
#
#plt.show()


# Calculate the normal component 
#d12 = np.sqrt( (x2-x1)**2 + (y2-y1)**2)
#d13 = np.sqrt( (x3-x1)**2 + (y3-y1)**2)
#d23 = np.sqrt( (x2-x3)**2 + (y2-y3)**2)
#n12_x = (x2 - x1) / d12
#n13_x = (x3 - x1) / d13
#n23_x = (x3 - x2) / d23
#
#n12_y = (y2 - y1) / d12
#n13_y = (y3 - y1) / d13
#n23_y = (y3 - y2) / d23
#
#####
## Construct matrices
## Normal directions
#A = np.array([[n12_x, n12_y], [n23_x, n23_y], [n13_x, n13_y]])
## Phase speeds between each mooring
#b = np.array([d12/T_12, d23/T_23, d13/T_13]) 
#
#C = linalg.lstsq(A,b)
#
#A = np.array([[n12_x, n12_y], [n23_x, n23_y]])
## Phase speeds between each mooring
#b = np.array([d12/T_12, d23/T_23]) 
#
#C2 = np.linalg.solve(A,b)


#plt.figure()
#
#ax1 = plt.subplot(311)
#dsbwave0.A_n[:,mode].plot(color='y')
#dsbwave1.A_n[:,mode].plot(color='g')
#dsbwave2.A_n[:,mode].plot(color='r')
#
#plt.subplot(312, sharex=ax1)
#plt.plot(dsbwave0.time, eta_mag)        
#
#plt.subplot(313, sharex=ax1)
#plt.plot(dsbwave0.time, theta_p2)        
#
#plt.show()



######
# Testing
"""
## Find the peak mode-one waves of depression
mode = 0
wavefunc = 'min'
window=3

peaks, tpeakmin, ctr = get_peak_window(dsb0.A_n[:,mode].values, dsb0.time.values,\
                360, 15, fun=wavefunc, ctr=0, vals=[], idxs=[])

peak_min = dsb0.A_n.sel(time=tpeakmin)[:,mode].values


wavenum = 1

######
# Isotherm direction
print x_NP, x_SP, x_WP
print y_NP, y_SP, y_WP

A_SP = dsbwave0.A_n[:,mode].values
A_NP = dsbwave1.A_n[:,mode].values
A_WP = dsbwave2.A_n[:,mode].values

ones = np.ones_like(A_NP)

eta_x, eta_y = gradplane(x_SP*ones, x_NP*ones, x_WP*ones,\
        y_SP*ones, y_NP*ones, y_WP*ones,\
        A_SP, A_NP, A_WP)
eta_mag = np.abs(eta_x+1j*eta_y)
# Calculate the dirction
rad2deg = 180/np.pi
theta_p2 = np.arctan2(eta_y, eta_x) * rad2deg
#theta_deg = pol2compass(theta_p2)
#theta_p2 = np.angle(eta_x + 1j*eta_y) *rad2deg


"""
