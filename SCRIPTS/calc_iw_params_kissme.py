"""
Plot the vertical profiles of bouyancy frequency and eigenfunction shapes
"""

import numpy as np
from scipy import linalg
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt
import operator
from scipy.optimize import leastsq
import pandas as pd

import gsw

import mycurrents.oceanmooring as om
# Use all of the internal wave functions from here:
#import make_scenario_realN2_batch as isw
from iwaves import IWaveModes
from iwaves.utils.density import FitDensity

import pdb


def load_temp(time):
    """
    Load buoyancy frequency profile from previously saved temperature data
    """
    ###########
    # Input variables
    #ncfile = '/home/suntans/Share/ARCHub/DATA/FIELD/ShellPreludeRPS/\
    #    NetCDF/Prelude_lowfreq_strat.nc'
    #ncfile = '/home/mrayson/scratch/SUNTANS/SolitonNWS/DATA/Prelude_lowfreq_strat.nc'
    ncdir = 'Data/NetCDF'
    ncfile = '%s/KISSME_lowfreq_strat.nc'%ncdir

    Hmax = -250

    ncgroups = [\
        'SP250',\
    ]
    varname = 'Tm'

    T = om.from_netcdf(ncfile, varname, group=ncgroups[0]).to_xray()

    my_rho = T.sel(time=time)[:,0]
    rho_out= my_rho.values
    Z = T.depth.values
    idx = np.isfinite(rho_out)

    return rho_out[idx], Hmax + Z[idx]

###############
# Idealised models
###########
tstarts = pd.date_range('2017-04-03 00:00:00','2017-05-03 00:00:00', freq='6H')
#tstarts = pd.date_range('2008-01-01','2008-02-01', freq='1D')

zmax = -250
dz = 5
mode = 0
density_func='single_tanh'
#############
fill = -999999
if density_func=='double_tanh':
    print 'time,rho0,drho1,drho2,z1,z2,h1,h2,r10,r01,r20,h_e,c1'
else:
    print 'time,rho0,drho1,z1,h1,r10,r01,r20,h_e,c1'

for tstart in tstarts:
    time = tstart.strftime('%Y-%m-%d')
     
    try:
        T, z = load_temp(time)

        iw = IWaveModes(T, z, 34.6*np.ones_like(T), 
            density_class=FitDensity, density_func=density_func)

        iw(zmax, dz, 0)
        params =  iw.calc_nonlin_params()

        ## For testing
        #plt.figure()
        #iw.plot_modes()
        #plt.show()
        
        # Create output string
        outstr = '%s, '%time
        if density_func=='double_tanh':
            outstr += 7*'%3.2f, '%tuple(iw.Fi.f0) # double tanh
        else:
            outstr += 4*'%3.2f, '%tuple(iw.Fi.f0)

        outstr += '%3.2e, %3.2f, %3.2e, %3.2f'%params
        outstr += ', %3.2f'%iw.c1
        print outstr
    except:
        if density_func=='double_tanh':
            print '%s, '%time, 11*'%6.0f,'%(11*(fill,)), '%6.0f'%fill
        else:
            print '%s, '%time, 8*'%6.0f,'%(8*(fill,)), '%6.0f'%fill

