"""
Calculate the low-frequency temperature and buoyancy frequency climatology 
from the Prelude mooring data
"""

import mycurrents.oceanmooring as om

import xray
import numpy as np
import matplotlib.pyplot as plt

###########
# Input variables
ncdir = 'Data/NetCDF'
ncfile = '%s/KISSME_Gridded_Mooring_TP_60sec.nc'%ncdir

ncgroups = [\
    'SP250',\
    'NP250',\
    'WP250',\
]

varname = 'temperature'

alpha = -2e-4 # coefficient of expansivity
grav = 9.81

k = -1

clevs = np.arange(6,34,3)
clevsfine = np.arange(6,33,1.)

zout = np.arange(-250, 10, 10)

outfile = '%s/KISSME_lowfreq_strat.nc'%ncdir
mode = 'w'
############


for ncgroup in ncgroups:
    # Load the data as an ocean mooring object
    T = om.from_netcdf(ncfile, varname, group=ncgroup)

    Tlow = T.godinfilt()
    Tfilt = Tlow.resample(3*3600.)
    Tfilt.is2Dz = False
    Tfilt.positive='up'
    #Tfilt = om.OceanMooring(Tlow.t, Tlow.y, Tlow.Z, **Tlow.get_meta())
    #break
    #Tlow = T.copy()
    ### Find the trend in the data
    #trend = T.find_trend()


    ### Lowpass filter the data
    ### Remove the Trend before filtering
    #Tlow.y -= trend

    #Tlow.y[np.isnan(Tlow.y)] = 0.
    ###Tlow.y = Tlow.filt(14*24*3600., btype='low', order=3)
    #yfilt = Tlow.filt(34*3600., btype='low', order=2)
    ##Tlow.godinfilt() # Inplace operation
    ## Add the trend
    #yfilt += trend

    ## Preserve the old mask
    #yfilt = np.ma.MaskedArray(yfilt, mask=T.y.mask)
    #
    #Tfilt = om.OceanMooring(Tlow.t, yfilt, Tlow.Z, **Tlow.get_meta())

    # Output to a new ocean mooring object
    

    ## Use xray to compute the monthly means
    #Tx = Tlow.to_xray()
    #Tmean = om.from_xray( Tx.resample('D', dim='time', how='mean') )
    #
    ## Interpolate the values on a contant vertical grid (testing)
    #Tmean_z = Tmean.interp_z(zout)
    #Tmean_z = om.OceanMooring(Tmean.t, Tmean_z, zout,\
    #    long_name='Lowpass filtered water temperature', units='degC',\
    #    X=T.X,\
    #    Y=T.Y,\
    #)

    # Compute N2
    #dT_dz = Tmean.grad_z()
    ##dT_dz = Tmean_z.grad_z()
    #N2 = -grav*alpha*dT_dz
    #N2[N2<0.] = 0.

    #N = om.OceanMooring(Tmean.t, np.sqrt(N2), Tmean.Z,\
    #    long_name='Buoyancy frequency', units='s-1',\
    #    X=T.X,\
    #    Y=T.Y,\
    #)
    #N = om.OceanMooring(Tmean_z.t, np.sqrt(N2), Tmean_z.Z)

    # Plot the first row
    #ax= plt.subplot(211)
    #plt.plot(T.t, T.y[k,:],'k',linewidth=0.3)
    #plt.plot(Tlow.t, Tlow.y[k,:],'r',linewidth=2)

    #plt.subplot(212, sharex=ax)
    #plt.plot(Tlow.t, N2[k,:],'b',linewidth=1.3)

    ## pcolor the low frequency temp

    plt.figure( figsize=(12,6))
    plt.subplot(111)
    #plt.contourf(Tlow.t, Tlow.y, Tlow.y.T, clevs, cmap='Spectral_r')
    #C = Tlow.contourf(clevs, cbar=False)
    C, cb = Tfilt.contourf(clevsfine, cbar=False, filled=True)

    #C = Tmean.contourf(clevs, cbar=False)
    #Tmean.contourf(clevsfine, cbar=False, filled=False)

    #clevsN = np.arange(0, 0.02, 0.001)
    #C,cb = N.contourf(clevsN, cbar=False , cmap='RdYlBu_r')
    #Tmean.contourf(clevsfine, cbar=False, filled=False)

    # Save N and Tmean to netcdf files
    ds = xray.Dataset({\
        'Tm':Tfilt.to_xray(),\
    },
    attrs = {\
        'Description':'KISMME Godin filtered low frequency water temp',\
        },
    )
    N2group = '%s'%ncgroup
    ds.to_netcdf(outfile, mode=mode, group=N2group)
    print 'Data written to group: %s'%N2group
    mode='a'


    plt.colorbar(C)
    plt.show()
