"""
Plot fitted buoyancy modes
"""

import xray
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime
from scipy.signal import hilbert
import matplotlib.dates as mdates

from iwaves.utils.minmax import get_peak_window

from mycurrents import oceanmooring as om
omega = 1.42e-4

####
#ncfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Fitted_Buoyancy.nc'
#ncfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Fitted_Buoyancy_Combined.nc'
ncfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Fitted_Buoyancy_wout_motion.nc'
#ncfile = '/home/suntans/Share/ARCHub/DATA/FIELD/ShellCrux/KP150_Fitted_Buoyancy.nc'
#
uvfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Fitted_Velocity.nc'

ncgroups = [\
    #'KP150_T',
    #'WP250',
    #'NP250',
    'SP250',
]


#ncfile = '/home/suntans/Share/ARCHub/DATA/FIELD/ShellPreludeRPS/Prelude_Fitted_Buoyancy.nc'
#
#ncgroups = [\
#    'F_Block_2007_CM04',\
#    'F_Block_2008a_CM04',\
#    'F_Block_2008b_CM04',\
#    'F_Block_2009a_CM04',\
#    'F_Block_2009b_CM04',\
#]

xlims = [datetime(2017,4,1), datetime(2017,5,8)]
xlims1 = [datetime(2017,4,2,12,0,0), datetime(2017,4,4,12,0,0)]
xlims2 = [datetime(2017,4,29,12,0,0), datetime(2017,5,1,12,0,0)]

ylims = [-70,70]

#####
RHO0 = 1024.

for ncgroup in ncgroups:
    ds = xray.open_dataset(ncfile, group=ncgroup)
    dsu = xray.open_dataset(uvfile, group=ncgroup[0:2])

    ###
    # APE
    # Interpolate the N2 and modal functions onto all time steps
    N2 = ds.N2.sel(timeslow=ds.time, method='nearest')
    phi = ds.phi.sel(timeslow=ds.time, method='nearest')

    Jn = np.trapz(phi**2 * N2, -ds.z, axis=-1)

    # Calculate the APE
    APE_n = 0.5*RHO0 * ds.A_n**2 * Jn

    # Interpolate the N2 and modal functions onto all time steps
    N2u = ds.N2.sel(timeslow=dsu.time, method='nearest')
    phiu = ds.phi.sel(timeslow=dsu.time, method='nearest')
    cu = ds.cn.sel(timeslow=dsu.time, method='nearest')
    dz = -np.diff(ds.z).mean()
    phiz = np.gradient(phiu, dz, axis=-1)
    Ju = np.trapz(phiz**2, -ds.z, axis=-1)
    Jw = np.trapz(phiu**2, -ds.z, axis=-1)

    KE_n = 0.5*RHO0 *\
        (dsu.Au_n.values[:,1:5]**2*Ju +\
        dsu.Av_n.values[:,1:5]**2*Ju + \
        dsu.Aw_n.values[:,1:5]**2*Jw)\
        *cu.values**2

 
plt.figure()
plt.plot(ds.time,APE_n[:,0],'r',lw=0.5)
plt.plot(dsu.time,KE_n[:,0],lw=0.5)
plt.ylim(0,1e5) 

plt.show()
