"""
Calculate the sheared profile modes
"""
import mycurrents.oceanmooring as om

from soda.utils.timeseries import timeseries
from soda.utils import harmonic_analysis
from soda.utils.othertime import datetime64todatetime, datetimetodatetime64

from iwaves import IWaveModes
from iwaves.utils.density import FitDensity
from iwaves.utils.tgsolve import tgsolve
from iwaves.utils import isw
import gsw

from scipy.interpolate import PchipInterpolator

import xray
import numpy as np
import numpy.polynomial.chebyshev as cheb
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
import matplotlib.dates as mdates
from scipy.interpolate import interp1d

import pdb

def return_uvw(t1,t2, veldepths, U1, U2, V1, V2, W1, W2):
    # Interpolate the velocity data onto a constant depth-array
    U1tmp = U1.clip(t1, t2)
    V1tmp = V1.clip(t1, t2)
    W1tmp = W1.clip(t1, t2)

    if U2 is not None:
        U2tmp = U2.clip(t1, t2)
        V2tmp = V2.clip(t1, t2)
        W2tmp = W2.clip(t1, t2)

    z1 = U1tmp.zvar.max(axis=1)
    if U2 is not None:
        z2 = U2tmp.zvar.max(axis=1)

    Uz = None
    Wz = None
    for zz in veldepths:
        # Check if the adcp is in the first or second depth profile
        if  zz >= z1.min() and zz <= z1.max():
            #print zz, group1
            uzz = U1tmp.interp_z(zz)
            vzz = V1tmp.interp_z(zz)
            wzz = W1tmp.interp_z(zz)
        elif U2 is None:
            #print zz, 'No DATA'
            uzz = np.zeros((U1tmp.Nt,))
            vzz = np.zeros((V1tmp.Nt,))
            wzz = np.zeros((W1tmp.Nt,))
            uzz[:] = np.nan
            vzz[:] = np.nan
            wzz[:] = np.nan
        elif  zz >= z2.min() and zz <= z2.max():
            #print zz, group2
            uzz = U2tmp.interp_z(zz)
            vzz = V2tmp.interp_z(zz)
            wzz = W2tmp.interp_z(zz)
        else:
            #print zz, 'No DATA'
            uzz = np.zeros((U1tmp.Nt,))
            vzz = np.zeros((V1tmp.Nt,))
            wzz = np.zeros((W1tmp.Nt,))
            uzz[:] = np.nan
            vzz[:] = np.nan
            wzz[:] = np.nan

        if Uz is None:
            Uz = om.OceanMooring(U1tmp.t, uzz, zz)
            Vz = om.OceanMooring(V1tmp.t, vzz, zz)
            Wz = om.OceanMooring(W1tmp.t, wzz, zz)
        else:
            Uz.vstack( om.OceanMooring(U1tmp.t, uzz[:,np.newaxis], -zz) )
            Vz.vstack( om.OceanMooring(V1tmp.t, vzz[:,np.newaxis], -zz) )
            Wz.vstack( om.OceanMooring(W1tmp.t, wzz[:,np.newaxis], -zz) )

    return Uz, Vz, Wz


def extract_uvw_kissme(site):
    """
    Extract TWC velocity profile at all sites for a given time period
    """
    adcpfile = 'Data/NetCDF/KISSME2017_RDI_QAQC.nc'

    #zmin = -250.
    #Nz = 100

    # Depths for interpolating the velocity field
    # Note that velocity is stored in pressure units (positive down from surface)
    #veldepths = np.arange(0.,-zmin,4.)

    if site=='WP':
        group1 = 'WP250_RDI_150_kHz_Quartermaster_11795'
        group2 = 'WP250_RDI_300_kHz_Sentinel_20092'
        Tgroup = 'WP250'
    elif site=='SP':
        group1 = 'SP250_RDI75_kHz_Longranger_24613'
        group2 = None
        Tgroup = 'SP250'
    elif site=='NP':
        group1 = 'NP250_RDI_150_kHz_Quartermaster_16856'
        group2 = 'NP250_RDI_300_kHz_Monitor_20089'
        Tgroup = 'NP250'

    ######
    U1 = om.from_netcdf(adcpfile, 'u', group=group1)
    V1 = om.from_netcdf(adcpfile, 'v', group=group1)
    W1 = om.from_netcdf(adcpfile, 'w', group=group1)
    if group2 is not None:
        U2 = om.from_netcdf(adcpfile, 'u', group=group2)
        V2 = om.from_netcdf(adcpfile, 'v', group=group2)
        W2 = om.from_netcdf(adcpfile, 'w', group=group2)

    else:
        U2 = None
        V2 = None
        W2 = None


    return U1, V1, W1, U2, V2, W2

def interp_z_uvw(U1,V1,W1,U2,V2,W2, t1,t2, veldepths):
    ## Fill in the gaps
    #U1 = U1.fill_gaps_z()
    #V1 = V1.fill_gaps_z()
    #W1 = W1.fill_gaps_z()
    #if group2 is not None:
    #    U2 = U2.fill_gaps_z()
    #    V2 = V2.fill_gaps_z()
    #    W2 = W2.fill_gaps_z()

    # Interpolate the velocity data onto a constant depth-array
    U1tmp = U1.clip(t1, t2)
    V1tmp = V1.clip(t1, t2)
    W1tmp = W1.clip(t1, t2)

    if U2 is not None:
        U2tmp = U2.clip(t1, t2)
        V2tmp = V2.clip(t1, t2)
        W2tmp = W2.clip(t1, t2)


    z1 = U1tmp.zvar.max(axis=1)
    if U2 is not None:
        z2 = U2tmp.zvar.max(axis=1)
    else:
        z2 = np.array([0.,0.])

    # Work out whether we should use the first or second instrument
    g1idx = np.zeros_like(veldepths).astype(np.bool)
    g2idx = np.zeros_like(veldepths).astype(np.bool)

    for ii,zz in enumerate(veldepths):
        if  zz >= z2.min() and zz <= z2.max():
            #print( zz, group2)
            g2idx[ii] = True
        elif  zz >= z1.min() and zz <= z1.max():
            #print( zz, group1)
            g1idx[ii] = True

    # New interpolation method
    u1 = U1tmp.interp_z(veldepths, method='nearest')
    u1[~g1idx,:] = 0
    v1 = V1tmp.interp_z(veldepths, method='nearest')
    v1[~g1idx,:] = 0
    w1 = W1tmp.interp_z(veldepths, method='nearest')
    w1[~g1idx,:] = 0

    if U2 is not None:
        u2 = U2tmp.interp_z(veldepths, method='nearest')
        u2[~g2idx,:] = 0
        v2 = V2tmp.interp_z(veldepths, method='nearest')
        v2[~g2idx,:] = 0
        w2 = W2tmp.interp_z(veldepths, method='nearest')
        w2[~g2idx,:] = 0
    else:
        u2 = 0.
        v2 = 0.
        w2 = 0.

    Uz = om.OceanMooring(U1tmp.t[:], u1[:]+u2, -veldepths)
    Vz = om.OceanMooring(V1tmp.t[:], v1[:]+v2, -veldepths)
    Wz = om.OceanMooring(W1tmp.t[:], w1[:]+w2, -veldepths)

    return Uz, Vz, Wz

    ## Old interpolation method

    #Uz = None
    #Wz = None
    #for zz in veldepths:
    #    # Check if the adcp is in the first or second depth profile
    #    if  zz >= z1.min() and zz <= z1.max():
    #        #print zz, group1
    #        uzz = U1tmp.interp_z(zz)
    #        vzz = V1tmp.interp_z(zz)
    #        wzz = W1tmp.interp_z(zz)
    #    elif group2 is None:
    #        #print zz, 'No DATA'
    #        uzz = np.zeros((U1tmp.Nt,))
    #        vzz = np.zeros((V1tmp.Nt,))
    #        wzz = np.zeros((W1tmp.Nt,))
    #        uzz[:] = np.nan
    #        vzz[:] = np.nan
    #        wzz[:] = np.nan
    #    elif  zz >= z2.min() and zz <= z2.max():
    #        #print zz, group2
    #        uzz = U2tmp.interp_z(zz)
    #        vzz = V2tmp.interp_z(zz)
    #        wzz = W2tmp.interp_z(zz)
    #    else:
    #        #print zz, 'No DATA'
    #        uzz = np.zeros((U1tmp.Nt,))
    #        vzz = np.zeros((V1tmp.Nt,))
    #        wzz = np.zeros((W1tmp.Nt,))
    #        uzz[:] = np.nan
    #        vzz[:] = np.nan
    #        wzz[:] = np.nan

    #    if Uz is None:
    #        Uz = om.OceanMooring(U1tmp.t, uzz, zz)
    #        Vz = om.OceanMooring(V1tmp.t, vzz, zz)
    #        Wz = om.OceanMooring(W1tmp.t, wzz, zz)
    #    else:
    #        Uz.vstack( om.OceanMooring(U1tmp.t, uzz[:,np.newaxis], -zz) )
    #        Vz.vstack( om.OceanMooring(V1tmp.t, vzz[:,np.newaxis], -zz) )
    #        Wz.vstack( om.OceanMooring(W1tmp.t, wzz[:,np.newaxis], -zz) )

    #return Uz, Vz, Wz



def get_iwmodes(T, t1, t2, density_func):
    """
    Initialise the modal structure function class based on the mean temperature
    for the period
    """
    Ttmp = T.clip(t1,t2)

    # Remove any bad layers
    idx = np.where(~np.any(np.isnan(Ttmp.y.data),axis=1))[0]

    # Remove instruments at the same height (WP250)
    Zu, Zidx = np.unique(Ttmp.Z[idx], return_index=True)
    is_unique = np.zeros(idx.shape, np.bool)
    is_unique[Zidx] = True
    
    #idx = (idx) & (is_unique)
    idx = idx[is_unique]

    Tshort = om.OceanMooring(Ttmp.t, Ttmp.y[idx,:], Ttmp.Z[idx], zvar=Ttmp.zvar[idx,:])

    Tmean = Tshort.y.mean(axis=1)
    Z = -Tshort.zvar.mean(axis=1)

    # Calculate the modes
    iw = IWaveModes(Tmean, Z, salt=34.6*np.ones_like(Tmean),\
            density_class=FitDensity, density_func=density_func)

    return iw




###########
# Input variables
#bfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Density_Combined_BestFit.nc'
bfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Density_Combined_BestFit_UnevenFilt.nc'

modes = range(2)
#wave_angle = -50.
#wave_angle = -40.
wave_angle = -60.

outfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Shear_Modes_Angle%02d.nc'%(np.abs(wave_angle))


# Wave rotation angle
theta = wave_angle*np.pi/180

zmin = -250.
Nz = 100

# Depths for interpolating the velocity field
# Note that velocity is stored in pressure units (positive down from surface)
#veldepths = np.arange(0.,-zmin,4.)

veldepths = np.arange(30.,-zmin,4.)

density_func='double_tanh'

time = [ datetime(2017,4,3,0,0,0)+timedelta(hours=6*ii) for ii in range(4*36)]
#time = [ datetime(2017,4,3,4,0,0)+timedelta(hours=6*ii) for ii in range(4*2)]
#time = [ datetime(2017,4,3,4,0,0)+timedelta(days=ii) for ii in range(36)]
#time = [ datetime(2017,4,2,20,0,0)+timedelta(hours=4*ii) for ii in range(36)]
###########


# Load the buoyancy data
dsb = xray.open_dataset(bfile)
#veldepths = -dsb.z.values

### create the time windows
t1s = [t0 - timedelta(hours=24) for t0 in time]
t2s = [t0 + timedelta(hours=24) for t0 in time]

###
# Low frequency current extraction (Use NP250 as it has the best coverage)
U1,V1,W1, U2,V2,W2 = extract_uvw_kissme('SP')

def process_u(U1, t1, t2, veldepths):
    # Clip
    U1aa = U1.clip(t1,t2)

    # Fill gaps
    U1a = U1aa.fill_gaps_z(kind='nearest')

    # Interpolate
    U1b = om.OceanMooring(U1a.t, U1a.interp_z(veldepths[::-1], method='linear'),-veldepths[::-1])

    # Fill gaps
    #U1c = U1b.fill_gaps_z(kind='linear')

    # Filter
    Uf = om.OceanMooring(U1b.t, U1b.filt_uneven(72*3600.), U1b.Z)

    # Use a chebychev to smooth
    #Uf = Uf_raw.fill_gaps_z_cheby(order=3)
    return Uf

Uf = process_u(U1, t1s[0], t2s[-2], veldepths)
Vf = process_u(V1, t1s[0], t2s[-2], veldepths)
#Uf.contourf(np.arange(-0.3,0.325,0.025), cmap='RdBu');plt.show()

# Old Method
#U,V,W = interp_z_uvw(U1,V1,W1,U2,V2,W2, t1s[0], t2s[-2], veldepths)
#Uf = om.OceanMooring(U.t, U.filt(30*3600., btype='low'), U.Z)
#Vf = om.OceanMooring(U.t, V.filt(30*3600., btype='low'), V.Z)

# Rotate the velocity
Upr = om.OceanMooring(Uf.t, (Uf.y*np.cos(theta) + Vf.y*np.sin(theta)), Uf.Z)
Vpr = om.OceanMooring(Uf.t, (-Uf.y*np.sin(theta) + Vf.y*np.cos(theta)), Uf.Z)

Upr.contourf(np.arange(-0.3,0.325,0.025), cmap='RdBu');plt.show()
dsu = Upr.to_xray()

######
# Create the output arrays
# Define the coordinates:
Nchunk = len(t1s)
Nz = dsb.z.shape[0]
Nmode= len(modes)

coords = {
    'timeslow': datetimetodatetime64(t1s),
    'modes':np.array(modes),
    'z': dsb.z.values, # top to bottom
    }

N2_t = xray.DataArray(np.zeros((Nchunk, Nz)),
        dims = ('timeslow','z'),
        coords = {'timeslow':coords['timeslow'], 'z':coords['z']},
        attrs = {'long_name':'Background buoyancy frequency squared',
            'units':'s^-2',
        }
       )

U_t = xray.DataArray(np.zeros((Nchunk, Nz)),
        dims = ('timeslow','z'),
        coords = {'timeslow':coords['timeslow'], 'z':coords['z']},
        attrs = {'long_name':'Background velocity in wave direction',
            'units':'m s^-1',
        }
       )

phi_t = xray.DataArray(np.zeros((Nchunk, Nz, Nmode)),
        dims = ('timeslow','z','modes'),
        coords = {'timeslow':coords['timeslow'], 'z':coords['z'],\
               'modes':coords['modes'] },
        attrs = {'long_name':'Modal eigenfunction',
            'units':'',
        }
       )

phi_s_t = xray.DataArray(np.zeros((Nchunk, Nz, Nmode)),
        dims = ('timeslow','z','modes'),
        coords = {'timeslow':coords['timeslow'], 'z':coords['z'],\
               'modes':coords['modes'] },
        attrs = {'long_name':'Modal eigenfunction w shear',
            'units':'',
        }
       )

cn_t = xray.DataArray(np.zeros((Nchunk, Nmode)),
        dims = ('timeslow','modes'),
        coords = {'timeslow':coords['timeslow'], 'modes':coords['modes']},
        attrs = {'long_name':'Linear phase speed',
                'units':'m s^-1',
                }
       )

cn_s_t = xray.DataArray(np.zeros((Nchunk, Nmode)),
        dims = ('timeslow','modes'),
        coords = {'timeslow':coords['timeslow'], 'modes':coords['modes']},
        attrs = {'long_name':'Linear phase speed with shear',
                'units':'m s^-1',
                }
       )

alpha_t = xray.DataArray(np.zeros((Nchunk, Nmode)),
        dims = ('timeslow','modes'),
        coords = {'timeslow':coords['timeslow'], 'modes':coords['modes']},
        attrs = {'long_name':'Nonlinear steepening parameter',
                'units':'s^-1',
                }
       )

alpha_s_t = xray.DataArray(np.zeros((Nchunk, Nmode)),
        dims = ('timeslow','modes'),
        coords = {'timeslow':coords['timeslow'], 'modes':coords['modes']},
        attrs = {'long_name':'Nonlinear steepening parameter with shear',
                'units':'s^-1',
                }
       )

ii=-1
# Start of the for loop here...
for t1 in t1s:
    ii+=1

    # Get N2 and Uz at the present time
    N2 = dsb['N2'].sel(timeslow=t1, method='nearest')
    U = dsu.sel(time=t1, method='nearest')

    dz = np.diff(N2.z)[0]

    # Map the velocity data onto the N2 depths
    order = 4
    cx = cheb.chebfit(U.depth.values, U.values, order)
    Ufit = cheb.chebval(N2.z.values, cx)
    #F = interp1d(U.depth.values, U.values, fill_values='extrapolate')
    #Ufit = F(N2.z.values)

    #u = U.values[::-1]
    u = Ufit

    for mode in modes:

        phi, cn, status = tgsolve(N2.z.values, u*0., N2.values, mode)
        try:
            phi_s, cn_s, status = tgsolve(N2.z.values, u, N2.values, mode)
            alpha_s = isw.calc_alpha_wshear(phi_s, cn_s, u, dz)
            
        except:
            status = -1

        if status < 0:
            print( 'Solver failed to converge... Returning NaNs')
            alpha_s = np.nan
            cn_s = np.nan
            phi_s = np.zeros_like(phi)
            phi_s[:] = np.nan

        # Alpha without shear
        alpha = isw.calc_alpha_wshear(phi, cn, u*0.0, dz)

        print( t1, mode, cn, cn_s, alpha, alpha_s)

        alpha_s_t[ii,mode] = alpha_s
        alpha_t[ii,mode] = alpha
        cn_s_t[ii,mode] = cn_s
        cn_t[ii,mode] = cn

        phi_t[ii,:,mode] = phi
        phi_s_t[ii,:,mode] = phi_s

        plotting =False
        if plotting:
            plt.figure()
            plt.subplot(131)
            plt.plot(N2.values,N2.z)
            plt.subplot(132)
            #plt.plot(U.values[::-1], U.Z)
            plt.plot(U.values, U.depth.values)
            plt.plot(Ufit,N2.z)
     
            plt.subplot(133)
            plt.plot(phi, N2.z, 'b')
            plt.plot(phi_s, N2.z, 'r')
            plt.show()


    N2_t[ii,:] = N2.values
    U_t[ii,:] = u


ds = xray.Dataset({
        'N2':N2_t,
        'U':U_t,
        'phi':phi_t,
        'phi_s':phi_s_t,
        'cn_s':cn_s_t,
        'cn':cn_t,
        'alpha_s':alpha_s_t,
        'alpha':alpha_t,
    },
    attrs={
        'ncfile':bfile,
        'Description':'Linear vertical modes with shear from KISSME mooring data',
    })


print( 'Writing to file: %s...'%outfile)
ds.to_netcdf(outfile, mode='w', format='NETCDF4')
print( 'Done')

 
