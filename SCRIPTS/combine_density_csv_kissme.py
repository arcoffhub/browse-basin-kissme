"""
Combine the density low-pass csv files into one dataset
"""
import mooring_utils
import pandas as pd
import xarray as xr
import matplotlib.pyplot as plt
import numpy as np

from mycurrents import oceanmooring as om

###
csvfile = 'KISSME2017_Groups.csv'
densityfilestr = 'Data/NetCDF/KISSME2017_Density_%s.csv'
###

ds = pd.read_csv(csvfile, index_col='group')

sites = ds.index.values.tolist()

## Use the oceanmooring object to stack
#densitycsv = densityfilestr%sites[0]
#
#dsrho = mooring_utils.read_density_csv(densitycsv)
#
#T = om.OceanMooring(dsrho.time.values, dsrho.values, dsrho.depth.values)
#for site in sites[1:]:
#    print(site)
#    densitycsv = densityfilestr%site
#    dsrho = mooring_utils.read_density_csv(densitycsv)
#
#    for ii,zz in enumerate(dsrho.depth):
#         tmp = om.OceanMooring(dsrho.time.values, dsrho.values[:,ii], dsrho.depth.values[ii])
#         T=T.vstack(tmp)

ds = []
for site in sites:
    print(site)
    densitycsv = densityfilestr%site

    dsrho = xr.Dataset({'rho':mooring_utils.read_density_csv(densitycsv)})
    ds.append(dsrho)
    

dsall = xr.concat(ds, dim='depth',)
dsall = dsall.sortby(dsall.depth)

#plt.figure()
#dsall.plot.contourf(np.arange(12,31,1),cmap='Spectral_r')
#plt.show()
#
