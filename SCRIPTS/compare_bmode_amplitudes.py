"""
Compare the modal amplitude calculation methods
"""

import xray
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime
from scipy.signal import hilbert
import matplotlib.dates as mdates
from iwaves.utils.minmax import get_peak_window
from mycurrents import oceanmooring as om

####
ncfile1 = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Fitted_Buoyancy.nc'
ncfile2 = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Fitted_Buoyancy_Combined.nc'
ncfile3 = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Fitted_Buoyancy_wout_motion.nc'

ncgroups = [\
    #'KP150_T',
    #'WP250',
    #'NP250',
    'SP250',
]
ncgroup = ncgroups[0]

ds1 = xray.open_dataset(ncfile1, group=ncgroup)
ds2 = xray.open_dataset(ncfile2, group=ncgroup)
ds3 = xray.open_dataset(ncfile3, group=ncgroup)

# Plot the mode one amplitude
#ds.A_n[:,0].plot(color='b', lw=0.2)
plt.figure()
#ds1.Atide[:,0].plot(ls='-', color='b', lw=0.7)
#ds2.Atide[:,0].plot(ls='-', color='r', lw=0.7)
#ds3.Atide[:,0].plot(ls='-', color='k', lw=0.7)

ds1.A_n[:,1].plot(ls='-', color='b', lw=0.3)
ds2.A_n[:,1].plot(ls='-', color='r', lw=0.3)
ds3.A_n[:,1].plot(ls='-', color='k', lw=0.3)



plt.show()
