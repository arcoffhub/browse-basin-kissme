##

###

import mycurrents.oceanmooring as om

from soda.utils.timeseries import timeseries
from soda.utils import harmonic_analysis
from soda.utils.othertime import datetime64todatetime, datetimetodatetime64

from iwaves import IWaveModes
from iwaves.utils.density import FitDensity
import gsw

from scipy.interpolate import PchipInterpolator
import scipy.linalg as la
from scipy.optimize import newton_krylov

import xray
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
import matplotlib.dates as mdates

import pdb

GRAV = 9.81
RHO0 = 1024.

def calc_buoyancy_h99(B, phi_1, c1, N2, dN2_dz, r10, T10, nonlinear=True):
    """
    Use the Holloway et al 99 version of the eqn's
    """
    #dN2_dz = np.gradient(N2, -np.abs(dz_s))
    
    # Linear term
    b = B[:,np.newaxis] * phi_1 * N2
    
    # nonlinear terms
    if nonlinear:
        alpha = -2*c1*r10
        b -= alpha/(2*c1)*B[:,np.newaxis]*phi_1*N2
        b -= 0.5*dN2_dz*B[:,np.newaxis]**2. * phi_1**2.
        b += c1*B[:,np.newaxis]**2. *N2 * T10
        
    return b


def fit_bmodes_linear(rho, rhoz, z,  zmin, modes,\
        Nz=100, density_func='single_tanh', full_output=True):
    """
    Compute the linear modal amplitude to the mode numbers in the list

    Inputs:
    ---
        rho - matrix[nz, nt], density data
        rhoz - vector[nz], background density profile from bottom to top (desceding)
        z - vector[nz], depth from bottom to top, negative values (ascending)
        modes - list[nmodes], mode numbers in python index i.e. 0=1

    Returns
    ---
        A_t - matrix[nmodes, nt], modal amplitude
        phi - matrix[nmodes, nz], modal structure functions
        rhofit - matrix[nz, nt], best fit density profile

    """

    nz, nt = rho.shape
    nmodes = len(modes)

    # Compute buoyancy from density and backgroud density
    rhopr = rho.T - rhoz[np.newaxis,...]
    b = GRAV*rhopr/RHO0

    # Compute the modal structures
    L = np.zeros((nz,nmodes))
    phi_n = []

    # Calculate dz
    Z = np.linspace(zmin, 0, Nz)
    dz = np.mean(np.diff(Z))
    c1 = []
    r10 = []

    for ii, mode in enumerate(modes):
        # Use the mode class to create the profile
        iw = IWaveModes(rhoz, z,\
                density_class=FitDensity, density_func=density_func)
        phi, cn, he, znew = iw(zmin, dz, mode)
        rn0, _, _, _ = iw.calc_nonlin_params()

        c1.append(cn)
        r10.append(rn0)

        if full_output:
            if ii==0:
                Nz = iw.Z.size
                Lout = np.zeros((Nz, nmodes))
            Lout[:,ii] = phi * iw.N2
            phi_n.append(phi)

        ## Interpolate the modal shape and N2 onto the measured depth locations
        F = PchipInterpolator(iw.Z[::-1], iw.phi[::-1])
        my_phi = F(z)

        F = PchipInterpolator(iw.Z[::-1], iw.N2[::-1])
        my_N2 = F(z)

        L[:,ii] = my_phi*my_N2
        #phi_n.append(my_phi)


    ## Fit Ax=b
    A_t,_,_,_ = la.lstsq(L , b.T)

    # Reconstruct the density field
    bfit_n = L[:,np.newaxis,:]*A_t.T[np.newaxis,...]
    bfit = bfit_n.sum(axis=-1) # sum the modes

    rhoprfit = bfit.T*RHO0/GRAV
    rhofit = rhoprfit + rhoz[np.newaxis,:]

    if full_output:
        bfit_n = Lout[:,np.newaxis,:]*A_t.T[np.newaxis,...]
        bfit = bfit_n.sum(axis=-1) # sum the modes
        rhoprfit = bfit.T*RHO0/GRAV
        rhofit_full = rhoprfit + iw.rhoZ[np.newaxis,:]
        return A_t, np.array(phi_n).T, rhofit, rhofit_full, iw, r10, c1
    else:
        return A_t, np.array(phi_n).T, rhofit, r10, c1


def fit_bmodes_nonlinear(rho, rhoz, z, mode, dz=2.5, density_func='single_tanh'):
    """
    Compute the nonlinear modal amplitude to the mode numbers in the list
    

    Inputs:
    ---
        rho - matrix[nz, nt], density data
        rhoz - vector[nz], background density profile from bottom to top (desceding)
        z - vector[nz], depth from bottom to top, negative values (ascending)
        modes - list[nmodes], mode numbers in python index i.e. 0=1

    Returns
    ---
        A_t - matrix[nmodes, nt], modal amplitude
        rhofit - matrix[nz, nt], best fit density profile
        iw - internal wave mode class

    """
    nz, nt = rho.shape
    nmodes = len(modes)

    # Compute buoyancy from density and backgroud density
    rhopr = rho.T - rhoz[np.newaxis,...]
    b = GRAV*rhopr/RHO0


    # Use the mode class to create the profile
    iw = IWaveModes(rhoz, z,\
        density_class=FitDensity, density_func=density_func)
    phi, c1, he, znew = iw(z.min(), dz, mode)

    r10, _, _, _ = iw.calc_nonlin_params()
    _, _, _, T10, _, _, _ = iw.calc_nonlin_structure()
    
    # Interpolate the structure etc
    F = PchipInterpolator(iw.Z[::-1], iw.phi[::-1])
    my_phi = F(z)

    F = PchipInterpolator(iw.Z[::-1], iw.N2[::-1])
    my_N2 = F(z)

    # Interpolate the higher order structure function
    F = PchipInterpolator(iw.Z[::-1], T10[::-1])
    my_T10 = F(Z)
    
    # Calculate the gradient of N2 and then interpolate it...
    dN2_dz = np.gradient(iw.N2, -iw.dz)
    F = PchipInterpolator(iw.Z[::-1], dN2_dz[::-1])
    my_dN2 = F(Z)
    
    ## Fit alpha as well
    #alpha = -0.008*0
    #my_T10 = my_T10s + alpha*my_phi
    
    def minfun(x0):
        
        btest = calc_buoyancy_h99(x0, my_phi, iw.c1, my_N2,\
                my_dN2, r10, my_T10, nonlinear=False)
    
        err = np.sum( (b-btest)**2., axis=1)
        #print err.max(), alpha
        return err
    
    # Use the Newton-Krylov solver that works on large nonlinear problems
    A_nl = newton_krylov(minfun, np.zeros((nt,)), f_tol=3.1e-5,\
            method='gmres', rdiff=1e-6, iter=100)
    
    # Reconstruct the nonlinear density field
    bfit_nl = calc_buoyancy_h99(A_nl, my_phi, iw.c1, my_N2,\
                my_dN2, r10, my_T10, nonlinear=False)
    
    rhoprfit = bfit_nl*RHO0/GRAV
    rhofitnl = rhoprfit + rhobar[np.newaxis,:]

    return A_nl, rhofitnl, iw
 

def fit_modes(site, outfile, write_mode):
    ###########
    # Input variables
    #ncfile = '/home/suntans/Share/ARCHub/DATA/FIELD/ShellPreludeRPS/NetCDF/Prelude_Gridded_Tuv.nc'
    ncfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Gridded_Mooring_TP_60sec.nc'

    #outfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Fitted_Buoyancy.nc'
    #write_mode = None # No write

    #ncgroups = [\
    #    'WP250',
    #    'NP250',
    #    'SP250',
    #]
    #ncgroup = ncgroups[2]
    ncgroup = site

    if site=='WP250':
        modes = range(5) # WP250
    else:
        modes = range(10)

    varname = 'temperature'


    #t0 = datetime(2017,4,3,4,0,0) # Waves of depression
    #t0 = datetime(2017,4,4,0,0,0)
    #t0 = datetime(2017,4,11,8,0,0)
    #t0 = datetime(2017,4,27,8,0,0) # Waves of elevation
    #t0 = datetime(2017,5,6,8,0,0) # Large Mode-2

    # Load some data
    T = om.from_netcdf(ncfile, varname, group=ncgroup)

    freqs = ['M2','M4','M6']

    plotting = False
    plotpath = 'FIGURES/BuoyancyModes/KISSME'

    zmin = -250.
    Nz = 100

    density_func='double_tanh'

    time = [ datetime(2017,4,3,4,0,0)+timedelta(hours=6*ii) for ii in range(4*36)]
    #time = [ datetime(2017,4,3,4,0,0)+timedelta(hours=ii) for ii in range(36)]
    ###########

    # create the time windows
    t1s = [t0 - timedelta(hours=24) for t0 in time]
    t2s = [t0 + timedelta(hours=24) for t0 in time]

    # define the dimensions
    Nchunk = len(time)
    Nz = Nz
    Nfreq = len(freqs)
    freqvals, frqnames = harmonic_analysis.getTideFreq(freqs)

    tstart,tend = T.get_tslice(time[0]-timedelta(days=1), time[-1]+timedelta(days=1))
    Nt = tend-tstart
    Nmode = len(modes)

    # Define the coordinates:
    coords = {
        'time':T.t[tstart:tend],
        'timeslow': datetimetodatetime64(time),
        'modes':np.array(modes),
        'z': np.linspace(zmin,0, Nz)[::-1], # top to bottom
        'freqs':freqvals,
        }

    # Create the output variable as xray.DataArray objects
    A_nt = xray.DataArray(np.zeros((Nt, Nmode)),
            dims = ('time','modes'),
            coords = {'time':coords['time'], 'modes':coords['modes']},
            attrs = {'long_name':'Modal buoyancy amplitude',
                    'units':'m',
                    }
           )

    rhofit_t = xray.DataArray(np.zeros((Nt, Nz)),
            dims = ('time','z'),
            coords = {'time':coords['time'], 'z':coords['z']},
            attrs = {'long_name':'Best-fit density',
                    'units':'kg m^-3',
                    }
           )

    Atide = xray.DataArray(np.zeros((Nt, Nmode)),
            dims = ('time','modes'),
            coords = {'time':coords['time'],'modes':coords['modes']},
            attrs = {'long_name':'Tidal fit amplitude',
                    'units':'m',
                    }
           )

    rho_t = xray.DataArray(np.zeros((Nchunk, Nz)),
            dims = ('timeslow','z'),
            coords = {'timeslow':coords['timeslow'], 'z':coords['z']},
            attrs = {'long_name':'Background density',
                    'units':'kg m^-3',
                    }
           )

    N2_t = xray.DataArray(np.zeros((Nchunk, Nz)),
            dims = ('timeslow','z'),
            coords = {'timeslow':coords['timeslow'], 'z':coords['z']},
            attrs = {'long_name':'Backgrouna buoyancy frequency squared',
                    'units':'s^-2',
                    }
           )

    cn_t = xray.DataArray(np.zeros((Nchunk, Nmode)),
            dims = ('timeslow','modes'),
            coords = {'timeslow':coords['timeslow'], 'modes':coords['modes']},
            attrs = {'long_name':'Linear phase speed',
                    'units':'m s^-1',
                    }
           )

    r10_t = xray.DataArray(np.zeros((Nchunk, Nmode)),
            dims = ('timeslow','modes'),
            coords = {'timeslow':coords['timeslow'], 'modes':coords['modes']},
            attrs = {'long_name':'Nonlinear steepening parameter',
                    'units':'m^-1',
                    }
           )

    phi_t = xray.DataArray(np.zeros((Nchunk, Nmode, Nz)),
            dims = ('timeslow','modes','z'),
            coords = {'timeslow':coords['timeslow'], 'modes':coords['modes'], 'z':coords['z']},
            attrs = {'long_name':'Modal structure function',
                    'units':'',
                    }
           )

    amp_t = xray.DataArray(np.zeros((Nchunk,Nmode, Nfreq)),
            dims = ('timeslow', 'modes', 'freqs'),
            coords = {'timeslow':coords['timeslow'],\
                    'modes':coords['modes'],\
                    'freqs':coords['freqs']},
            attrs = {'long_name':'Mode-one harmonic amplitude',
                    'units':'kg m^-3',
                    }
           )

    phs_t = xray.DataArray(np.zeros((Nchunk,Nmode, Nfreq)),
            dims = ('timeslow', 'modes', 'freqs'),
            coords = {'timeslow':coords['timeslow'],\
                    'modes':coords['modes'],\
                    'freqs':coords['freqs']},
            attrs = {'long_name':'Mode-one harmonic phase',
                    'units':'radians',
                    }
           )


    # Initialise some output arrays

    #for t0 in time:
    #    t1 = t0 - timedelta(hours=24)
    #    t2 = t0 + timedelta(hours=24)
    ii=-1
    for t1, t2 in zip(t1s, t2s):
        print t1
        ii+=1

        # Clip the data
        Ttmp = T.clip(t1,t2)

        # Get the index for inserting the new time series in the output array
        ti1,ti2 = T.get_tslice(t1, t2)
        if ii == 0:
            tstart = ti1
        ti1 -= tstart
        ti2 -= tstart
        print ti1, ti2

        # Remove any bad layers
        idx = np.where(~np.any(np.isnan(Ttmp.y.data),axis=1))[0]

        # Remove instruments at the same height (WP250)
        Zu, Zidx = np.unique(Ttmp.Z[idx], return_index=True)
        is_unique = np.zeros(idx.shape, np.bool)
        is_unique[Zidx] = True
        
        #idx = (idx) & (is_unique)
        idx = idx[is_unique]

        Tshort = om.OceanMooring(Ttmp.t, Ttmp.y[idx,:], Ttmp.Z[idx], zvar=Ttmp.zvar[idx,:])

        Tmean = Tshort.y.mean(axis=1)
        Z = -Tshort.zvar.mean(axis=1)

        # Calculate the modes
        #iw = IWaveModes(Tmean, Z, salt=34.6*np.ones_like(Tmean),\
        #        density_class=FitDensity, density_func='single_tanh')
        #phi, c1, he, z = iw(-253, 2.5, mode)
        #
        #iw.plot_modes()
        #plt.show()

        ## Calculate the density time series
        Temp = Tshort.y
        #Z = Tshort.Z
        nz,nt = Temp.shape
        S = 34.6*np.ones((nz,nt))
        P = -Z[...,np.newaxis]*np.ones((1,nt))

        rho = gsw.pot_rho_t_exact(S, Temp, P)
        rho = rho.data
        rhobar = rho.mean(axis=1)

        
        A_t, phi, rhofit, rhofitall, iw, r10, c1 = fit_bmodes_linear(rho, rhobar, Z,\
            zmin, modes, Nz=Nz, density_func=density_func)
        #A_t, phi, rhofit = fit_bmodes_linear(rho, rhobar, Z, modes,\
        #    full_output=False)

        # Nonlinear fit
        #Anl, rhofitnl, iw = fit_bmodes_nonlinear(rho, rhobar, Z, 0)

        # Compute harmonics of A (mode-one only)
        #An = om.OceanMooring(Tshort.t, A_t[0,:].squeeze(), 0.)
        An = om.OceanMooring(Tshort.t, A_t, np.arange(Nmode))
        #freqs = ['M2']
        amp, phs, frq, mean, yfit, yrms = An.tidefit(frqnames=freqs,\
                basetime=datetime(2017,1,1))

        print 'amp: ', amp
        print 'phs diff: ',phs-phs[0]

        # Convert to an oceanmooring object for plotting
        rho_om = om.OceanMooring(Tshort.t, rho, Z)
        #rhofit_om = om.OceanMooring(Tshort.t, rhofit, Z)
        rhofit_om = om.OceanMooring(Tshort.t, rhofitall, iw.Z)
        #rhofitnl_om = om.OceanMooring(Tshort.t, rhofitnl, Z)

        # Update the DataArray objects
        A_nt[ti1:ti2,:] = A_t.T
        rhofit_t[ti1:ti2,:] = rhofitall
        Atide[ti1:ti2,:] = yfit
        rho_t[ii,:] = iw.rhoZ
        N2_t[ii,:] = iw.N2
        phi_t[ii,:,:] = phi.T

        r10_t[ii,...] = r10
        cn_t[ii,...] = c1

        amp_t[ii,...] = amp
        phs_t[ii,...] = phs

        ####
        #
        if plotting:
            fig = plt.figure(figsize=(10,7))
            ax=plt.subplot(311)
            rho_om.contourf(clevs=np.arange(1020, 1028.,0.5), cbar=False)
            rho_om.contourf(clevs=np.arange(1020, 1028.,0.5), filled=False, cbar=False)
            plt.ylabel('Depth [m]')
            ax.set_xlim(t1,t2)
            ax.set_xticklabels([])

            ax1= plt.subplot(312, sharex=ax)
            rhofit_om.contourf(clevs=np.arange(1020, 1028.,0.5), cbar=False)
            rhofit_om.contourf(clevs=np.arange(1020, 1028.,0.5), filled=False, cbar=False)
            plt.ylabel('Depth [m]')
            ax.set_xlim(t1,t2)
            ax1.set_xticklabels([])

            ax2= plt.subplot(313,)
            plt.plot(Tshort.t, A_t[0,:], 'b', lw=0.5)
            plt.plot(Tshort.t, A_t[1,:], 'r', lw=0.5)

            plt.plot(An.t, yfit,'m--')
            plt.legend(('Mode 1','Mode 2','Harmonic'), loc='upper left')
            #plt.plot(An.t, Anl,'k', lw=0.7) # Nonlinear mode-1
            ax2.set_xlim(t1,t2)
            ax2.set_ylim(-55,55)
            plt.ylabel('Amplitude [m]')
            plt.grid(b=True)

            myFmt = mdates.DateFormatter('%d-%b %H:%M')
            ax2.xaxis.set_major_formatter(myFmt)
            plt.xticks(rotation=17)

            plt.tight_layout()

            outplot = '%s_%s_%s_%s.png'%(plotpath, ncgroup, t1.strftime('%Y%m%d%H%M'),
                t2.strftime('%Y%m%d%H%M'))
            print outplot
            fig.savefig(outplot, dpi=150)

            #plt.show()



    # Create a dataset object and save
    #A_nt[ti1:ti2,:] = A_t.T
    #rhofit_t[ti1:ti2,:] = rhofitall
    #rho_t[ii,:] = iw.rhoZ
    #N2_t[ii,:] = iw.N2
    #phi_t[ii,:,:] = phi.T
    #amp_t[ii,:] = amp
    #phs_t[ii,:] = phs

    ds = xray.Dataset({
            'A_n':A_nt,
            'rhofit':rhofit_t,
            'Atide':Atide,
            'rhobar':rho_t,
            'N2':N2_t,
            'phi':phi_t,
            'amp':amp_t,
            'phs':phs_t,
            'r10':r10_t,
            'cn':cn_t,
        },
        attrs={
            'ncfile':ncfile,
            'group':ncgroup,
            'Description':'Linear vertical mode fit to KISSME mooring data',
            'X':T.X,\
            'Y':T.Y,
        })


    if write_mode is not None:
        ds.to_netcdf(outfile, mode=write_mode, format='NETCDF4', group=ncgroup)
        print 'Done'

    #Atide.plot();plt.show()

#########
#
outfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Fitted_Buoyancy.nc'
write_mode = 'w'

sites = ['WP250','NP250','SP250']

for site in sites:
    fit_modes(site, outfile, write_mode)
    write_mode = 'a'


####################################################
# Redundant stuff
#plt.figure()
#An.plot()

#plt.plot(An.t, yfit,'r')


#rhopr = rho.T - rhobar[np.newaxis,...]
#b = GRAV*rhopr/RHO0
#b = b.data # Masked array
#
### Interpolate the modal shape and N2 onto the measured depth locations
#F = PchipInterpolator(iw.Z[::-1], iw.phi[::-1])
#my_phi = F(Z)
#
#F = PchipInterpolator(iw.Z[::-1], iw.N2[::-1])
#my_N2 = F(Z)
#
##plt.plot(iw.N2, iw.Z)
##plt.plot(my_N2, Z ,'ro')
#
### Fit Ax=b
#L = my_phi*my_N2
#
#A_t,_,_,_ = la.lstsq(L[:,np.newaxis] , b.T)
#A_t = A_t.ravel()
#
## Reconstruct the density field
#bfit = L[:,np.newaxis]*A_t[np.newaxis,:]
#
#rhoprfit = bfit.T*RHO0/GRAV
#rhofit = rhoprfit + rhobar[np.newaxis,:]
#
####
## Nonlinear fit
##calc_buoyancy_h99(B, phi_1, c1, N2, dz_s, r10, T10, nonlinear=True):
#r10, _, _, T10,_ = iw.calc_nonlin_params()
#
## Interpolate the higher order structure function
#F = PchipInterpolator(iw.Z[::-1], T10[::-1])
#my_T10s = F(Z)
#
## Calculate the gradient of N2 and then interpolate it...
#dN2_dz = np.gradient(iw.N2, -iw.dz)
#F = PchipInterpolator(iw.Z[::-1], dN2_dz[::-1])
#my_dN2 = F(Z)
#
## Fit alpha as well
#alpha = -0.008*0
#my_T10 = my_T10s + alpha*my_phi
#
#def minfun(x0):
#    
#    btest = calc_buoyancy_h99(x0, my_phi, iw.c1, my_N2,\
#            my_dN2, r10, my_T10, nonlinear=False)
#
#    err = np.sum( (b-btest)**2., axis=1)
#    #print err.max(), alpha
#    return err
#
#A_nl = newton_krylov(minfun, A_t*0, f_tol=3.1e-5,\
#        method='bicgstab', rdiff=1e-6, iter=100)
#
## Reconstruct the nonlinear density field
#bfit_nl = calc_buoyancy_h99(A_nl, my_phi, iw.c1, my_N2,\
#            my_dN2, r10, my_T10, nonlinear=False)
#
#rhoprfit = bfit_nl*RHO0/GRAV
#rhofitnl = rhoprfit + rhobar[np.newaxis,:]
#
#plt.figure()
#plt.plot(A_nl,'r')
#plt.plot(A_t,'b')
#plt.legend(('nonlinear','linear'))
##plt.show()
#
#

#
#plt.subplot(313, sharex=ax)
#rhofitnl_om.contourf(clevs=np.arange(1020, 1028.,0.2))
#rhofitnl_om.contourf(clevs=np.arange(1020, 1028.,0.2), filled=False, cbar=False)
#plt.ylabel('Depth [m]')
#
##plt.figure()
##rho_om.contourf(clevs=np.arange(1020, 1028.,0.2), filled=False, cbar=False,
##        colors='k', linewidths=0.7)
##
##rhofit_om.contourf(clevs=np.arange(1020, 1028.,0.2), filled=False, cbar=False,
##        colors='b')
##
##rhofitnl_om.contourf(clevs=np.arange(1020, 1028.,0.2), filled=False, cbar=False,
##        colors='r')
##
#plt.show()
