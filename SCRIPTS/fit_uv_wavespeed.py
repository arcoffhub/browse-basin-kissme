"""
Fit the nonlinear phase speed to the velocity data by
    projecting the buoyancy modes onto the velocity data
"""

import mycurrents.oceanmooring as om

from soda.utils.timeseries import timeseries
from soda.utils import harmonic_analysis
from soda.utils.othertime import datetime64todatetime, datetimetodatetime64

from iwaves import IWaveModes
from iwaves.utils.density import FitDensity
import gsw

from scipy.interpolate import PchipInterpolator, interp1d
import scipy.linalg as la
from scipy.optimize import newton_krylov, least_squares

import xray
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
import matplotlib.dates as mdates

import pdb



############
# Function 1: 
def extract_uvw_kissme(site, t1, t2):
    """
    Extract TWC velocity profile at all sites for a given time period
    """
    adcpfile = 'Data/NetCDF/KISSME2017_RDI_QAQC.nc'

    zmin = -250.
    Nz = 100

    # Depths for interpolating the velocity field
    # Note that velocity is stored in pressure units (positive down from surface)
    veldepths = np.arange(0.,-zmin,4.)

    if site=='WP':
        group1 = 'WP250_RDI_150_kHz_Quartermaster_11795'
        group2 = 'WP250_RDI_300_kHz_Sentinel_20092'
        Tgroup = 'WP250'
    elif site=='SP':
        group1 = 'SP250_RDI75_kHz_Longranger_24613'
        group2 = None
        Tgroup = 'SP250'
    elif site=='NP':
        group1 = 'NP250_RDI_150_kHz_Quartermaster_16856'
        group2 = 'NP250_RDI_300_kHz_Monitor_20089'
        Tgroup = 'NP250'

    ######
    U1 = om.from_netcdf(adcpfile, 'u', group=group1)
    V1 = om.from_netcdf(adcpfile, 'v', group=group1)
    W1 = om.from_netcdf(adcpfile, 'w', group=group1)
    if group2 is not None:
        U2 = om.from_netcdf(adcpfile, 'u', group=group2)
        V2 = om.from_netcdf(adcpfile, 'v', group=group2)
        W2 = om.from_netcdf(adcpfile, 'w', group=group2)

    # Interpolate the velocity data onto a constant depth-array
    U1tmp = U1.clip(t1, t2)
    V1tmp = V1.clip(t1, t2)
    W1tmp = W1.clip(t1, t2)

    if group2 is not None:
        U2tmp = U2.clip(t1, t2)
        V2tmp = V2.clip(t1, t2)
        W2tmp = W2.clip(t1, t2)

    z1 = U1tmp.zvar.max(axis=1)
    if group2 is not None:
        z2 = U2tmp.zvar.max(axis=1)

    Uz = None
    Wz = None
    for zz in veldepths:
        # Check if the adcp is in the first or second depth profile
        if  zz >= z1.min() and zz <= z1.max():
            #print zz, group1
            uzz = U1tmp.interp_z(zz)
            vzz = V1tmp.interp_z(zz)
            wzz = W1tmp.interp_z(zz)
        elif group2 is None:
            #print zz, 'No DATA'
            uzz = np.zeros((U1tmp.Nt,))
            vzz = np.zeros((V1tmp.Nt,))
            wzz = np.zeros((W1tmp.Nt,))
            uzz[:] = np.nan
            vzz[:] = np.nan
            wzz[:] = np.nan
        elif  zz >= z2.min() and zz <= z2.max():
            #print zz, group2
            uzz = U2tmp.interp_z(zz)
            vzz = V2tmp.interp_z(zz)
            wzz = W2tmp.interp_z(zz)
        else:
            #print zz, 'No DATA'
            uzz = np.zeros((U1tmp.Nt,))
            vzz = np.zeros((V1tmp.Nt,))
            wzz = np.zeros((W1tmp.Nt,))
            uzz[:] = np.nan
            vzz[:] = np.nan
            wzz[:] = np.nan

        if Uz is None:
            Uz = om.OceanMooring(U1tmp.t, uzz, zz)
            Vz = om.OceanMooring(V1tmp.t, vzz, zz)
            Wz = om.OceanMooring(W1tmp.t, wzz, zz)
        else:
            Uz.vstack( om.OceanMooring(U1tmp.t, uzz[:,np.newaxis], -zz) )
            Vz.vstack( om.OceanMooring(V1tmp.t, vzz[:,np.newaxis], -zz) )
            Wz.vstack( om.OceanMooring(W1tmp.t, wzz[:,np.newaxis], -zz) )

    return Uz, Vz, Wz


############
# Function 2: Extract modal data from the fitted buoyancy modes



#############
# Load the data
#############

time = [ datetime(2017,4,3,4,0,0)+timedelta(hours=6*ii) for ii in range(4*30)]

#bfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Fitted_Buoyancy_Combined.nc'
bfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Fitted_Buoyancy_wout_motion.nc'

ds = xray.open_dataset(bfile, group='NP250')

###########

## create the time windows
t1s = [t0 - timedelta(hours=24) for t0 in time]
t2s = [t0 + timedelta(hours=24) for t0 in time]

### Test one time period
t1 = t1s[0]
t2 = t2s[0]

# Load velocity
Uz, Vz, Wz = extract_uvw_kissme('SP', t1, t2)

# Load modal structure and ampltude from the buoyancy data
phi_n = ds.phi.sel(timeslow=t1, method='nearest')
An = ds.A_n.sel(time=slice(t1,t2))
modes = ds.modes.values
nmodes = modes.size

####### Old fitting function
# Calculate dz
#Z = np.linspace(zmin, 0, Nz)
Z = phi_n.z.values
dz = np.mean(np.diff(Z))
znew = Uz.Z
nt = Uz.Nt
nz = Z.size

Lout = np.ones((nz, nmodes+1))

A_t = np.zeros((nt,nmodes+1))

for ii, mode in enumerate(modes):
    # Use the mode class to create the profile
    #phi, cn, he, znew = iw(zmin, dz, mode)
    #rn0, _, _, _ = iw.calc_nonlin_params()
    #c1.append(cn)
    #r10.append(rn0)
    phi = phi_n.values[ii,:]


    dphidz = np.gradient(phi, Z)
    Lout[:,ii+1] = dphidz[::-1]#*cn

# Compute the size of the output array
ctr = 0
i1 = np.zeros((nt,), np.int)
i2 = np.zeros((nt,), np.int)
p1 = 0
for tt in range(nt):
#for tt in [0]:
    # Quality control the data
    ytmp = Uz.y.data[:,tt]
    idx = ~np.isnan(ytmp)
    ctr += idx.sum()
    p2 = p1+idx.sum() 
    i1[tt] = p1
    i2[tt] = p2
    p1 = p2
 
# Compute the modal shape for each time step
cxall = np.zeros((nt,))
LHS = np.zeros((ctr,nmodes))
RHS = np.zeros((ctr,))

for tt in range(nt):
#for tt in [0]:
    # Quality control the data
    ytmp = Vz.y.data[:,tt]
    idx = ~np.isnan(ytmp)
    
    ## Interpolate the modal shape and N2 onto the measured depth locations
    #F = PchipInterpolator(Z[::-1], Lout, axis=0)
    F = interp1d(Z[::-1], Lout, axis=0, fill_value='extrapolate')

    L = F(znew[idx])
    
    ## Fit Ax=b
    A_tmp,_,_,_ = la.lstsq(L , ytmp[idx])
    #try:
    #except:
    #    print 'Fitting failed!'
    #    A_tmp = np.zeros((nmodes+1,))
    A_t[tt,:] = A_tmp

    ## get the barotropic velocity
    #ubar = A_tmp[0]
    ##print A_tmp[0]

    ## Compute the velocity from the buoyancy modes
    #u_n = Lout[:,1::] * An.values[tt,:]
    ##ufit = u_n.sum(axis=-1)

    ## Interpolate the fitted velocity onto the measured depth
    #F = interp1d(Z[::-1], u_n, axis=0, fill_value='extrapolate')
    #u_nz = F(znew[idx])

    #LHS[i1[tt]:i2[tt],:] = u_nz
    #RHS[i1[tt]:i2[tt]] = ytmp[idx]# - ubar
    ##cx,_,_,_ = la.lstsq(u_nz, ytmp[idx] - ubar)
    ##print cx[0]
    ##cxall[tt] = cx[0]
    

######
# Least-squares fit the velocity modes to the buoyancy modes
# to find c_x in each direction
cx,_,_,_ = la.lstsq(A_t[:,1::], An.values[:,:])

# Use least_squares to perform robust linear regression
def fun(x0, A1, A2):
   return A1 - x0*A2

#cx_l = least_squares(fun, [1.1], args=(An.values[:,0], A_t[:,1]))
cx_l = np.zeros((nmodes,))
for n in range(nmodes):
    soln = least_squares(fun, [1.1], args=(An.values[:,n], A_t[:,n+1]),
        loss='soft_l1', f_scale=0.1)
    cx_l[n] = soln['x']
        


#cx,_,_,_ = la.lstsq(LHS, RHS)
#ufit_n = Lout[:,np.newaxis,:]*A_t[np.newaxis,...]
#ufit = ufit_n.sum(axis=-1) # sum the modes


