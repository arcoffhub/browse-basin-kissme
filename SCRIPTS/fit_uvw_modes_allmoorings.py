##

###

import mycurrents.oceanmooring as om

from soda.utils.timeseries import timeseries
from soda.utils import harmonic_analysis
from soda.utils.othertime import datetime64todatetime, datetimetodatetime64

from iwaves import IWaveModes
from iwaves.utils.density import FitDensity, InterpDensity
import gsw

from scipy.interpolate import PchipInterpolator, interp1d
import scipy.linalg as la
from scipy.optimize import newton_krylov

import xray
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
import matplotlib.dates as mdates

import pdb

GRAV = 9.81
RHO0 = 1024.

def fit_uvmodes_linear(y, iw, z,  zmin, modes, isUV=True, Nz=100):
    """
    Compute the linear modal amplitude to the mode numbers in the list

    Inputs:
    ---
        u - matrix[nz, nt], velocity data
        iw - IWaveMode class
        z - vector[nz], depth from bottom to top, negative values (ascending)
        modes - list[nmodes], mode numbers in python index i.e. 0=1

    Returns
    ---
        A_t - matrix[nmodes, nt], modal amplitude
        ...

    """
    nmodes = len(modes)
    nz, nt = y.shape

    # Compute the modal structures
    Lout = np.ones((Nz,nmodes+1)) # Include the barotropic mode
    phi_n = []

    # Calculate dz
    Z = np.linspace(zmin, 0, Nz)
    dz = np.mean(np.diff(Z))

    c1 = []
    r10 = []

    for ii, mode in enumerate(modes):
        # Use the mode class to create the profile
        phi, cn, he, znew = iw(zmin, dz, mode)
        rn0, _, _, _ = iw.calc_nonlin_params()
        c1.append(cn)
        r10.append(rn0)

        if isUV:
            dphidz = np.gradient(phi, znew)
            #Lout[:,ii+1] = dphidz[::-1]*cn
            Lout[:,ii+1] = dphidz[::-1]
        else: # Vertical velocity modes
            #print 'Using vertical velocity modal functions...'
            #Lout[:,ii+1] = -phi[::-1]*cn
            Lout[:,ii+1] = phi[::-1]

    # Compute the modal shape for each time step
    A_t = np.zeros((nt,nmodes+1))
    for tt in range(nt):
    #for tt in [0]:
        # Quality control the data
        ytmp = y[:,tt]
        idx = ~np.isnan(ytmp)
        
        ## Interpolate the modal shape and N2 onto the measured depth locations
        #F = PchipInterpolator(znew[::-1], Lout, axis=0)
        F = interp1d(znew[::-1], Lout, axis=0, fill_value='extrapolate')

        L = F(z[idx])
        
        ## Fit Ax=b
        try:
            A_tmp,_,_,_ = la.lstsq(L , ytmp[idx])
        except:
            print 'Fitting failed!'
            A_tmp = np.zeros((nmodes+1,))

        A_t[tt,:] = A_tmp
        #print A_tmp[0]

    ufit_n = Lout[:,np.newaxis,:]*A_t[np.newaxis,...]
    ufit = ufit_n.sum(axis=-1) # sum the modes

    return A_t, ufit, Lout, np.array(c1), np.array(r10)

def get_iwmodes(T, t1, t2, density_func):
    """
    Initialise the modal structure function class based on the mean temperature
    for the period
    """
    Ttmp = T.clip(t1,t2)

    # Remove any bad layers
    idx = np.where(~np.any(np.isnan(Ttmp.y.data),axis=1))[0]

    # Remove instruments at the same height (WP250)
    Zu, Zidx = np.unique(Ttmp.Z[idx], return_index=True)
    is_unique = np.zeros(idx.shape, np.bool)
    is_unique[Zidx] = True
    
    #idx = (idx) & (is_unique)
    idx = idx[is_unique]

    Tshort = om.OceanMooring(Ttmp.t, Ttmp.y[idx,:], Ttmp.Z[idx], zvar=Ttmp.zvar[idx,:])

    Tmean = Tshort.y.mean(axis=1)
    Z = -Tshort.zvar.mean(axis=1)

    # Calculate the modes
    iw = IWaveModes(Tmean, Z, salt=34.6*np.ones_like(Tmean),\
            density_class=FitDensity, density_func=density_func)

    return iw

def fit_modes(site, outfile, write_mode):
    ###########
    # Input variables
    #ncfile = '/home/suntans/Share/ARCHub/DATA/FIELD/ShellPreludeRPS/NetCDF/Prelude_Gridded_Tuv.nc'
    ncfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Gridded_Mooring_TP_60sec_linear.nc'

    bfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Density_Combined_BestFit.nc'

    adcpfile = 'Data/NetCDF/KISSME2017_RDI_QAQC.nc'


    if site=='WP':
        group1 = 'WP250_RDI_150_kHz_Quartermaster_11795'
        group2 = 'WP250_RDI_300_kHz_Sentinel_20092'
        Tgroup = 'WP250'
    elif site=='SP':
        group1 = 'SP250_RDI75_kHz_Longranger_24613'
        group2 = None
        Tgroup = 'SP250'
    elif site=='NP':
        group1 = 'NP250_RDI_150_kHz_Quartermaster_16856'
        group2 = 'NP250_RDI_300_kHz_Monitor_20089'
        Tgroup = 'NP250'


    modes = range(10)
    #modes = range(5) # WP250

    varname = 'temperature'

    # Load some data
    dsb = xray.open_dataset(bfile)

    T = om.from_netcdf(ncfile, varname, group=Tgroup)

    U1 = om.from_netcdf(adcpfile, 'u', group=group1)
    V1 = om.from_netcdf(adcpfile, 'v', group=group1)
    W1 = om.from_netcdf(adcpfile, 'w', group=group1)
    if group2 is not None:
        U2 = om.from_netcdf(adcpfile, 'u', group=group2)
        V2 = om.from_netcdf(adcpfile, 'v', group=group2)
        W2 = om.from_netcdf(adcpfile, 'w', group=group2)


    freqs = ['M2','M4','M6']

    plotting = False
    plotpath = 'FIGURES/BuoyancyModes/KISSME'

    zmin = -250.
    Nz = 100

    # Depths for interpolating the velocity field
    # Note that velocity is stored in pressure units (positive down from surface)
    veldepths = np.arange(0.,-zmin,4.)

    density_func='double_tanh'

    time = [ datetime(2017,4,3,4,0,0)+timedelta(hours=6*ii) for ii in range(4*30)]
    #time = [ datetime(2017,4,3,4,0,0)+timedelta(hours=6*ii) for ii in range(4*2)]
    #time = [ datetime(2017,4,3,4,0,0)+timedelta(days=ii) for ii in range(36)]
    ###########

    ## create the time windows
    t1s = [t0 - timedelta(hours=24) for t0 in time]
    t2s = [t0 + timedelta(hours=24) for t0 in time]

    # define the dimensions
    Nchunk = len(time)
    Nz = Nz
    Nfreq = len(freqs)
    freqvals, frqnames = harmonic_analysis.getTideFreq(freqs)

    #tstart,tend = T.get_tslice(time[0]-timedelta(days=1), time[-1]+timedelta(days=1))
    tstart,tend = T.get_tslice(t1s[0], t2s[-1])
    Nt = tend-tstart
    Nmode = len(modes)+1

    # Define the coordinates:
    coords = {
        'time':T.t[tstart:tend],
        'timeslow': datetimetodatetime64(time),
        'modes':np.arange(0, Nmode,1, dtype=int),
        'z': np.linspace(zmin,0, Nz)[::-1], # top to bottom
        'freqs':freqvals,
        }

    ## Create the output variable as xray.DataArray objects
    Au_nt = xray.DataArray(np.zeros((Nt, Nmode)),
            dims = ('time','modes'),
            coords = {'time':coords['time'], 'modes':coords['modes']},
            attrs = {'long_name':'Modal eastward velocity amplitude',
                    'units':'m s-1',
                    }
           )

    Av_nt = xray.DataArray(np.zeros((Nt, Nmode)),
            dims = ('time','modes'),
            coords = {'time':coords['time'], 'modes':coords['modes']},
            attrs = {'long_name':'Modal northward velocity amplitude',
                    'units':'m s-1',
                    }
           )

    Aw_nt = xray.DataArray(np.zeros((Nt, Nmode)),
            dims = ('time','modes'),
            coords = {'time':coords['time'], 'modes':coords['modes']},
            attrs = {'long_name':'Modal vertical velocity amplitude',
                    'units':'m s-1',
                    }
           )

    ufit_t = xray.DataArray(np.zeros((Nt, Nz)),
            dims = ('time','z'),
            coords = {'time':coords['time'], 'z':coords['z']},
            attrs = {'long_name':'Best-fit eastward velocity',
                    'units':'m s-1',
                    }
           )

    vfit_t = xray.DataArray(np.zeros((Nt, Nz)),
            dims = ('time','z'),
            coords = {'time':coords['time'], 'z':coords['z']},
            attrs = {'long_name':'Best-fit northward velocity',
                    'units':'m s-1',
                    }
           )

    wfit_t = xray.DataArray(np.zeros((Nt, Nz)),
            dims = ('time','z'),
            coords = {'time':coords['time'], 'z':coords['z']},
            attrs = {'long_name':'Best-fit vertical velocity',
                    'units':'m s-1',
                    }
           )

    Autide = xray.DataArray(np.zeros((Nt,Nmode)),
            dims = ('time','modes'),
            coords = {'time':coords['time'], 'modes':coords['modes'] },
            attrs = {'long_name':'Eastward velocity tidal fit amplitude',
                    'units':'m s-1',
                    }
           )

    Avtide = xray.DataArray(np.zeros((Nt,Nmode)),
            dims = ('time','modes'),
            coords = {'time':coords['time'], 'modes':coords['modes']},
            attrs = {'long_name':'Northward velocity tidal fit amplitude',
                    'units':'m',
                    }
           )

    #Awtide = xray.DataArray(np.zeros((Nt,Nmode)),
    #        dims = ('time','modes'),
    #        coords = {'time':coords['time'], 'modes':coords['modes']},
    #        attrs = {'long_name':'Vertical velocity tidal fit amplitude',
    #                'units':'m s-1',
    #                }
    #       )

    rho_t = xray.DataArray(np.zeros((Nchunk, Nz)),
            dims = ('timeslow','z'),
            coords = {'timeslow':coords['timeslow'], 'z':coords['z']},
            attrs = {'long_name':'Background density',
                    'units':'kg m^-3',
                    }
           )

    N2_t = xray.DataArray(np.zeros((Nchunk, Nz)),
            dims = ('timeslow','z'),
            coords = {'timeslow':coords['timeslow'], 'z':coords['z']},
            attrs = {'long_name':'Backgrouna buoyancy frequency squared',
                    'units':'s^-2',
                    }
           )

    #phiu_t = xray.DataArray(np.zeros((Nchunk, Nmode, Nz)),
    #        dims = ('timeslow','modes','z'),
    #        coords = {'timeslow':coords['timeslow'], 'modes':coords['modes'], 'z':coords['z']},
    #        attrs = {'long_name':'Modal structure function',
    #                'units':'',
    #                }
    #       )
    #
    #phiw_t = xray.DataArray(np.zeros((Nchunk, Nmode, Nz)),
    #        dims = ('timeslow','modes','z'),
    #        coords = {'timeslow':coords['timeslow'], 'modes':coords['modes'], 'z':coords['z']},
    #        attrs = {'long_name':'Modal structure function vertical velocity',
    #                'units':'',
    #                }
    #       )


    uamp_t = xray.DataArray(np.zeros((Nchunk, Nmode, Nfreq)),
            dims = ('timeslow','modes','freqs'),
            coords = {'timeslow':coords['timeslow'],\
                    'modes':coords['modes'],\
                    'freqs':coords['freqs']},
            attrs = {'long_name':'Mode-one u harmonic amplitude',
                    'units':'m s-1',
                    }
           )

    uphs_t = xray.DataArray(np.zeros((Nchunk, Nmode, Nfreq)),
            dims = ('timeslow','modes','freqs'),
            coords = {'timeslow':coords['timeslow'],\
                    'modes':coords['modes'],\
                    'freqs':coords['freqs']},
            attrs = {'long_name':'Mode-one u harmonic phase',
                    'units':'radians',
                    }
           )

    vamp_t = xray.DataArray(np.zeros((Nchunk, Nmode, Nfreq)),
            dims = ('timeslow','modes','freqs'),
            coords = {'timeslow':coords['timeslow'],\
                    'modes':coords['modes'],\
                    'freqs':coords['freqs']},
            attrs = {'long_name':'Mode-one v harmonic amplitude',
                    'units':'m s-1',
                    }
           )

    vphs_t = xray.DataArray(np.zeros((Nchunk, Nmode, Nfreq)),
            dims = ('timeslow','modes','freqs'),
            coords = {'timeslow':coords['timeslow'],\
                    'modes':coords['modes'],\
                    'freqs':coords['freqs']},
            attrs = {'long_name':'Mode-one v harmonic phase',
                    'units':'radians',
                    }
           )

    ###
    t1 = t0 - timedelta(hours=24)
    t2 = t0 + timedelta(hours=24)
    ii=-1

    # Start of the for loop here...
    for t1, t2 in zip(t1s, t2s):
        ii+=1

        # Get the index for inserting the new time series in the output array
        ti1,ti2 = T.get_tslice(t1, t2)
        if ii == 0:
            tstart = ti1
        ti1 -= tstart
        ti2 -= tstart
        print t1, t2
        print ti1, ti2

        # Convert the temperature to a modal structure function
        #iw = get_iwmodes(T, t1,t2, density_func)

        # Get the density from the pre-loaded timeseries
        dsnow = dsb.sel(timeslow=time[ii], method='nearest')

        iw = IWaveModes(dsnow.rhobar.values[::-1], dsnow.z.values[::-1],\
                density_class=InterpDensity)

        # Interpolate the velocity data onto a constant depth-array
        U1tmp = U1.clip(t1, t2)
        V1tmp = V1.clip(t1, t2)
        W1tmp = W1.clip(t1, t2)

        if group2 is not None:
            U2tmp = U2.clip(t1, t2)
            V2tmp = V2.clip(t1, t2)
            W2tmp = W2.clip(t1, t2)

        z1 = U1tmp.zvar.max(axis=1)
        if group2 is not None:
            z2 = U2tmp.zvar.max(axis=1)

        Uz = None
        Wz = None
        for zz in veldepths:
        # Check if the adcp is in the first or second depth profile
            if  zz >= z1.min() and zz <= z1.max():
                #print zz, group1
                uzz = U1tmp.interp_z(zz)
                vzz = V1tmp.interp_z(zz)
                wzz = W1tmp.interp_z(zz)
            elif group2 is None:
                #print zz, 'No DATA'
                uzz = np.zeros((U1tmp.Nt,))
                vzz = np.zeros((V1tmp.Nt,))
                wzz = np.zeros((W1tmp.Nt,))
                uzz[:] = np.nan
                vzz[:] = np.nan
                wzz[:] = np.nan
            elif  zz >= z2.min() and zz <= z2.max():
                #print zz, group2
                uzz = U2tmp.interp_z(zz)
                vzz = V2tmp.interp_z(zz)
                wzz = W2tmp.interp_z(zz)
            else:
                #print zz, 'No DATA'
                uzz = np.zeros((U1tmp.Nt,))
                vzz = np.zeros((V1tmp.Nt,))
                wzz = np.zeros((W1tmp.Nt,))
                uzz[:] = np.nan
                vzz[:] = np.nan
                wzz[:] = np.nan

            if Uz is None:
                Uz = om.OceanMooring(U1tmp.t, uzz, zz)
                Vz = om.OceanMooring(V1tmp.t, vzz, zz)
                Wz = om.OceanMooring(W1tmp.t, wzz, zz)
            else:
                Uz.vstack( om.OceanMooring(U1tmp.t, uzz[:,np.newaxis], -zz) )
                Vz.vstack( om.OceanMooring(V1tmp.t, vzz[:,np.newaxis], -zz) )
                Wz.vstack( om.OceanMooring(W1tmp.t, wzz[:,np.newaxis], -zz) )

        print '\tFitting U...'
        Au_t, ufit, Lu, c1, r10 = fit_uvmodes_linear(Uz.y.data, iw, Uz.Z,  zmin, modes, Nz=100)

        print '\tFitting V...'
        Av_t, vfit, Lu, c1, r10 = fit_uvmodes_linear(Vz.y.data, iw, Uz.Z,  zmin, modes, Nz=100)

        print '\tFitting W...'
        Aw_t, wfit, Lw, c1, r10 = fit_uvmodes_linear(Wz.y.data, iw, Wz.Z,  zmin, modes,\
                Nz=100, isUV=False)

        # Compute harmonics of A (mode-one only)
        Aun = om.OceanMooring(Uz.t, Au_t, np.arange(Nmode))
        uamp, uphs, frq, mean, utidefit, yrms = Aun.tidefit(frqnames=freqs,\
            basetime=datetime(2017,1,1))

        Avn = om.OceanMooring(Uz.t, Av_t, np.arange(Nmode))
        vamp, vphs, frq, mean, vtidefit, yrms = Avn.tidefit(frqnames=freqs,\
            basetime=datetime(2017,1,1))


        # Update the DataArray objects
        Au_nt[ti1:ti2,:] = Au_t
        Av_nt[ti1:ti2,:] = Av_t
        Aw_nt[ti1:ti2,:] = Aw_t
        ufit_t[ti1:ti2,:] = ufit.T
        vfit_t[ti1:ti2,:] = vfit.T
        wfit_t[ti1:ti2,:] = wfit.T

        Autide[ti1:ti2,:] = utidefit
        Avtide[ti1:ti2,:] = vtidefit
        rho_t[ii,:] = iw.rhoZ
        N2_t[ii,:] = iw.N2
        #phi_t[ii,:,:] = phi.T
        uamp_t[ii,...] = uamp
        uphs_t[ii,...] = uphs
        vamp_t[ii,...] = vamp
        vphs_t[ii,...] = vphs

        ## Convert to an object for plotting
        if plotting:
            ufit_om = om.OceanMooring(Uz.t, ufit, iw.Z[::-1])
            
            plt.figure()
            ax=plt.subplot(211)
            Uz.contourf(np.linspace(-1.0,1.0,21), cmap='RdBu')
            
            plt.subplot(212, sharex=ax)
            ufit_om.contourf(np.linspace(-1.0,1.0,21), cmap='RdBu')

            #plt.figure()
            #ax=plt.subplot(211)
            #Wz.contourf(np.linspace(-0.1,0.1,21), cmap='RdBu')
            #
            #plt.subplot(212, sharex=ax)
            #wfit_om.contourf(np.linspace(-0.1,0.1,21), cmap='RdBu')
            plt.show()



    ####
    # Create a dataset object and save
    ds = xray.Dataset({
            'Au_n':Au_nt,
            'Av_n':Av_nt,
            'Aw_n':Aw_nt,
            'ufit':ufit_t,
            'vfit':vfit_t,
            'wfit':wfit_t,
            'Autide':Autide,
            'Avtide':Avtide,
            'rhobar':rho_t,
            'N2':N2_t,
            #'phi':phi_t,
            'uamp':uamp_t,
            'uphs':uphs_t,
            'vamp':vamp_t,
            'vphs':vphs_t,
        
        },
        attrs={
            'ncfile':ncfile,
            'site':site,
            'Description':'Linear vertical mode fit to KISSME mooring data',
        })


    if write_mode is not None:
        ds.to_netcdf(outfile, mode=write_mode, format='NETCDF4', group=site)
        print 'Dataset written to group: %s'%site
        print 72*'#'



#########
#
outfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Fitted_Velocity.nc'
write_mode = 'w'

sites = ['NP','WP','SP']

for site in sites:
    fit_modes(site, outfile, write_mode)
    write_mode = 'a'

###########

"""
#for t0 in time:
ii=-1
for t1, t2 in zip(t1s, t2s):
    print t1
    ii+=1

    # Clip the data
    Ttmp = T.clip(t1,t2)

    # Get the index for inserting the new time series in the output array
    ti1,ti2 = T.get_tslice(t1, t2)
    if ii == 0:
        tstart = ti1
    ti1 -= tstart
    ti2 -= tstart
    print ti1, ti2

    # Remove any bad layers
    idx = np.where(~np.any(np.isnan(Ttmp.y.data),axis=1))[0]

    # Remove instruments at the same height (WP250)
    Zu, Zidx = np.unique(Ttmp.Z[idx], return_index=True)
    is_unique = np.zeros(idx.shape, np.bool)
    is_unique[Zidx] = True
    
    #idx = (idx) & (is_unique)
    idx = idx[is_unique]

    Tshort = om.OceanMooring(Ttmp.t, Ttmp.y[idx,:], Ttmp.Z[idx], zvar=Ttmp.zvar[idx,:])

    Tmean = Tshort.y.mean(axis=1)
    Z = -Tshort.zvar.mean(axis=1)

    # Calculate the modes
    #iw = IWaveModes(Tmean, Z, salt=34.6*np.ones_like(Tmean),\
    #        density_class=FitDensity, density_func='single_tanh')
    #phi, c1, he, z = iw(-253, 2.5, mode)
    #
    #iw.plot_modes()
    #plt.show()

    ## Calculate the density time series
    Temp = Tshort.y
    #Z = Tshort.Z
    nz,nt = Temp.shape
    S = 34.6*np.ones((nz,nt))
    P = -Z[...,np.newaxis]*np.ones((1,nt))

    rho = gsw.rho_t_exact(S, Temp, P)
    rho = rho.data
    rhobar = rho.mean(axis=1)

    
    A_t, phi, rhofit, rhofitall, iw = fit_bmodes_linear(rho, rhobar, Z,\
        zmin, modes, Nz=Nz, density_func=density_func)
    #A_t, phi, rhofit = fit_bmodes_linear(rho, rhobar, Z, modes,\
    #    full_output=False)

    # Nonlinear fit
    #Anl, rhofitnl, iw = fit_bmodes_nonlinear(rho, rhobar, Z, 0)

    # Compute harmonics of A (mode-one only)
    An = om.OceanMooring(Tshort.t, A_t[0,:].squeeze(), 0.)
    #freqs = ['M2']
    amp, phs, frq, mean, yfit, yrms = An.tidefit(frqnames=freqs,\
            basetime=datetime(2017,1,1))

    print 'amp: ', amp
    print 'phs diff: ',phs-phs[0]

    # Convert to an oceanmooring object for plotting
    rho_om = om.OceanMooring(Tshort.t, rho, Z)
    #rhofit_om = om.OceanMooring(Tshort.t, rhofit, Z)
    rhofit_om = om.OceanMooring(Tshort.t, rhofitall, iw.Z)
    #rhofitnl_om = om.OceanMooring(Tshort.t, rhofitnl, Z)

    # Update the DataArray objects
    A_nt[ti1:ti2,:] = A_t.T
    rhofit_t[ti1:ti2,:] = rhofitall
    Atide[ti1:ti2] = yfit
    rho_t[ii,:] = iw.rhoZ
    N2_t[ii,:] = iw.N2
    phi_t[ii,:,:] = phi.T
    amp_t[ii,:] = amp
    phs_t[ii,:] = phs


    ####
    #
    if plotting:
        fig = plt.figure(figsize=(10,7))
        ax=plt.subplot(311)
        rho_om.contourf(clevs=np.arange(1020, 1028.,0.5), cbar=False)
        rho_om.contourf(clevs=np.arange(1020, 1028.,0.5), filled=False, cbar=False)
        plt.ylabel('Depth [m]')
        ax.set_xlim(t1,t2)
        ax.set_xticklabels([])

        ax1= plt.subplot(312, sharex=ax)
        rhofit_om.contourf(clevs=np.arange(1020, 1028.,0.5), cbar=False)
        rhofit_om.contourf(clevs=np.arange(1020, 1028.,0.5), filled=False, cbar=False)
        plt.ylabel('Depth [m]')
        ax.set_xlim(t1,t2)
        ax1.set_xticklabels([])

        ax2= plt.subplot(313,)
        plt.plot(Tshort.t, A_t[0,:], 'b', lw=0.5)
        plt.plot(Tshort.t, A_t[1,:], 'r', lw=0.5)

        plt.plot(An.t, yfit,'m--')
        plt.legend(('Mode 1','Mode 2','Harmonic'), loc='upper left')
        #plt.plot(An.t, Anl,'k', lw=0.7) # Nonlinear mode-1
        ax2.set_xlim(t1,t2)
        ax2.set_ylim(-55,55)
        plt.ylabel('Amplitude [m]')
        plt.grid(b=True)

        myFmt = mdates.DateFormatter('%d-%b %H:%M')
        ax2.xaxis.set_major_formatter(myFmt)
        plt.xticks(rotation=17)

        plt.tight_layout()

        outplot = '%s_%s_%s_%s.png'%(plotpath, ncgroup, t1.strftime('%Y%m%d%H%M'),
            t2.strftime('%Y%m%d%H%M'))
        print outplot
        fig.savefig(outplot, dpi=150)

        #plt.show()


"""


