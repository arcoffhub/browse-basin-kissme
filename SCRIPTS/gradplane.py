"""
Code to compute gradient in a plane
"""


def gradplane(xA, xB, xC, yA, yB, yC, zA, zB, zC):

    ABx = xB - xA
    ABy = yB - yA
    ABz = zB - zA
    
    ACx = xC - xA
    ACy = yC - yA
    ACz = zC - zA
    
    mx = ABy*ACz - ABz*ACy
    my = ABz*ACx - ABx*ACz
    mz = ABx*ACy - ACx*ABy
    
    return -mx/mz, -my/mz



