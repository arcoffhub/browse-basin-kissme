"""
Maps the mooring temperature data into pressure coordinates
"""

import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import interp1d, PchipInterpolator

from mycurrents import oceanmooring as om

#########
Tfile = 'Data/NetCDF/KISSME_Gridded_Mooring_T_60sec.nc'
Pfile = 'Data/NetCDF/KISSME_Gridded_Mooring_P_60sec.nc'

outfile = 'Data/NetCDF/KISSME_Gridded_Mooring_TP_60sec_linear.nc'

groups = ['SP250', 'NP250','WP250']

bottom_depth = 252.

mode = 'w'
#########

for group in groups:
    T = om.from_netcdf(Tfile, 'temperature', group=group)
    P = om.from_netcdf(Pfile, 'pressure', group=group)

    assert P.Nt == T.Nt

    nt = P.Nt

    ######
    # Build a matrix for interpolating
    y = np.vstack([bottom_depth * np.ones((1,nt)), P.y.data])
    z = np.hstack([0, P.Z])

    # Build the interpolation object
    F = interp1d(z, y, axis=0, kind='linear', fill_value='extrapolate')
    #F = PchipInterpolator(z, y, axis=0, extrapolate=True)
    zout = T.Z
    z_hat = F(zout)
    z_hat[np.isnan(z_hat)] = 0.

    # Give the 
    T.is2Dz = True
    T.zvar = z_hat

    # Make sure the depth attributes are pointing down
    T.positive=u'down'

    ####
    # Write the output
    T.to_netcdf(outfile, group=group, mode=mode)
    mode ='a'
    #plt.figure()
    #plt.pcolormesh(P.tsec, zout, z_hat)
    #plt.colorbar()
    #plt.show()

print 'Done'
