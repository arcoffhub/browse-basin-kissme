"""
Print out the ADCP files
"""
from soda.dataio import mydbase

dbfile = 'Data/KISSME_Instruments.db'
table = 'Deployments'

condition = 'InstrumentType LIKE "%RDI%"'

# Query the database and get all of the sites
varnames = ['StationID','FileName',\
	'FilePath','Latitude',\
	'Longitude','InstrumentDepth',\
	'Depth','Site']
query = mydbase.returnQuery(dbfile, varnames, table, condition)
#print 'Found stations:\n', query['StationID']

nfiles = len(query['FileName'])

ii=0
mode='w'
for adcpdir, adcpfiles in zip(query['FilePath'],query['FileName']):
    #if 'LR75' in adcpfiles:
    #if '16752' in adcpdir:
    #if 'Monitor' in adcpdir:
    infile = '%s/%s'%(adcpdir,adcpfiles)
    print 'python SCRIPTS/parse_adcp.py "%s" %d %s'%(infile, ii, mode)
    
    ii+=1
    mode='a'



