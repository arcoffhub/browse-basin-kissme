"""
3D mooring plot
"""

from mayavi import mlab
import xray
import numpy as np
from soda.dataio import mydbase
from soda.utils.myproj import MyProj

#######
basedir = '/home/suntans/Share/ScottReef/DATA/BATHYMETRY/'
#ncfile = '%s/Browse_GeoOzMultibeam_WEL_Blended_250m_DEM.nc'%basedir
ncfile = '%s/GA_WEL_Multi_Merged_w_GA250_250m_DEM.nc'%basedir
#ncfile = '%s/TimorSea_GA_GEBCO_Combined_DEM.nc'%basedir

vscale = 20.
#vscale = 1/5000.
ff = 2

dbfile = 'Data/KISSME_Instruments.db'
table = 'Deployments'

condition = 'Site LIKE "%SBE%"'
#
######

# Query the database and get all of the sites
varnames = ['StationID','FileName',\
	'FilePath','Latitude',\
	'Longitude','InstrumentDepth',\
        'InstrumentType',\
	'Depth','Site']


query = mydbase.returnQuery(dbfile, varnames, table, condition)
print 'Found stations:\n', query['StationID']

lon = np.array(query['Longitude'])
lat = np.array(query['Latitude'])
P = MyProj(None, utmzone=51, isnorth=False)
X,Y = P(lon,lat)




ds = xray.open_dataset(ncfile)

#X,Y = np.meshgrid(ds.X.values, ds.Y.values)
extents = [ds.X.values.min(), ds.X.values.max(), ds.Y.values.min(), ds.Y.values.max(), -6000,0]

#mlab.imshow(ds.topo.values[::ff,::ff].T, extent=extents,\
#        colormap='bone')
mlab.surf(ds.X.values[::ff], ds.Y.values[::ff], ds.topo.values[::ff,::ff].T,\
        colormap='bone',warp_scale=vscale)

mlab.points3d(X,Y,\
        np.array(query['InstrumentDepth'])*vscale,\
        color=(1.0,0.0,1.0), scale_factor=300.)
mlab.show()


