# coding: utf-8

"""
Process RDI adcp data using dolfyn (lkilcher.github.io/dolfyn/)
"""

#from dolfyn.adp import api as adcp
#from dolfyn.adp import rotate
from mycurrents.adcpio import ADCP_io
from soda.dataio import mydbase

import xray
import os
from datetime import datetime, timedelta
from collections import OrderedDict
import numpy as np
from glob import glob

import pdb

def read_all_adcp(adcpfiles, query, ii, config={}):

    adcpfiles = adcpfiles.replace('\\','/') # Windows to linux
    print 'Reading adcp file: %s...'%adcpfiles
    # Get all of the files
    adcps = []
    adcp1 = None
    for ff in sorted(glob(adcpfiles)):
        print ff
        adcp = ADCP_io(ff, rotate=True, mapbins=True, config_update=config)
        if adcp1 is None:
            adcp1 = adcp
        else:
            adcp.ds['distance'] = adcp1.ds.distance

        adcps.append(adcp.ds)
    # Concatenate
    alladcp = xray.concat(adcps,dim='time',coords='minimal')

    # Add metadata from the sql query to the attributes
    for kk in query.keys():
        alladcp.attrs.update({kk:query[kk][ii]})
    alladcp.attrs.update({'StationName':query['Site'][ii]})

    # Create the "Z" attribute from the distance
    if alladcp.orientation=='down':
	Z = alladcp.InstrumentDepth - alladcp.distance.values
    else:
	Z = alladcp.InstrumentDepth + alladcp.distance.values
    
    alladcp.attrs.update({'Z':Z})

    # Read single file
    #alladcp = ADCP_io(adcpfiles, rotate=True, config_update=config)

    #alladcp.attrs = {}
    #for kk in query.keys():
    #    alladcp.attrs.update({kk:query[kk][ii]})
    #alladcp.attrs.update({'StationName':query['Site'][ii]})


    return alladcp, adcp.encoding

    #alladcp.to_netcdf(ncfile, mode=mode, encoding=adcp.encoding, group=group)
    #print 'Done'

def adcp2nc(infile,query,ii,mode,orientation):
    alladcp, encoding = read_all_adcp(infile,\
	query, ii, config={'orientation':orientation})

    #group = alladcp.attrs['Site']
    group = alladcp.Site
    alladcp.to_netcdf(ncfile, mode=mode, encoding=encoding, group=group)

##########

#adcpfiles = '%s/DATA/ADCP600kHz/BR600*.000'%basedir
#group='RDI600'
#config={}

#### Can't read the 150 kHz...
#adcpfiles = '%s/DATA/ADCP150kHz/BR150003.000'%basedir
#ncfile = '%s/ProcessedData/UWABB2010_RDIADCPData.nc'%basedir
#group='RDI150'
#config={}

if __name__=='__main__':
    import sys

    #############
    # Inputs

    dbfile = 'Data/KISSME_Instruments.db'
    table = 'Deployments'

    condition = 'InstrumentType LIKE "%RDI%"'

    ncfile = 'Data/NetCDF/KISSME2017_RDIADCPData_float64.nc'

    #mode='w'
    #####

    # Query the database and get all of the sites
    varnames = ['StationID','FileName','FilePath','Latitude','Longitude','InstrumentDepth',\
	    'Depth','Site']
    query = mydbase.returnQuery(dbfile, varnames, table, condition)
    print 'Found stations:\n', query['StationID']

    infile = sys.argv[1]
    ii = int(sys.argv[2])
    mode = sys.argv[3]
    orientation=sys.argv[4]

    print infile, ii, mode, orientation
    adcp2nc(infile,query,ii,mode, orientation)

    """
    nfiles = len(query['FileName'])

    ii=0
    for adcpdir, adcpfiles in zip(query['FilePath'],query['FileName']):
	#if 'LR75' in adcpfiles:
	#if '16752' in adcpdir:
	#if 'Monitor' in adcpdir:
	infile = '%s/%s'%(adcpdir,adcpfiles)
	print 'python SCRIPTS/parse_adcp.py', infile, ii, mode
	
	ii+=1
	mode='a'

	#try:

	#    adcp2nc(infile,query,ii,mode)

	#    print 'Done'

	#    ii+=1
	#    mode='a'
	#except:
	#	print 'ERROR file failed: %s'%adcpfiles
   """

'''
# Test: read binary and save
adcp = ADCP_io(adcpfile, rotate=True, config_update=config)
adcp2 = ADCP_io(adcpfile2, rotate=True, config_update=config)
print adcp.ds.time.values[0], adcp.ds.time.values[-1]

# Concatenation code
adcp2.ds['distance'] = adcp.ds.distance
adcp3 = xray.concat([adcp.ds,adcp2.ds],dim='time',coords='minimal')
#adcp.to_netcdf(ncfile)
#
### Test: read netcdf
#adcp = ADCP_io(ncfile, rotate=True)
#
'''
