"""
Read seabird cnv files 
"""

from mycurrents import seabird as sb
#import soda.utils.mypandas as mpd
from soda.dataio import mydbase

from netCDF4 import num2date
from datetime import datetime,timedelta
import xray
import numpy as np
import os

import pdb


#############
# Inputs

dbfile = 'Data/KISSME_Instruments.db'
table = 'Deployments'

#condition = 'StationID LIKE "%SBE 56%"'
#condition = 'Site LIKE "%SBE56%"'
#condition = 'StationID LIKE "%SBE 39%"'
#condition = 'StationID LIKE "%SBE%"'
condition = 'InstrumentType LIKE "%SBE56%"'
#condition = 'InstrumentType LIKE "%SBE37%"'
#condition = 'InstrumentType LIKE "%SBE39%"'

outfile = 'Data/NetCDF/KISSME_SBE56Data.nc'
#outfile = 'Data/NetCDF/KISSME_SBE37Data.nc'
#outfile = 'Data/NetCDF/KISSME_SBE39Data.nc'
#########

# Query the database and get all of the sites
varnames = ['StationID','FileName',\
	'FilePath','Latitude',\
	'Longitude','InstrumentDepth',\
        'InstrumentType',\
	'Depth','Site']


query = mydbase.returnQuery(dbfile, varnames, table, condition)
print 'Found stations:\n', query['StationID']

nfiles = len(query['FileName'])
mode='w'
for ii in range(nfiles):
    filename = '%s/%s'%(query['FilePath'][ii],query['FileName'][ii])
    filename =filename.replace('\\','/') # Windows to linux

    print filename

    # Get the extension
    fname, ext = os.path.splitext(filename)

    # Hack for SBE56...
    # They do not specify the time units as seconds since 2000-01-01
    if 'SBE56' in query['InstrumentType'][ii]:
        basetime = datetime(2000,1,1)
    else: 
        basetime = None

    # This returns an xray dataset object
    try:
	if ext == '.cnv':
	    data = sb.parse_seabird_cnv(filename, basetime=basetime)
	elif ext == '.asc':
	    data = sb.parse_seabird_asc(filename, basetime=basetime)
    except:
	print 72*'!'
    	print 'ERROR: File %s failed!!!'%filename
	print 72*'!'
	continue


    # Fill in some extra info about the station
    #data.attrs.update({'stationname':query['Site'][ii]})
    #data.attrs.update({'original_file':query['FileName'][ii]})
    for vv in varnames:
        data.attrs.update({vv:query[vv][ii]})

    #stationname = '%s_%s_%dm'%(\
    #    query['Site'][ii].split('-')[0],\
    #    query['StationID'][ii].split('-')[0],\
    #    int(query['InstrumentDepth'][ii]),\
    #    )
    stationname = query['Site'][ii]

    data.attrs.update({'StationName':stationname})

    #print data
    print data.time.values[0], data.time.values[-1]

    # Save
    data.to_netcdf(outfile,group=stationname, mode=mode)
    mode = 'a'

    print '\tWritten to group: %s'%stationname

print 72*'#'
print 'done.'
print 72*'#'
