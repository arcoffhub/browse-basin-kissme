
# coding: utf-8

# # Loading and playing with SAR imagery data
# 
# 

# In[3]:


import xray
import rasterio
from glob import glob
#from soda.utils.maptools import readraster
from osgeo import gdal
from gdalconst import *

import matplotlib.pyplot as plt
from scipy.ndimage import gaussian_filter
import numpy as np


# In[6]:


#sardir='/home/suntans/Share/ARCHub/DATA/REMOTE/Glovis/S2A_OPER_PRD_MSIL1C_PDMC_20161105T111512_R017_V20161105T020442_20161105T020442.SAFE/GRANULE/S2A_OPER_MSI_L1C_TL_SGS__20161105T085105_A007164_T51LWE_N02.04/IMG_DATA/'
#sarfiles = glob('%s/*.jp2'%sardir)
sardir = '/home/suntans/Share/ARCHub/DATA/REMOTE/Glovis/'
sarfiles = glob('%s/*.jp2'%sardir)
outfile = '%s/test_sentinel.nc'%sardir


#sardir = '/home/suntans/Share/ARCHub/DATA/REMOTE/Glovis/S2A_OPER_PRD_MSIL1C_PDMC_20161105T111512_R017_V20161105T020442_20161105T020442.SAFE'
#sardir = '/home/suntans/Share/ARCHub/DATA/REMOTE/Glovis/S2A_OPER_PRD_MSIL1C_PDMC_20161105T111512_R017_V20161105T020442_20161105T020442.SAFE/GRANULE/S2A_OPER_MSI_L1C_TL_SGS__20161105T085105_A007164_T51LWE_N02.04'
#sarfiles = glob('%s/*.xml'%sardir)
print sarfiles[0]



## In[11]:
#
#
ds = xray.open_rasterio(sarfiles[0])
data = ds[0,:,:].values.astype(np.float)
dx = np.diff(ds.x).mean()

#ds.to_netcdf(outfile, format='NETCDF4_CLASSIC', engine='netcdf4')
#ds.to_netcdf(outfile)

# Use rasterio
#ds = rasterio.open(sarfiles[0])
#_,dx,_,_,_,dy = ds.transform
#data = ds.read(1)

filtwidth=1000.
filtwidth2 = 100.

sigma = filtwidth/dx
print 'Filtering dx = %f...'%filtwidth
datal = gaussian_filter(data, sigma)

datah = data - datal
del data 
del datal

# Bandpass to remove short wavelengths
sigma = filtwidth2/dx
print 'Filtering dx = %f...'%filtwidth2
datab = gaussian_filter(datah, sigma)

#ddy,ddx = np.gradient(datab, dx)
#gradh = np.abs(ddy+1j*ddx)
#del ddy
#del ddx



plt.imshow(datab, cmap='gray_r', vmin=-500, vmax=500.);plt.colorbar();plt.show()
#plt.imshow(gradh, cmap='gray_r');plt.colorbar(); plt.show()
#

#ds[0,:,:].plot.imshow(cmap='gray');plt.show()
#
#
## In[4]:
#
#
#get_ipython().set_next_input(u'data = readraster');get_ipython().magic(u'pinfo readraster')
#

# In[7]:
#infile = sarfiles[0]
#
#
##data = readraster(sarfiles[1])
##
## register all of the drivers
#gdal.AllRegister()
## open the image
#ds = gdal.Open(infile, GA_ReadOnly)
#subsets = ds.GetSubDatasets()
#


# In[ ]:




