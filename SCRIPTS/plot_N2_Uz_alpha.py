"""
Plot the nliw parameters

Calculated using: calc_iw_params_profiles.py
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from datetime import datetime

from iwaves import IWaveModes
from iwaves.utils.density import double_tanh_rho, single_tanh_rho
from soda.utils.mynumpy import grad_z
from soda.utils.otherplot import axcolorbar

import xray

###########

ncfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Density_Combined_BestFit_UnevenFilt.nc'

wave_angle = -50
uvfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Shear_Modes_Angle%02d.nc'%(np.abs(wave_angle))

wave_angle = -40
uvfile2 = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Shear_Modes_Angle%02d.nc'%(np.abs(wave_angle))

wave_angle = -60
uvfile3 = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Shear_Modes_Angle%02d.nc'%(np.abs(wave_angle))




sites = [None]

plot_alpha = True
outfile = 'FIGURES/KISSME_IW_N2_alpha_wShear'

#plot_alpha = False
#outfile = 'FIGURES/KISSME_IW_N2_cn_wShear'



#outfile = '../../FIGURES/CRUX_IW_N2_alpha_Apr2017'
#outfile = '../../FIGURES/CRUX_IW_N2_alpha_20162017'

###########

# Load the parameters from the netcdf file
def plot_site(site, ax0, ax1, ax2, colorbar=True):
    ds = xray.open_dataset(ncfile, group=site)
    dsuv = xray.open_dataset(uvfile, group=site)
    dsuv2 = xray.open_dataset(uvfile2, group=site)
    dsuv3 = xray.open_dataset(uvfile3, group=site)

    z = ds.z.values
    time = ds.timeslow.values
    N = np.sqrt(ds.N2.values)
    rho = ds.rhobar.values
    tlims = [time[0],time[-1]]

    #alpha = -2*ds['r10']*ds['cn']

    timeuv = dsuv.timeslow.values
    zuv = dsuv.z.values
    U = dsuv.U.values


    plt.sca(ax0)
    plt.contour(time, z, rho.T, np.arange(1020, 1027., 0.25),\
            colors='k',linewidths=0.25)
    #cc = plt.contourf(time, z, N.T, np.arange(0.0, 0.024, 0.002),\
    #     cmap='Blues', extend='max')
    cc = plt.contourf(timeuv, zuv, U.T, np.arange(-0.3, 0.325, 0.025),\
         cmap='RdBu', extend='both')

    #plt.colorbar()
    #ax0.set_xlim(tlims)
    ax0.set_xticklabels([])
    plt.ylabel('Depth [m]')
    plt.text(0.05,0.9,'(a)',transform=ax0.transAxes)

    #####
    plt.sca(ax2)
    if plot_alpha:
        dsuv['alpha'][:,0].plot(color='k',ls='',marker='.')
        dsuv['alpha_s'][:,0].plot(color='r',ls='',marker='.')
        dsuv2['alpha'][:,0].plot(color='k',ls='',marker='.',ms=0.5)
        dsuv2['alpha_s'][:,0].plot(color='r',ls='',marker='.',ms=0.5)
        dsuv3['alpha'][:,0].plot(color='k',ls='',marker='.',ms=0.5)
        dsuv3['alpha_s'][:,0].plot(color='r',ls='',marker='.',ms=0.5)

    else:
        dsuv['cn'][:,0].plot(color='k',ls='', marker='.')
        dsuv['cn_s'][:,0].plot(color='r',ls='', marker='.')
    plt.legend(('No Shear','Shear'), loc='lower right')
    (0*dsuv.alpha[:,0]).plot(color='0.5',ls='--',lw=1,)
    #plt.plot(params['r10']*0,'r--')
    #plt.ylabel('$r_{10}$ [m$^{-1}$]')
    if plot_alpha:
        plt.ylabel(r'$\alpha_1$ [s$^{-1}$]')
    else:
        plt.ylabel('$c_1$ [m s$^{-1}$]')
    #ax1.set_yscale('log')
    ax2.set_xticklabels([])
    plt.xlabel('')
    #ax2.set_xlim(tlims)
    #if mode==0:
    if plot_alpha:
        ax2.set_ylim(-15e-3,15e-3)
    else:
        ax2.set_ylim(1.0,1.5)


    #elif mode==1:
    #ax2.set_ylim(-0.015,0.015)
    plt.xticks(rotation=17.)
    plt.text(0.05,0.8,'(b)',transform=ax2.transAxes)
    plt.title('')
    plt.grid(b=True)

    plt.sca(ax1)
    # Plot a time series of each variable
    if plot_alpha:
        dsuv['alpha'][:,1].plot(color='k',ls='', marker='.')
        dsuv['alpha_s'][:,1].plot(color='r',ls='', marker='.')
        dsuv2['alpha'][:,1].plot(color='k',ls='', marker='.',ms=0.5)
        dsuv2['alpha_s'][:,1].plot(color='r',ls='', marker='.',ms=0.5)
        dsuv3['alpha'][:,1].plot(color='k',ls='', marker='.',ms=0.5)
        dsuv3['alpha_s'][:,1].plot(color='r',ls='', marker='.',ms=0.5)


    else:
        dsuv['cn'][:,1].plot(color='k',ls='', marker='.')
        dsuv['cn_s'][:,1].plot(color='r',ls='', marker='.')

    #plt.legend(('No Shear','Shear'), loc='upper right')
    (0*dsuv.alpha[:,0]).plot(color='0.5',ls='--',lw=1,)
 
    if plot_alpha:
        plt.ylabel(r'$\alpha_2$ [s$^{-1}$]')
    else:
        plt.ylabel('$c_2$ [m s$^{-1}$]')
    plt.xlabel('')
    if plot_alpha:
        ax1.set_ylim(-0.015,0.015)
    else:
        ax1.set_ylim(0.4,0.7)

    plt.xticks(rotation=17.)
    plt.text(0.05,0.8,'(c)',transform=ax1.transAxes)
    plt.title('')
    plt.grid(b=True)

    #ax2 = plt.subplot2grid((5,1), (3,0), rowspan=1, sharex=ax0)
    #plt.plot(params['r20'])
    #plt.plot(params['r20']*0,'r--')
    #plt.ylabel('$r_{20}$ [s m$^{-3}$]')
    #ax2.set_xticklabels([])
    #ax2.set_xlim(tlims)
    #plt.text(0.05,0.9,'(c)',transform=ax2.transAxes)
    #
    #ax3 = plt.subplot2grid((5,1), (4,0), rowspan=1,)
    #plt.plot(params['c1'])
    ##plt.plot(params['c1']*0,'r--')
    #plt.ylabel('$c_{1}$ [m s$^{-1}$]')
    #ax3.set_xlim(tlims)
    #plt.text(0.05,0.9,'(d)',transform=ax3.transAxes)
    #plt.xticks(rotation=17.)

    #plt.tight_layout()

    #plt.savefig('%s.png'%outfile, dpi=150)
    #plt.savefig('%s.pdf'%outfile, dpi=150)
    return cc


plt.figure(figsize=(7,9))
ax0 = plt.subplot2grid((5,1), (0,0), rowspan=3)
ax1 = plt.subplot2grid((5,1), (4,0), rowspan=1, )
ax2 = plt.subplot2grid((5,1), (3,0), rowspan=1, )
for site in sites:
    cc = plot_site(site, ax0, ax1, ax2,)

# KISSME period
ax0.set_xlim(datetime(2017, 3, 31), datetime(2017, 5, 8))

ax1.set_xlim(ax0.get_xlim())
ax2.set_xlim(ax0.get_xlim())
#cb=axcolorbar(cc, ax=ax2, pos =[0.15, 0.85, 0.26, 0.08])
cb=axcolorbar(cc, ax=ax0, pos =[0.15, 0.10, 0.26, 0.04])
#cb.ax.set_title('N(z) [s$^{-1}$]')
#cb.set_ticks([0,0.01,0.02])
cb.ax.set_title('U(z) [m s$^{-1}$]')
cb.set_ticks([-0.3,0.0,0.3])


plt.savefig('%s.png'%outfile, dpi=150)
plt.savefig('%s.pdf'%outfile, dpi=150)

plt.show()



