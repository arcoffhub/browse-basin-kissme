"""
Plot the nliw parameters

Calculated using: calc_iw_params_profiles.py
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from iwaves import IWaveModes
from iwaves.utils.density import double_tanh_rho, single_tanh_rho
from soda.utils.mynumpy import grad_z
from soda.utils.otherplot import axcolorbar

import xray

###########

#proftype = 'single'
#strat_param_file = 'Docs/iw_params_NP250_single_tanh.csv'
#outfile = 'FIGURES/KISSME_IW_Params_NP_single'

#proftype = 'double'
#strat_param_file = 'Docs/iw_params_SP250_double_tanh.csv'

#dz = 2.5

#ncfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Density_Combined_BestFit.nc'
ncfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Density_Combined_BestFit_newtanh.nc'
#ncfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Density_Combined_BestFit_ButterFilt.nc'
ncfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Density_Combined_BestFit_UnevenFilt.nc'


outfile = 'FIGURES/KISSME_IW_N2_alpha_uneven'

###########

# Load the parameters from the netcdf file
ds = xray.open_dataset(ncfile)
z = ds.z.values
time = ds.timeslow.values
N = np.sqrt(ds.N2.values)
rho = ds.rhobar.values
tlims = [time[0],time[-1]]

alpha = -2*ds['r10']*ds['cn']

plt.figure(figsize=(7,9))
ax0 = plt.subplot2grid((5,1), (0,0), rowspan=3)
plt.contour(time, z, rho.T, np.arange(1020, 1027., 0.25),\
        colors='k',linewidths=0.25)
cc = plt.contourf(time, z, N.T, np.arange(0.0, 0.024, 0.002),\
     cmap='Blues', extend='max')
#plt.colorbar()
ax0.set_xlim(tlims)
ax0.set_xticklabels([])
plt.ylabel('Depth [m]')
plt.text(0.05,0.9,'(a)',transform=ax0.transAxes)

ax2 = plt.subplot2grid((5,1), (3,0), rowspan=1, )
plt.plot()
ds['cn'][:,0].plot(color='k')
ds['cn'][:,1].plot(color='r')
#plt.legend(('Mode 1','Mode 2'), loc='lower left')
(0*alpha[:,0]).plot(color='0.5',ls='--',lw=1,)
#plt.plot(params['r10']*0,'r--')
#plt.ylabel('$r_{10}$ [m$^{-1}$]')
plt.ylabel(r'$c_n$ [m s$^{-1}$]')
#ax1.set_yscale('log')
#ax1.set_xticklabels([])
ax2.set_xlim(tlims)
#if mode==0:
#    ax1.set_ylim(-5e-3,5e-3)
#elif mode==1:
ax2.set_ylim(0.0,1.5)
plt.xticks(rotation=17.)
plt.text(0.05,0.8,'(b)',transform=ax2.transAxes)
plt.title('')
plt.grid(b=True)



# Plot a time series of each variable
ax1 = plt.subplot2grid((5,1), (4,0), rowspan=1, )
plt.plot()
alpha[:,0].plot(color='k')
alpha[:,1].plot(color='r')
plt.legend(('Mode 1','Mode 2'), loc='lower right')
(0*alpha[:,0]).plot(color='0.5',ls='--',lw=1,)
#plt.plot(params['r10']*0,'r--')
#plt.ylabel('$r_{10}$ [m$^{-1}$]')
plt.ylabel(r'$\alpha$ [s$^{-1}$]')
#ax1.set_yscale('log')
#ax1.set_xticklabels([])
ax1.set_xlim(tlims)
#if mode==0:
#    ax1.set_ylim(-5e-3,5e-3)
#elif mode==1:
ax1.set_ylim(-0.015,0.015)
plt.xticks(rotation=17.)
plt.text(0.05,0.8,'(c)',transform=ax1.transAxes)
plt.title('')
plt.grid(b=True)

cb=axcolorbar(cc, ax=ax2, pos =[0.15, 0.85, 0.26, 0.08])
cb.ax.set_title('N(z) [s$^{-1}$]')
cb.set_ticks([0,0.01,0.02])

#ax2 = plt.subplot2grid((5,1), (3,0), rowspan=1, sharex=ax0)
#plt.plot(params['r20'])
#plt.plot(params['r20']*0,'r--')
#plt.ylabel('$r_{20}$ [s m$^{-3}$]')
#ax2.set_xticklabels([])
#ax2.set_xlim(tlims)
#plt.text(0.05,0.9,'(c)',transform=ax2.transAxes)
#
#ax3 = plt.subplot2grid((5,1), (4,0), rowspan=1,)
#plt.plot(params['c1'])
##plt.plot(params['c1']*0,'r--')
#plt.ylabel('$c_{1}$ [m s$^{-1}$]')
#ax3.set_xlim(tlims)
#plt.text(0.05,0.9,'(d)',transform=ax3.transAxes)
#plt.xticks(rotation=17.)

#plt.tight_layout()

plt.savefig('%s.png'%outfile, dpi=150)
plt.savefig('%s.pdf'%outfile, dpi=150)

plt.show()



