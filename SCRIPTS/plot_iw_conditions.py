"""
Plot fitted buoyancy modes
"""

import xray
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime
from scipy.signal import hilbert

#from iwaves.utils.minmax import get_peak_window

from mycurrents import oceanmooring as om

####
#ncfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Fitted_Buoyancy.nc'
#ncfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Fitted_Buoyancy_Combined.nc'
ncfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Fitted_Buoyancy_wout_motion.nc'
#ncfile = '/home/suntans/Share/ARCHub/DATA/FIELD/ShellCrux/KP150_Fitted_Buoyancy.nc'
#
uvfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Fitted_Velocity.nc'

ncgroups = [\
    #'KP150_T',
    #'WP250',
    #'NP250',
    'SP250',
]

#ncgroup = ncgroups[0]

#ncfile = '/home/suntans/Share/ARCHub/DATA/FIELD/ShellPreludeRPS/Prelude_Fitted_Buoyancy.nc'
#
#ncgroups = [\
#    'F_Block_2007_CM04',\
#    'F_Block_2008a_CM04',\
#    'F_Block_2008b_CM04',\
#    'F_Block_2009a_CM04',\
#    'F_Block_2009b_CM04',\
#]

xlims = [datetime(2017,4,3), datetime(2017,5,8)]

outfile = 'FIGURES/IW_Parameters_Timeseries'
#####


plt.figure(figsize=(5,8))
for ncgroup in ncgroups:


    ds = xray.open_dataset(ncfile, group=ncgroup)
    dsu = xray.open_dataset(uvfile, group=ncgroup[0:2])
    
    # Extract the barotropic tide
    ubar = dsu.Au_n[:,0].values
    vbar = dsu.Av_n[:,0].values

    # Rotate along the main axis
    tide_angle = -42*np.pi/180.

    # Note this is a clockwise rotation matrix
    ubar_pr = ubar*np.cos(tide_angle) + vbar*np.sin(tide_angle)
    vbar_pr = -ubar*np.sin(tide_angle) + vbar*np.cos(tide_angle)

    ubar_pr[np.abs(ubar_pr)>2] = 0. # QC

    U = om.OceanMooring(dsu.time.values, ubar_pr, 0.)
    V = om.OceanMooring(dsu.time.values, vbar_pr, 0.)

    utide = U.tidefit(['M2','S2','K1','O1','P1','Q1','N2','K2'])
    vtide = V.tidefit(['M2','S2','K1','O1','P1','Q1','N2','K2'])

    utidefit = utide[-2]
    vtidefit = vtide[-2]

    # hilbert transform the get the amplitude
    uh = hilbert(utidefit)

    # Calculate the steepening time scale
    k_x = ds.freqs[0].values/ds.cn.values
    Lw = 2*np.pi/k_x
    a1 = 0.17 # See calc_kdv_steeptime.py (in the ISW directory)
    tau_1 = a1*np.pi/(np.abs(ds.r10.values)*ds.freqs[0].values * 1)
    tau_10 = a1*np.pi/(np.abs(ds.r10.values)*ds.freqs[0].values * 10)
    tau_30 = a1*np.pi/(np.abs(ds.r10.values)*ds.freqs[0].values * 30)

    Ls_1 = tau_1 * ds.cn.values
    Ls_10 = tau_10 * ds.cn.values
    Ls_30 = tau_30 * ds.cn.values

    # Plot u_bar
    #ax=plt.subplot(411)
    #plt.plot(dsu.time, utidefit, '0.2', lw=0.5)
    ##plt.plot(dsu.time, np.abs(uh), 'k', lw=0.5)

    #plt.grid(b=True)
    #plt.xlim(xlims)
    #plt.ylim(-0.45,0.45)
    #plt.ylabel('$U_0$ [m s$^{-1}$]')
    #ax.text(0,1.04,'(a)', transform=ax.transAxes)
    #ax.set_yticks([-0.4,-0.2,0,0.2,0.4])

    ## Plot Amp_M2 (mode 1 and 2)
    #ax = plt.subplot(412)

    #ds.amp[:,0,0].plot(color='k', lw=0.7)
    #ds.amp[:,1,0].plot(color='r',lw=0.7)
    #plt.ylabel('Amplitude [m]')
    #plt.title('')
    #plt.grid(b=True)
    #plt.xlim(xlims)
    #ax.text(0,1.04,'(b)', transform=ax.transAxes)

    # Plot c1,2
    ax = plt.subplot(411)
    ds.cn[:,0].plot(color='k',lw=0.7)
    ds.cn[:,1].plot(color='r',lw=0.7)
    plt.ylabel('$c_n$ [m s$^{-1}$]')
    plt.title('')
    plt.ylim(0, 1.5)
    plt.grid(b=True)
    plt.xlim(xlims)
    ax.text(0,1.04,'(a)', transform=ax.transAxes)

    # Plot r10,n
    ax = plt.subplot(412)
    ds.r10[:,0].plot(color='k',lw=0.7)
    ds.r10[:,1].plot(color='r',lw=0.7)
    plt.ylabel('$r_{10}$ [m$^{-1}$]')
    #ax.set_yscale('log')
    plt.title('')
    plt.grid(b=True)
    plt.xlim(xlims)
    plt.legend(('mode 1','mode 2'), loc='lower left')
    ax.text(0,1.04,'(b)', transform=ax.transAxes)

    # Plot r10,n
    ax = plt.subplot(413)
    plt.semilogy(ds.timeslow.values, tau_10[:,0]/86400., 'k--', lw=0.7)
    plt.semilogy(ds.timeslow.values, tau_10[:,1]/86400., 'r--', lw=0.7)
    plt.semilogy(ds.timeslow.values, tau_30[:,0]/86400., 'k', lw=0.7)
    plt.semilogy(ds.timeslow.values, tau_30[:,1]/86400., 'r', lw=0.7)
    plt.ylabel(r'$\tau_s$ [d]')
    #ax.set_yscale('log')
    ax.set_ylim(1e-1,100)
    plt.title('')
    plt.grid(b=True)
    plt.xlim(xlims)
    ax.set_xticklabels([])
    ax.text(0,1.04,'(c)', transform=ax.transAxes)

    ax = plt.subplot(414)
    plt.semilogy(ds.timeslow.values, Ls_10[:,0]/1000., 'k--', lw=0.7)
    plt.semilogy(ds.timeslow.values, Ls_10[:,1]/1000., 'r--', lw=0.7)
    plt.semilogy(ds.timeslow.values, Ls_30[:,0]/1000., 'k', lw=0.7)
    plt.semilogy(ds.timeslow.values, Ls_30[:,1]/1000., 'r', lw=0.7)
    plt.ylabel(r'$L_s$ [km]')
    #ax.set_yscale('log')
    ax.set_ylim(1,1e4)
    plt.title('')
    plt.grid(b=True)
    plt.xlim(xlims)
    plt.xticks(rotation=17)

    ax.text(0,1.04,'(d)', transform=ax.transAxes)

plt.tight_layout()

plt.savefig('%s_%s.png'%(outfile, ncgroup), dpi=150)
plt.savefig('%s_%s.pdf'%(outfile, ncgroup), dpi=150)
plt.show()


