"""
Plot fitted buoyancy modes
"""

import xray
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime
from scipy.signal import hilbert
import matplotlib.dates as mdates

from iwaves.utils.minmax import get_peak_window

from mycurrents import oceanmooring as om
from soda.utils.timeseries import skill

omega = 1.42e-4

def print_max_params(ds, uh, time, mode):
    tstr = str(tt)[0:19].replace('T',' ')
    amp = ds.A_n.sel(time=time).values[mode]
    amptide = ds.Atide.sel(time=time).values[mode]
    ubar = uh.sel(time=time, method='nearest').values
    r10 = ds.r10.sel(timeslow=time, method='nearest').values[mode]
    cn = ds.cn.sel(timeslow=time, method='nearest').values[mode]
    if mode ==0:
        a0 = 30.
    else:
        a0 = 10.
    tau_s = 1/(omega*a0*np.abs(r10))

    print '%s, %3.1f, %3.1f, %3.1f, %3.2f, %1.6f, %1.2f, %4.1f'%\
        ( tstr, amp, amptide, amp/amptide, np.abs(ubar), r10, cn, tau_s/86400. )

####
#ncfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Fitted_Buoyancy.nc'
#ncfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Fitted_Buoyancy_Combined.nc'
ncfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Fitted_Buoyancy_wout_motion.nc'
#ncfile = '/home/suntans/Share/ARCHub/DATA/FIELD/ShellCrux/KP150_Fitted_Buoyancy.nc'
#
uvfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Fitted_Velocity.nc'

ncgroups = [\
    #'KP150_T',
    #'WP250',
    #'NP250',
    'SP250',
]


#ncfile = '/home/suntans/Share/ARCHub/DATA/FIELD/ShellPreludeRPS/Prelude_Fitted_Buoyancy.nc'
#
#ncgroups = [\
#    'F_Block_2007_CM04',\
#    'F_Block_2008a_CM04',\
#    'F_Block_2008b_CM04',\
#    'F_Block_2009a_CM04',\
#    'F_Block_2009b_CM04',\
#]

xlims = [datetime(2017,4,1), datetime(2017,5,8)]
xlims1 = [datetime(2017,4,2,12,0,0), datetime(2017,4,4,12,0,0)]
xlims2 = [datetime(2017,4,29,12,0,0), datetime(2017,5,1,12,0,0)]

ylims = [-70,70]

outfile = 'FIGURES/IW_Harmonic_Timeseries'

plot = True
#####

def plotbox(xlims, ylims):
    plt.fill([xlims[0],xlims[1],xlims[1],xlims[0],xlims[0]],\
        [ylims[0],ylims[0],ylims[1],ylims[1],ylims[0]],\
        color='0.5',alpha=0.5)

# Plot formatting options
days = mdates.DayLocator()   # every year
hours = mdates.HourLocator()  # every month

if plot:
    plt.figure(figsize=(9,8))
for ncgroup in ncgroups:
    ds = xray.open_dataset(ncfile, group=ncgroup)
    dsu = xray.open_dataset(uvfile, group=ncgroup[0:2])
    
    # Extract the barotropic tide
    ubar = dsu.Au_n[:,0].values
    vbar = dsu.Av_n[:,0].values

    # Rotate along the main axis
    tide_angle = -42*np.pi/180.

    # Note this is a clockwise rotation matrix
    ubar_pr = ubar*np.cos(tide_angle) + vbar*np.sin(tide_angle)
    vbar_pr = -ubar*np.sin(tide_angle) + vbar*np.cos(tide_angle)

    ubar_pr[np.abs(ubar_pr)>10.] = 0.

    U = om.OceanMooring(dsu.time.values, ubar_pr, 0.)
    V = om.OceanMooring(dsu.time.values, vbar_pr, 0.)

    utide = U.tidefit(['M2','S2','K1','O1','P1','Q1','N2','K2'])
    vtide = V.tidefit(['M2','S2','K1','O1','P1','Q1','N2','K2'])

    utidefit = utide[-2]
    vtidefit = vtide[-2]

    # hilbert transform the get the amplitude
    uh = hilbert(utidefit)
    uh = om.OceanMooring(dsu.time.values, uh, 0.).to_xray()


    # Calculate the harmonic residual
    def plot_mode(mode, legend=True):
        Amp_1 = om.OceanMooring(ds.time.values, ds.A_n[:,mode].values, 0. )
        Amp_harm1 = om.OceanMooring(ds.time.values, ds.Atide[:,mode].values, 0. )
        res1 = om.OceanMooring(ds.time.values, ds.A_n[:,mode].values - ds.Atide[:,mode].values, 0. )

        # Use the RMS or hilbert transform
        Amp_rms = om.OceanMooring(ds.time.values, 
            Amp_harm1.running_rms(Amp_harm1.y,2*86400.), 0.)
        Amp_hil = om.OceanMooring(ds.time.values, 
            np.abs(hilbert(Amp_harm1.y)), 0.)

        Amp_ratio = om.OceanMooring(ds.time.values, 
            np.abs(res1.y) / Amp_rms.y, 0.)

        Amp_fit = Amp_1.tidefit(['M2','S2','K1','O1','P1','Q1','N2','K2'])
        Amp_coh = om.OceanMooring(ds.time.values, Amp_fit[-2], 0.)

        Amp_SD =om.OceanMooring(ds.time.values,  
                Amp_1.filt([30*3600., 3*3600.], btype='band', order=2), 0.)

        print 'Percetange variance short-time fit %3.1f'%(skill(Amp_harm1.y, Amp_1.y)*100)
        print 'Percetange variance tidal fit %3.1f'%(skill(Amp_coh.y, Amp_1.y)*100)


        #plt.subplot(311)
        Amp_1.plot(lw=0.4)
        Amp_harm1.plot(c='m',lw=2.1, ls='--')
        Amp_coh.plot(c='k',lw=0.7,ls='--')
        #Amp_SD.plot() # Band-passed result
        plt.ylim(-70,70)
        plt.xlim(datetime(2017,4,3), datetime(2017,4,6))
        plt.ylabel('Mode %d Amplitude [m]'%(mode+1))
        plt.grid(b=True)

        if legend:
            plt.legend(('Amplitude','Short-Time Fit','Tidal Harmonics Fit'))

    ax1 = plt.subplot(211)
    plot_mode(0)

    plt.text(0.1,0.9, '(a)', transform=ax1.transAxes)
    ax1.set_xticklabels([])
    ax1.xaxis.set_major_locator(days)
    ax1.xaxis.set_minor_locator(hours)

    ax2 = plt.subplot(212)
    plot_mode(1, legend=False)

    days = mdates.DayLocator()   # every year
    hours = mdates.HourLocator()  # every month
    daysFmt = mdates.DateFormatter('%Y-%m-%d')
    ax2.xaxis.set_major_locator(days)
    ax2.xaxis.set_minor_locator(hours)
    ax2.xaxis.set_major_formatter(daysFmt)
    plt.text(0.1,0.9, '(b)', transform=ax2.transAxes)

    plt.tight_layout()
    plt.savefig('%s.png'%outfile,dpi=150)
    plt.savefig('%s.pdf'%outfile,dpi=150)
    plt.show()



