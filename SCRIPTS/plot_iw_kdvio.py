"""
Plot fitted buoyancy modes
"""

import xray
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime
from scipy.signal import hilbert
import matplotlib.dates as mdates

from iwaves.utils.minmax import get_peak_window

from mycurrents import oceanmooring as om
omega = 1.42e-4

def print_max_params(ds, uh, time, mode):
    tstr = str(tt)[0:19].replace('T',' ')
    amp = ds.A_n.sel(time=time).values[mode]
    amptide = ds.Atide.sel(time=time).values[mode]
    ubar = uh.sel(time=time, method='nearest').values
    r10 = ds.r10.sel(timeslow=time, method='nearest').values[mode]
    cn = ds.cn.sel(timeslow=time, method='nearest').values[mode]
    if mode ==0:
        a0 = 30.
    else:
        a0 = 10.
    tau_s = 1/(omega*a0*np.abs(r10))

    print '%s, %3.1f, %3.1f, %3.1f, %3.2f, %1.6f, %1.2f, %4.1f'%\
        ( tstr, amp, amptide, amp/amptide, np.abs(ubar), r10, cn, tau_s/86400. )

####
#ncfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Fitted_Buoyancy.nc'
#ncfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Fitted_Buoyancy_Combined.nc'
ncfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Fitted_Buoyancy_wout_motion.nc'
#ncfile = '/home/suntans/Share/ARCHub/DATA/FIELD/ShellCrux/KP150_Fitted_Buoyancy.nc'
#
uvfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Fitted_Velocity.nc'

ncgroups = [\
    #'KP150_T',
    #'WP250',
    #'NP250',
    'SP250',
]


#ncfile = '/home/suntans/Share/ARCHub/DATA/FIELD/ShellPreludeRPS/Prelude_Fitted_Buoyancy.nc'
#
#ncgroups = [\
#    'F_Block_2007_CM04',\
#    'F_Block_2008a_CM04',\
#    'F_Block_2008b_CM04',\
#    'F_Block_2009a_CM04',\
#    'F_Block_2009b_CM04',\
#]

xlims = [datetime(2017,4,1), datetime(2017,5,8)]
xlims1 = [datetime(2017,4,2,12,0,0), datetime(2017,4,4,12,0,0)]
xlims2 = [datetime(2017,4,29,12,0,0), datetime(2017,5,1,12,0,0)]

ylims = [-70,70]

outfile = 'FIGURES/IW_KdV_Inputs'

plot = True
#####

def plotbox(xlims, ylims):
    plt.fill([xlims[0],xlims[1],xlims[1],xlims[0],xlims[0]],\
        [ylims[0],ylims[0],ylims[1],ylims[1],ylims[0]],\
        color='0.5',alpha=0.5)

# Plot formatting options
days = mdates.DayLocator()   # every year
hours = mdates.HourLocator()  # every month

if plot:
    plt.figure(figsize=(9,8))
for ncgroup in ncgroups:
    ds = xray.open_dataset(ncfile, group=ncgroup)
    dsu = xray.open_dataset(uvfile, group=ncgroup[0:2])
    
    # Extract the barotropic tide
    ubar = dsu.Au_n[:,0].values
    vbar = dsu.Av_n[:,0].values

    # Rotate along the main axis
    tide_angle = -42*np.pi/180.

    # Note this is a clockwise rotation matrix
    ubar_pr = ubar*np.cos(tide_angle) + vbar*np.sin(tide_angle)
    vbar_pr = -ubar*np.sin(tide_angle) + vbar*np.cos(tide_angle)

    ubar_pr[np.abs(ubar_pr)>10.] = 0.

    U = om.OceanMooring(dsu.time.values, ubar_pr, 0.)
    V = om.OceanMooring(dsu.time.values, vbar_pr, 0.)

    utide = U.tidefit(['M2','S2','K1','O1','P1','Q1','N2','K2'])
    vtide = V.tidefit(['M2','S2','K1','O1','P1','Q1','N2','K2'])

    utidefit = utide[-2]
    vtidefit = vtide[-2]

    # hilbert transform the get the amplitude
    uh = hilbert(utidefit)
    uh = om.OceanMooring(dsu.time.values, uh, 0.).to_xray()

    # Find the max mode 1 and 2 waves
    numwaves = 10
    ## Find the peak mode-one waves of depression
    mode = 1
    wavefunc = 'max'
    window=3

    peaks, tpeakm2, ctr = get_peak_window(ds.A_n[:,mode].values, ds.time.values,\
                    360, numwaves, fun=wavefunc, ctr=0, vals=[], idxs=[])

    peak_mode2= ds.A_n.sel(time=tpeakm2)[:,mode].values

    mode = 0
    wavefunc = 'min'
    window=3

    peaks, tpeakm1, ctr = get_peak_window(ds.A_n[:,mode].values, ds.time.values,\
                    360, numwaves, fun=wavefunc, ctr=0, vals=[], idxs=[])

    peak_mode1= ds.A_n.sel(time=tpeakm1)[:,mode].values

    #print 'Mode 1:'
    #print 'time,A,A_tide,scale,ubar,r10,cn,tau_s'
    #for tt in tpeakm1: 
    #    print_max_params(ds, uh, tt, 0)


    #print 'Mode 2:'
    #print 'time,A,A_tide,scale,ubar,r10,cn,tau_s'
    #for tt in tpeakm2: 
    #    print_max_params(ds, uh, tt, 1)

    if plot:
        # Plot u_bar
        ax1=plt.subplot2grid((6,2),(0,0),rowspan=2)
        plt.plot(dsu.time, utidefit, '0.2', lw=0.5)
        #plt.plot(dsu.time, ubar_pr)
        #plt.plot(dsu.time, np.abs(uh), 'k', lw=0.5)

        plt.grid(b=True)
        plt.xlim(xlims)
        plt.ylim(-0.45,0.45)
        plt.ylabel('$U_0$ [m s$^{-1}$]')
        ax1.text(0,1.04,'(a)', transform=ax1.transAxes)
        ax1.set_yticks([-0.4,-0.2,0,0.2,0.4])


        # Plot Amp_M2 (mode 1 and 2)
        ax2=plt.subplot2grid((6,2),(2,0),rowspan=2)

        ds.A_n[:,0].plot(color='b', lw=0.2)
        ds.Atide[:,0].plot(ls='--', color='m', lw=0.7)
        plt.ylabel('Mode 1 - Amp [m]')
        plt.title('')
        plt.grid(b=True)
        plt.xlim(xlims)
        plt.ylim(ylims)
        ax2.text(0,1.04,'(b)', transform=ax2.transAxes)
        plotbox(xlims1, ylims)
        plt.text(xlims1[1],40,'see (d)')
        plt.plot(tpeakm1, peak_mode1 ,'kd')

        # Plot Amp_M2 (mode 1 and 2)
        ax3=plt.subplot2grid((6,2),(0,1),rowspan=3)

        #ds.A_n[:,0].plot(color='b', lw=0.5)
        #ds.Atide[:,0].plot(ls='--', color='m', lw=0.7)
        plt.plot(ds.A_n.time.values,ds.A_n.values[:,0], color='b',lw=0.5)
        plt.plot(ds.Atide.time.values,ds.Atide.values[:,0], ls='--', color='m', lw=0.7)
        #plt.ylabel('Amplitude [m]')
        plt.ylabel('Mode 1 - Amp [m]')
        plt.title('')
        plt.grid(b=True)
        plt.xlim(xlims1)
        plt.ylim(ylims)
        ax3.text(0,1.04,'(d)', transform=ax3.transAxes)


        days = mdates.DayLocator()   # every year
        hours = mdates.HourLocator()  # every month
        daysFmt = mdates.DateFormatter('%Y-%m-%d')
        ax3.xaxis.set_major_locator(days)
        ax3.xaxis.set_major_formatter(daysFmt)
        ax3.xaxis.set_minor_locator(hours)

        # r_10 parameter
        # Plot Amp_M2 (mode 1 and 2)
        ax4=plt.subplot2grid((6,2),(4,0),rowspan=2)

        ds.r10[:,0].plot(color='r', lw=0.5)
        #ds.A_n[:,1].plot(color='r', lw=0.2)
        #ds.Atide[:,1].plot(ls='--', color='m', lw=0.7)
        #plt.ylabel('Mode 2 - Amp [m]')
        plt.ylabel('Nonlinearity [m$^{-1}$]')
        plt.title('')
        plt.grid(b=True)
        plt.xlim(xlims)
        #plt.ylim(ylims)
        ax4.text(0,1.04,'(c)', transform=ax4.transAxes)

        # Plot Amp_M2 (mode 1 and 2)
        ax5=plt.subplot2grid((6,2),(3,1),rowspan=3)

        # Plot a histogram of the outputs
        ds.A_n[:,0].plot.hist(bins=np.arange(-75,75), normed=True)

        ##ds.A_n[:,1].plot(color='r', lw=0.5)
        #plt.plot(ds.A_n.time.values,ds.A_n.values[:,1], color='r',lw=0.5)
        #plt.plot(ds.Atide.time.values,ds.Atide.values[:,1], ls='--', color='m', lw=0.7)
        ##ds.Atide[:,1].plot(ls='--', color='m', lw=0.7)
        #plt.ylabel('Mode 2 - Amp [m]')
        #plt.title('')
        #plt.grid(b=True)
        #plt.xlim(xlims2)
        #plt.ylim(ylims)
        ax5.text(0,1.04,'(e)', transform=ax5.transAxes)

        #days = mdates.DayLocator()   # every year
        #hours = mdates.HourLocator()  # every month
        #daysFmt = mdates.DateFormatter('%Y-%m-%d')
        #ax5.xaxis.set_major_locator(days)
        #ax5.xaxis.set_major_formatter(daysFmt)
        #ax5.xaxis.set_minor_locator(hours)

if plot:
    # Try and get some tick labels back
    #ax3.set_xticklabels(ax3.get_xticklabels(), visible=True)
    #ax4.set_xticklabels(ax4.get_xticklabels())
    plt.setp(ax3.get_xticklabels(), visible=True)
    plt.setp(ax4.get_xticklabels(), visible=True, rotation=30, ha='right')

    plt.tight_layout()

    plt.savefig('%s.png'%outfile,dpi=150)
    plt.savefig('%s.pdf'%outfile,dpi=150)

    # Conditional probability plot
    #
    mode = 0
    r10 = ds.r10.sel(timeslow=ds.time, method='nearest')
    plt.figure()
    plt.subplot(211)
    plt.plot(r10[:,0], ds.A_n[:,0],'.')
    plt.subplot(212)
    plt.plot(r10[:,1], ds.A_n[:,1],'.')

    # Plot a few relationship
    plt.show()


