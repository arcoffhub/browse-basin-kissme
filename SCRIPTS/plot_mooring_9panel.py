"""
Plot the mooring data
"""

import matplotlib.pyplot as plt
import numpy as np

from mycurrents import oceanmooring as om

from netCDF4 import Dataset
from datetime import datetime, timedelta

import xray

import pdb

def plot_site(site, time1, time2):
    #####
    Tfile = 'Data/NetCDF/KISSME_Gridded_Mooring_TP_60sec.nc'
    #ampfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Fitted_Buoyancy_wout_motion.nc'
    ampfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Fitted_Buoyancy_wout_motion_unvenfilt.nc'

    if site=='WP':
        group1 = 'WP250_RDI_150_kHz_Quartermaster_11795'
        group2 = 'WP250_RDI_300_kHz_Sentinel_20092'
        Tgroup = 'WP250'
    elif site=='SP':
        group1 = 'SP250_RDI75_kHz_Longranger_24613'
        group2 = None
        Tgroup = 'SP250'
    elif site=='NP':
        group1 = 'NP250_RDI_150_kHz_Quartermaster_16856'
        group2 = 'NP250_RDI_300_kHz_Monitor_20089'
        Tgroup = 'NP250'


    #####
    # List the groups in the file
    #nc = Dataset(adcpfile)
    #for gg in nc.groups.keys():
    #    print gg
    #nc.close()

    # Load the moorings
    T = om.from_netcdf(Tfile, 'temperature', group=Tgroup)

    ds = xray.open_dataset(ampfile, group=Tgroup)

    # Clip the data for plotting
    T_p = T.clip(time1, time2)


    idx = ~np.any(T_p.y.mask, axis=1)
    T_p1 = om.OceanMooring(T_p.t, T_p.y[idx,:], T_p.Z[idx], zvar=T_p.zvar[idx,:])

    tstack = [time1+timedelta(hours=0.25) for ii in T_p1.Z]

    # Plot
    #plt.figure()
    T_p1.contourf(np.arange(12,31,1), cbar=False)
    T_p1.contourf(np.arange(12,31,1), cbar=False, filled=False)
    
    plt.plot(tstack, T_p.zvar[idx,0],'kd', ms=1)

    #u_p1.contourf(ulevs, cbar=True, filled=True, cmap='RdBu')
    #if group2 is not None:
    #    u_p2.contourf(ulevs, cbar=False, filled=True, cmap='RdBu')

    plt.ylim(0,250)
    plt.gca().invert_yaxis()
    plt.ylabel('Depth [m]')
    #plt.grid(b=True)
    plt.xlabel('Time [HH:MM after %s]'%datetime.strftime(time1, '%d-%B-%Y'))
    #plt.show()

    return ds

def plot_time(t1,t2, ax1, ax2, ax3, ax4, ii, mode):

    #fig = plt.figure(figsize=(12,9))

    #ax1 = plt.subplot(313)
    plt.sca(ax1)
    ds1 = plot_site('NP', t1,t2)
    plt.text(0.05 ,0.9, '(%s) NP250'%(chr(ii+1+96)), transform =ax1.transAxes)
    ax1.set_xticklabels([])
    plt.xlabel('')

    #ax2 = plt.subplot(312)
    plt.sca(ax2)
    ds2=plot_site('WP', t1,t2)
    plt.text(0.05 ,0.9, '(%s) WP250'%(chr(ii+4+96)), transform =ax2.transAxes)
    ax2.set_xticklabels([])
    plt.xlabel('')

    #ax3 = plt.subplot(311)
    plt.sca(ax3)
    ds3=plot_site('SP', t1,t2)
    plt.text(0.05 ,0.9, '(%s) SP250'%(chr(ii+7+96)), transform =ax3.transAxes)
    #ax3.set_xticklabels([])
    plt.xlabel('')


    plt.sca(ax4)
    ds1.A_n[:,mode].plot(color='g')
    ds2.A_n[:,mode].plot(color='r')
    ds3.A_n[:,mode].plot(color='y')
    if ii ==2:
        plt.legend(('NP','WP','SP'), loc='lower right')
    ax4.set_xlim(t1,t2)
    ax4.set_ylim(-70,70)
    ax4.set_ylabel('Amp [m]')
    ax4.set_title('')
    plt.grid(b=True)
    plt.text(0.05 ,0.9, '(%s)'%(chr(ii+10+96)), transform =ax4.transAxes)
    #t1s = datetime.strftime(t1, '%Y%m%d%H%M%S')
    #t2s = datetime.strftime(t2, '%Y%m%d%H%M%S')
    #outfile = 'FIGURES/kissme_3moorsnaps_%s_%s.png'%(t1s, t2s)
    #print outfile
    #plt.savefig(outfile, dpi=150)

    #plt.show()
    #del fig

########
#
time1 = [
    #datetime(2017,4,2,17,0,0),\
    #datetime(2017,4,3,7,0,0),\
    #datetime(2017,4,4,7,0,0),\
    datetime(2017,4,18,21,30,0),\
    datetime(2017,4,30,9,0,0),\
    datetime(2017,5,6,7,0,0),\
    #datetime(2017,4,22,10,0,0),\
    #datetime(2017,4,30,10,40,0),\
    #datetime(2017,5,6,7,10,0),\
    #datetime(2017,4,20,8,0,0),\
    #datetime(2017,4,22,10,0,0),\
    #datetime(2017,5,4,3,30,0),\

]

time2 = [
    #datetime(2017,4,2,23,0,0),\
    #datetime(2017,4,3,12,0,0),\
    #datetime(2017,4,4,12,0,0),\
    datetime(2017,4,19,02,30,0),\
    datetime(2017,4,30,15,40,0),\
    datetime(2017,5,6,12,10,0),\
    #datetime(2017,4,22,12,0,0),\
    #datetime(2017,4,30,12,10,0),\
    #datetime(2017,5,6,8,10,0),\
    #datetime(2017,4,20,15,0,0),\
    #datetime(2017,4,22,14,0,0),\
    #datetime(2017,5,4,7,30,0),\

]

#mode = 1
#outfiletmp = 'FIGURES/kissme_9panel_uneven_eg2_mode_%d'

mode = 1
outfiletmp = 'FIGURES/kissme_9panel_uneven_eg1_mode_%d'


fig = plt.figure(figsize=(12,9))
ii = 0
for t1, t2 in zip(time1, time2):
    ax1 = plt.subplot(4,3,1+ii)
    ax2 = plt.subplot(4,3,4+ii)
    ax3 = plt.subplot(4,3,7+ii)
    ax4 = plt.subplot(4,3,10+ii)
    
    plot_time(t1, t2, ax1, ax2, ax3, ax4, ii, mode)
    #plot_time('v', t1, t2)
    #plot_time('w', t1, t2)
    #plot_time('errvel', t1, t2)

    if ii > 0:
        ax1.set_ylabel('')
        ax1.set_yticklabels([])
        ax2.set_ylabel('')
        ax2.set_yticklabels([])
        ax3.set_ylabel('')
        ax3.set_yticklabels([])
        ax4.set_ylabel('')
        ax4.set_yticklabels([])

    ii+=1


plt.tight_layout()
plt.subplots_adjust(hspace=0.09, wspace=0.14)

outfile = outfiletmp%(mode+1)
print outfile
plt.savefig(outfile+'.png', dpi=150)
plt.savefig(outfile+'.pdf', dpi=150)

plt.show()
