"""
Calculate and plot the shear and strain for extreme waves from each mooring

TO BE COMPLETED
"""

import xray
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from datetime import timedelta

from iwaves.utils.minmax import get_peak_window

from mycurrents import oceanmooring as om
import soda.utils.mynumpy as mynp

def calc_shear_strain(timewave, window, ds, dsb):
    # Extract the time +/- window hours
    t1 = timewave - np.timedelta64(window,'h')
    t2 = timewave + np.timedelta64(window,'h')

    t1s = pd.to_datetime(str(t1)).strftime('%Y.%m.%d %H:%M:%S')
    t2s = pd.to_datetime(str(t2)).strftime('%Y.%m.%d %H:%M:%S')

    dswave = ds.sel(time=slice(t1s,t2s))
    dsbwave = dsb.sel(time=slice(t1s,t2s))

    # Select the nearest time to grab phi
    phiwave = dsb.phi.sel(timeslow=t1s, method='nearest')
    N2wave = dsb.N2.sel(timeslow=t1s, method='nearest')

    #dz = phiwave.z[1] - phiwave.z[0]
    zmodes = phiwave.z.values[:,np.newaxis]*np.ones_like(phiwave.modes.values)
    phi_z = mynp.grad_z(phiwave.values.T, zmodes)

    strain_n = dsbwave.A_n.values[:,np.newaxis,:]*phi_z
    strain_all = strain_n.sum(axis=-1)

    # Calculate the instantaneous N2
    N2_i = N2wave.values[np.newaxis,:]*(1. - strain_all)

    # Return variables as OceanMooring objects
    rho_om = om.OceanMooring(dsbwave.time.values, dsbwave.rhofit.values, dsbwave.z.values)
    strain_om = om.OceanMooring(dsbwave.time.values, strain_all, dsbwave.z.values)
    N2i_om = om.OceanMooring(dsbwave.time.values, N2_i, dsbwave.z.values)


    # Calculate the shear and vertical divergence (strain)
    u_om = om.OceanMooring(dswave.time.values, dswave.ufit.values, dswave.z.values)
    u_z = om.OceanMooring(dswave.time.values, u_om.grad_z(), dswave.z.values)


    v_om = om.OceanMooring(dswave.time.values, dswave.vfit.values, dswave.z.values)
    v_z = om.OceanMooring(dswave.time.values, v_om.grad_z(), dswave.z.values)

    S2_om = om.OceanMooring(dswave.time.values, v_z.y**2 + u_z.y**2, dswave.z.values)

    w_om = om.OceanMooring(dswave.time.values, dswave.wfit.values, dswave.z.values)
    w_z = om.OceanMooring(dswave.time.values, w_om.grad_z(), dswave.z.values)


    Ri_om = om.OceanMooring(dsbwave.time.values, \
                N2wave.values[np.newaxis,::-1]/S2_om.y.T, dswave.z.values)
    
    return rho_om, strain_om, S2_om, w_z, Ri_om, dswave, dsbwave

def plot_shear_strain(rho_om, strain_om, S2_om, w_z, Ri_om, dswave, dsbwave):
    plt.figure(figsize=(8,10))

    ax0 = plt.subplot(511)
    dsbwave.A_n[:,0].plot(color='b')
    dsbwave.A_n[:,1].plot(color='r')
    ax0.set_ylim(-50,50)
    plt.grid(b=True)
    ax0.set_xticklabels([])
    plt.title('')
    plt.ylabel('Amplitude [m]')
    plt.xlabel('')

    ### Strain
    ax = plt.subplot(512)
    #dsbwave.rhofit.T.plot.contour(levels=np.arange(1021,1028,0.25), \
    #            colors='k', linewidths=0.25)
    C, cb = strain_om.contourf(clevs=np.linspace(-2,2,21),\
            cmap='RdGy', filled=True, cbar=True,)

    rho_om.contourf(clevs=np.arange(1021,1028,0.25), filled=False, cbar=False, \
                colors='k', linewidths=0.25)

    cb.ax.set_title('$\eta_z$')
    ax.set_xticklabels([])
    plt.ylabel('Depth [m]')

    ### Shear
    ax = plt.subplot(513)
    C, cb = S2_om.contourf(clevs=np.linspace(0,2e-3,11),\
            cmap='bone_r', filled=True, cbar=True, extend='max')

    rho_om.contourf(clevs=np.arange(1021,1028,0.25), filled=False, cbar=False, \
                colors='k', linewidths=0.25)

    cb.ax.set_title('$S^2$ [s$^{-2}$]')
    ax.set_xticklabels([])
    plt.ylabel('Depth [m]')

    ### W_z
    ax = plt.subplot(514)
    C, cb = w_z.contourf(clevs=np.linspace(-1e-2, 1e-2,21),\
            cmap='PuOr', filled=True, cbar=True, extend='both')

    rho_om.contourf(clevs=np.arange(1021,1028,0.25), filled=False, cbar=False, \
                colors='k', linewidths=0.25)

    cb.ax.set_title('$w_z$ [s$^{-1}$]')
    ax.set_xticklabels([])
    plt.ylabel('Depth [m]')

    ### Ri
    ax = plt.subplot(515)
    #C, cb = N2i_om.contourf(clevs=np.linspace(0,1e-3,11),\
    #        cmap='Blues', filled=True, cbar=True, extend='max')
    C, cb = Ri_om.contourf(clevs=[0,0.25,0.5,1.,2.],\
            cmap='afmhot', filled=True, cbar=True, extend='max')

    rho_om.contourf(clevs=np.arange(1021,1028,0.25), filled=False, cbar=False, \
                colors='k', linewidths=0.25)

    #cb.ax.set_title('$N_i^2$ [s$^{-2}$]')
    cb.ax.set_title('$Ri$')
    plt.ylabel('Depth [m]')

    plt.tight_layout()

    pos1 = ax0.get_position()
    pos2 = ax.get_position()

    ax0.set_position([pos1.x0 , pos1.y0 ,  pos2.width, pos1.height])
    ax0.set_xlim(dswave.time.values[0],dswave.time.values[-1])


################
bfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Fitted_Buoyancy_Combined.nc'
#uvfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Fitted_Velocity_short.nc'
uvfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Fitted_Velocity.nc'

ugroup = 'NP'
bgroup = 'NP250'
####

# Load the dataset objects
ds = xray.open_dataset(uvfile, group = ugroup)
dsb = xray.open_dataset(bfile, group = bgroup)

# Flip the velocity depths
ds.z.values[:] = ds.z.values[::-1]

## Find the peak mode-one waves of depression
mode = 0
wavefunc = 'min'
window=3

peaks, tpeakmin, ctr = get_peak_window(dsb.A_n[:,mode].values, dsb.time.values,\
                360, 15, fun=wavefunc, ctr=0, vals=[], idxs=[])

peak_min = dsb.A_n.sel(time=tpeakmin)[:,mode].values


for wavenum in range(0,12):
    timewave = tpeakmin[wavenum]

    outfile = 'FIGURES/ShearStrainPlots/IW_ShearStrain_rank%02d_Mode%d_type_%s_%s_%s.png'%\
        (wavenum, mode+1, wavefunc, \
            pd.to_datetime(str(timewave)).strftime('%Y%m%d%H%M'), bgroup)

    print outfile

    try:
        rho_om, strain_om, S2_om, w_z, Ri_om, dswave, dsbwave\
                = calc_shear_strain(timewave, window, ds, dsb)
        plot_shear_strain(rho_om, strain_om, S2_om, w_z, Ri_om, dswave, dsbwave)

        plt.savefig(outfile,dpi=150)
        plt.show()
    except:
        print 'Failed to plot...'

