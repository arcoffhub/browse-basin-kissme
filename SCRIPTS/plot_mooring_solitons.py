"""
Plot the mooring data
"""

import matplotlib.pyplot as plt
import numpy as np

from mycurrents import oceanmooring as om

from netCDF4 import Dataset
from datetime import datetime

def plot_site(site, uvar, time1, time2):
    #####
    adcpfile = 'Data/NetCDF/KISSME2017_RDI_QAQC.nc'
    Tfile = 'Data/NetCDF/KISSME_Gridded_Mooring_TP_60sec.nc'

    if site=='WP':
        group1 = 'WP250_RDI_150_kHz_Quartermaster_11795'
        group2 = 'WP250_RDI_300_kHz_Sentinel_20092'
        Tgroup = 'WP250'
    elif site=='SP':
        group1 = 'SP250_RDI75_kHz_Longranger_24613'
        group2 = None
        Tgroup = 'SP250'
    elif site=='NP':
        group1 = 'NP250_RDI_150_kHz_Quartermaster_16856'
        group2 = 'NP250_RDI_300_kHz_Monitor_20089'
        Tgroup = 'NP250'


    if uvar in ['u','v']:
        ulevs = np.arange(-0.8,0.9,0.10)
    elif uvar in ['w', 'errvel']:
        ulevs = np.arange(-0.2,0.22,0.02)


    #####
    # List the groups in the file
    #nc = Dataset(adcpfile)
    #for gg in nc.groups.keys():
    #    print gg
    #nc.close()

    # Load the moorings
    u1 = om.from_netcdf(adcpfile, uvar, group=group1)
    if group2 is not None:
        u2 = om.from_netcdf(adcpfile, uvar, group=group2)
    T = om.from_netcdf(Tfile, 'temperature', group=Tgroup)

    # Clip the data for plotting
    u_p1 = u1.clip(time1, time2)
    if group2 is not None:
        u_p2 = u2.clip(time1, time2)
    T_p1 = T.clip(time1, time2)

    # Plot
    #plt.figure()
    T_p1.contourf(np.arange(12,31,1), cbar=False, filled=False)

    u_p1.contourf(ulevs, cbar=True, filled=True, cmap='RdBu')
    if group2 is not None:
        u_p2.contourf(ulevs, cbar=False, filled=True, cmap='RdBu')

    plt.ylim(0,250)
    plt.gca().invert_yaxis()
    plt.ylabel('Depth [m]')
    plt.grid(b=True)
    plt.xlabel('Time [HH:MM after %s]'%datetime.strftime(time1, '%d-%B-%Y'))
    #plt.show()

def plot_time(uvar, t1,t2):

    fig = plt.figure(figsize=(12,9))

    ax1 = plt.subplot(313)
    plot_site('SP', uvar, t1,t2)
    plt.text(0.05 ,0.9, '(a) SP250', transform =ax1.transAxes)
    #ax1.set_xticklabels([])

    ax2 = plt.subplot(312, sharex=ax1, sharey=ax1)
    plot_site('WP', uvar, t1,t2)
    plt.text(0.05 ,0.9, '(b) WP250', transform =ax2.transAxes)
    #ax2.set_xticklabels([])
    plt.xlabel('')

    ax3 = plt.subplot(311, sharex=ax1, sharey=ax1)
    plot_site('NP', uvar, t1,t2)
    plt.text(0.05 ,0.9, '(c) NP250', transform =ax3.transAxes)
    #ax3.set_xticklabels([])
    plt.xlabel('')

    plt.tight_layout()

    t1s = datetime.strftime(t1, '%Y%m%d%H%M%S')
    t2s = datetime.strftime(t2, '%Y%m%d%H%M%S')
    outfile = 'FIGURES/kissme_3moorsnaps_%s_%s_%s.png'%(t1s, t2s, uvar)
    print outfile
    plt.savefig(outfile, dpi=150)

    #plt.show()
    del fig

########
#
time1 = [
    #datetime(2017,4,30,0,0,0),\
    #datetime(2017,4,2,0,0,0),\
    #datetime(2017,4,7,0,0,0),\
    #datetime(2017,4,12,0,0,0),\
    #datetime(2017,4,17,0,0,0),\
    #datetime(2017,4,22,0,0,0),\
    #datetime(2017,4,27,0,0,0),\
    #datetime(2017,4,2,22,0,0),\
    #datetime(2017,4,2,17,0,0),\
    datetime(2017,4,3,7,0,0),\
    datetime(2017,4,3,17,0,0),\
    datetime(2017,4,4,6,0,0),\
    datetime(2017,4,4,20,0,0),\
    datetime(2017,4,17,0,0,0),\
    datetime(2017,4,22,10,0,0),\
    datetime(2017,4,23,9,0,0),\
    datetime(2017,4,30,9,0,0),\
    datetime(2017,5,3,12,0,0),\
    datetime(2017,5,4,0,0,0),\
]

time2 = [
    #datetime(2017,5,4,0,0,0),\
    #datetime(2017,4,7,0,0,0),\
    #datetime(2017,4,12,0,0,0),\
    #datetime(2017,4,17,0,0,0),\
    #datetime(2017,4,22,0,0,0),\
    #datetime(2017,4,27,0,0,0),\
    #datetime(2017,5,1,0,0,0),\
    #datetime(2017,4,3,11,0,0),\
    #datetime(2017,4,3,0,0,0),\
    datetime(2017,4,3,12,0,0),\
    datetime(2017,4,3,22,0,0),\
    datetime(2017,4,4,11,0,0),\
    datetime(2017,4,5,1,0,0),\
    datetime(2017,4,17,5,0,0),\
    datetime(2017,4,22,15,0,0),\
    datetime(2017,4,23,14,0,0),\
    datetime(2017,4,30,14,0,0),\
    datetime(2017,5,3,17,0,0),\
    datetime(2017,5,4,6,0,0),\
]

for t1, t2 in zip(time1, time2):
    plot_time('u', t1, t2)
    plot_time('v', t1, t2)
    plot_time('w', t1, t2)
    plot_time('errvel', t1, t2)
