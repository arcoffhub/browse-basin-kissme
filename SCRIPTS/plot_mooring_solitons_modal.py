"""
Calculate and plot the shear and strain for extreme waves from each mooring

TO BE COMPLETED
"""

import xray
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from datetime import timedelta

from iwaves.utils.minmax import get_peak_window

from mycurrents import oceanmooring as om
import soda.utils.mynumpy as mynp

def calc_uvwb(timewave, window, ds, dsb, rotangle):
    # Extract the time +/- window hours
    t1 = timewave - np.timedelta64(window,'h')
    t2 = timewave + np.timedelta64(window,'h')

    t1s = pd.to_datetime(str(t1)).strftime('%Y.%m.%d %H:%M:%S')
    t2s = pd.to_datetime(str(t2)).strftime('%Y.%m.%d %H:%M:%S')

    dswave = ds.sel(time=slice(t1s,t2s))
    dsbwave = dsb.sel(time=slice(t1s,t2s))


    # Return variables as OceanMooring objects
    rho_om = om.OceanMooring(dsbwave.time.values, dsbwave.rhofit.values, dsbwave.z.values)


    # Calculate the shear and vertical divergence (strain)
    u_om = om.OceanMooring(dswave.time.values, dswave.ufit.values, dswave.z.values)

    v_om = om.OceanMooring(dswave.time.values, dswave.vfit.values, dswave.z.values)

    w_om = om.OceanMooring(dswave.time.values, dswave.wfit.values, dswave.z.values)

    # Rotate the velocities (CW rotation matrix)
    urot_om = om.OceanMooring(dswave.time.values,\
        dswave.ufit.values*np.cos(rotangle) + dswave.vfit.values*np.sin(rotangle),\
        dswave.z.values)

    dswave['Aurot_n'] = dswave.Au_n*np.cos(rotangle) + dswave.Av_n*np.sin(rotangle)
    

    return rho_om, urot_om, v_om, w_om, dswave, dsbwave

def plot_ub(rho_om, u_om, dswave, dsbwave,mode):
    plt.figure(figsize=(6,5))

    ax0 = plt.subplot2grid((4,1),(3,0),rowspan=1)
    dsbwave.A_n[:,mode].plot(color='b')
    #dsbwave.A_n[:,1].plot(color='r')
    #dswave.Au_n[:,mode+1].plot(color='b',ls='--')
    #dswave.Aurot_n[:,mode+1].plot(color='b',ls='--')
    
    if mode==0:
        ax0.set_ylim(-70,70)
    elif mode==1:
        ax0.set_ylim(-50,50)
    plt.grid(b=True)
    #ax0.set_xticklabels([])
    plt.title('')
    plt.ylabel('A [m]')
    plt.xlabel('')
    plt.text(0.1,0.8,'(b)', transform=ax0.transAxes)

    plt.xlabel('Time in 2017 [mm-dd HH]')

    ### Velocity
    ax = plt.subplot2grid((4,1),(0,0),rowspan=3)
    #dsbwave.rhofit.T.plot.contour(levels=np.arange(1021,1028,0.25), \
    #            colors='k', linewidths=0.25)
    C, cb = u_om.contourf(clevs=np.linspace(-1.,1.,21),\
            cmap='RdBu', filled=True, cbar=True,)

    rho_om.contourf(clevs=np.arange(1021,1028,0.25), filled=False, cbar=False, \
                colors='k', linewidths=0.25)

    cb.ax.set_title('$u^{\prime}$ [m/s]')
    ax.set_xticklabels([])
    plt.ylabel('Depth [m]')
    plt.text(0.1,0.9,'(a)', transform=ax.transAxes)

    plt.tight_layout()

    pos1 = ax0.get_position()
    pos2 = ax.get_position()

    ax0.set_position([pos1.x0 , pos1.y0 ,  pos2.width, pos1.height])
    ax0.set_xlim(dswave.time.values[0],dswave.time.values[-1])


################
bfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Fitted_Buoyancy_Combined.nc'
#uvfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Fitted_Velocity_short.nc'
uvfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Fitted_Velocity.nc'

ugroup = 'NP'
bgroup = 'NP250'

rotangle = -50.*np.pi/180 
####

# Load the dataset objects
ds = xray.open_dataset(uvfile, group = ugroup)
dsb = xray.open_dataset(bfile, group = bgroup)

# Flip the velocity depths
ds.z.values[:] = ds.z.values[::-1]

## Find the peak mode-one waves of depression
mode = 1
wavefunc = 'max'
#mode = 0
#wavefunc = 'min'

window=3

peaks, tpeakmin, ctr = get_peak_window(dsb.A_n[:,mode].values, dsb.time.values,\
                360, 15, fun=wavefunc, ctr=0, vals=[], idxs=[])

peak_min = dsb.A_n.sel(time=tpeakmin)[:,mode].values


#for wavenum in [2,9,10,11]:
#for wavenum in [1,2,3]:
for wavenum in range(12):
    timewave = tpeakmin[wavenum]

    outfile = 'FIGURES/ModeAmp/IW_ubmodes_rank%02d_Mode%d_type_%s_%s_%s.png'%\
        (wavenum, mode+1, wavefunc, \
            pd.to_datetime(str(timewave)).strftime('%Y%m%d%H%M'), bgroup)

    try:
        print outfile

        rho_om, u_om, v_om, w_om, dswave, dsbwave\
            = calc_uvwb(timewave, window, ds, dsb, rotangle)
        plot_ub(rho_om, u_om, dswave, dsbwave, mode)

        plt.savefig(outfile,dpi=150)
        plt.show()
    except:
        print 'Failed'

