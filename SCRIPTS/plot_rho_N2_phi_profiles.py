"""
Plot fitted buoyancy modes
"""

import xray
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime
from scipy.signal import hilbert

#from iwaves.utils.minmax import get_peak_window

from mycurrents import oceanmooring as om

####
ncfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Fitted_Buoyancy.nc'
#ncfile = '/home/suntans/Share/ARCHub/DATA/FIELD/ShellCrux/KP150_Fitted_Buoyancy.nc'
#
uvfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Fitted_Velocity.nc'

ncgroups = [\
    #'KP150_T',
    #'WP250',
    #'NP250',
    'SP250',
]

ncgroup = ncgroups[0]

#ncfile = '/home/suntans/Share/ARCHub/DATA/FIELD/ShellPreludeRPS/Prelude_Fitted_Buoyancy.nc'
#
#ncgroups = [\
#    'F_Block_2007_CM04',\
#    'F_Block_2008a_CM04',\
#    'F_Block_2008b_CM04',\
#    'F_Block_2009a_CM04',\
#    'F_Block_2009b_CM04',\
#]


times = [datetime(2017,4,3), datetime(2017,4,15), datetime(2017,4,30)]
colors=['b','r','k']

outfile = 'FIGURES/KISSME_stratification_examples'
#####

#for ncgroup in ncgroups:
ncgroup = ncgroups[0]
ds = xray.open_dataset(ncfile, group=ncgroup)
dsu = xray.open_dataset(uvfile, group=ncgroup[0:2])

z = ds.z.values

# Init the figure
plt.figure(figsize=(10,5))

ax1=plt.subplot(141)
ax2=plt.subplot(142)
ax3=plt.subplot(143)
ax4=plt.subplot(144)

ax2.set_yticklabels([])
ax3.set_yticklabels([])
ax4.set_yticklabels([])

ax1.set_ylabel('Depth [m]')
ax1.set_xlabel(r'$\bar{\rho}(z)$ -1000 [kg m$^{-3}$]')
ax2.set_xlabel('$N(z)$ [s$^{-1}$]')
ax3.set_xlabel('$\phi_1(z)$')
ax4.set_xlabel('$\phi_2(z)$')

ax1.set_xlim(21,28)
ax2.set_xlim(0,0.025)
ax3.set_xlim(-1.1,1.1)
ax4.set_xlim(-1.1,1.1)

ax1.text(-0.01,1.01,'(a)',transform=ax1.transAxes)
ax2.text(-0.01,1.01,'(b)',transform=ax2.transAxes)
ax3.text(-0.01,1.01,'(c)',transform=ax3.transAxes)
ax4.text(-0.01,1.01,'(d)',transform=ax4.transAxes)

ax1.grid(b=True)
ax2.grid(b=True)
ax3.grid(b=True)
ax4.grid(b=True)

for time,c in zip(times,colors):
    dsnow = ds.sel(timeslow=time, method='nearest')

    ax1.plot(dsnow.rhobar.values-1000, z, color=c)
    ax2.plot(np.sqrt(dsnow.N2.values), z, color=c)
    ax3.plot(dsnow.phi.values[0,:], z, color=c)
    ax4.plot(dsnow.phi.values[1,:], z, color=c)

# Add a legend
leglabels = [tt.strftime('%b-%d') for tt in times]
ax1.legend(leglabels)
plt.tight_layout()

#plt.savefig('%s.png'%outfile,dpi=150)
#plt.savefig('%s.pdf'%outfile,dpi=150)
plt.show()

    

