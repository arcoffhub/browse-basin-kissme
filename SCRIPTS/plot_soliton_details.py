"""
Plot the mooring data
"""

import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import numpy as np

from mycurrents import oceanmooring as om

from netCDF4 import Dataset
from datetime import datetime, timedelta
import pdb

def load_site(site, uvar, time1, time2):
    #####
    #adcpfile = 'Data/NetCDF/KISSME2017_RDI_QAQC.nc'
    adcpfile = 'Data/NetCDF/KISSME2017_RDI_QAQC_noerrvel.nc'
    #Tfile = 'Data/NetCDF/KISSME_Gridded_Mooring_TP_60sec.nc'
    Tfile = 'Data/NetCDF/KISSME_Gridded_Mooring_T_SBE56.nc'

    if site=='WP':
        group1 = 'WP250_RDI_150_kHz_Quartermaster_11795'
        group2 = 'WP250_RDI_300_kHz_Sentinel_20092'
        Tgroup = 'WP250'
    elif site=='SP':
        group1 = 'SP250_RDI75_kHz_Longranger_24613'
        group2 = None
        Tgroup = 'SP250'
    elif site=='NP':
        group1 = 'NP250_RDI_150_kHz_Quartermaster_16856'
        group2 = 'NP250_RDI_300_kHz_Monitor_20089'
        Tgroup = 'NP250'


    if uvar in ['u','v']:
        ulevs = np.arange(-0.8,0.9,0.10)
    elif uvar in ['w', 'errvel']:
        ulevs = np.arange(-0.2,0.22,0.02)


    #####
    # List the groups in the file
    #nc = Dataset(adcpfile)
    #for gg in nc.groups.keys():
    #    print gg
    #nc.close()

    # Load the moorings
    u1 = om.from_netcdf(adcpfile, uvar, group=group1)
    if group2 is not None:
        u2 = om.from_netcdf(adcpfile, uvar, group=group2)
    T = om.from_netcdf(Tfile, 'Temperature', group=Tgroup)

    T_p1 = T.clip(time1, time2)

    # Clip the data for plotting
    u_p1 = u1.clip(time1, time2)
    if group2 is not None:
        u_p2 = u2.clip(time1, time2)

    return T_p1, u_p1

def plot_time(uvar, t1,t2):

    fig = plt.figure(figsize=(12,9))

    ax1 = plt.subplot(313)
    plot_site('SP', uvar, t1,t2)
    plt.text(0.05 ,0.9, '(a) SP250', transform =ax1.transAxes)
    #ax1.set_xticklabels([])

    ax2 = plt.subplot(312, sharex=ax1, sharey=ax1)
    plot_site('WP', uvar, t1,t2)
    plt.text(0.05 ,0.9, '(b) WP250', transform =ax2.transAxes)
    #ax2.set_xticklabels([])
    plt.xlabel('')

    ax3 = plt.subplot(311, sharex=ax1, sharey=ax1)
    plot_site('NP', uvar, t1,t2)
    plt.text(0.05 ,0.9, '(c) NP250', transform =ax3.transAxes)
    #ax3.set_xticklabels([])
    plt.xlabel('')

    plt.tight_layout()

    t1s = datetime.strftime(t1, '%Y%m%d%H%M%S')
    t2s = datetime.strftime(t2, '%Y%m%d%H%M%S')
    outfile = 'FIGURES/kissme_3moorsnaps_%s_%s_%s.png'%(t1s, t2s, uvar)
    plt.savefig(outfile, dpi=150)

    #plt.show()
    del fig

########
#
def rotate_uv(ubar, vbar, tide_angle):
    # Note this is a clockwise rotation matrix
    ubar_pr = ubar*np.cos(tide_angle) + vbar*np.sin(tide_angle)
    vbar_pr = -ubar*np.sin(tide_angle) + vbar*np.cos(tide_angle)
    return ubar_pr, vbar_pr


#t1 = datetime(2017,4,30,11,52,0)
#t2 = datetime(2017,4,30,12,12,0)

#t1 = datetime(2017,4,18,22,0,0)
#t2 = datetime(2017,4,19,0,0,0)

#t1 = datetime(2017,4,22,12,0,0)
#t2 = datetime(2017,4,22,13,30,0)

t1 = datetime(2017,4,20,13,50,0)
t2 = datetime(2017,4,20,14,20,0)
outfile = 'FIGURES/mode2_soliton_SP_Apr20'

#t1 = datetime(2017,4,3,8,0,0)
#t2 = datetime(2017,4,3,9,0,0)

#t1 = datetime(2017,5,6,7,37,0)
#t2 = datetime(2017,5,6,8,14,0)
#outfile = 'FIGURES/mode2_soliton_SP_May5'

T, u = load_site('SP','u',t1,t2)
T, v = load_site('SP','v',t1,t2)
T, w = load_site('SP','w',t1,t2)
#Tnew = T.fill_gaps_z(kind='linear')

idx = ~np.any(T.y.mask, axis=1)
Tnew = om.OceanMooring(T.t, T.y[idx,:], T.Z[idx], )

Tnew.Z = -(Tnew.Z - 252.5)
tstack = [t1+timedelta(minutes=4) for ii in Tnew.Z]

#Tnew.positive = 'down'

tide_angle = -42*np.pi/180.
u_rot, v_rot = rotate_uv(u.y, v.y, tide_angle)
U = u.copy_like(u.t, u_rot)

## Plot

myFmt = mdates.DateFormatter('%H:%M')

plt.figure(figsize=(5,8))
ax=plt.subplot(211, facecolor='0.9')
Tnew.contourf(np.arange(12,31,1), cbar=False, filled=False)

ulevs = np.arange(-0.8,0.9,0.10)
C,cb = U.contourf(ulevs, cbar=True, filled=True, cmap='RdBu')
cb.ax.set_title('u [m s$^{-1}$]')

plt.plot(tstack, Tnew.Z, 'kd', ms=1)

plt.ylim(250,0)
#plt.gca().invert_yaxis()
plt.ylabel('Depth [m]')
plt.grid(b=True)
plt.xlabel('')
ax.set_xticklabels([])
plt.text(0.1,0.9,'(a)',transform=ax.transAxes)

ax2=plt.subplot(212, facecolor='0.9')
C,cb = Tnew.contourf(np.arange(12,31,1), cbar=False, filled=False)

wlevs = np.arange(-0.25,0.275,0.025)
C,cb = w.contourf(wlevs, cbar=True, filled=True, cmap='PuOr')
cb.ax.set_title('w [m s$^{-1}$]')

plt.ylim(250,0)
#plt.gca().invert_yaxis()
plt.ylabel('Depth [m]')
plt.grid(b=True)

plt.xticks(rotation=25)
plt.xlabel('Time [HH:MM after %s]'%datetime.strftime(t1, '%d-%B-%Y'))
ax2.xaxis.set_major_formatter(myFmt)

plt.text(0.1,0.9,'(b)',transform=ax2.transAxes)

plt.tight_layout()

plt.savefig(outfile+'.png', dpi=150)
plt.savefig(outfile+'.pdf', dpi=150)
plt.show()
