"""
Plot fitted buoyancy modes
"""

import xray
import numpy as np
import matplotlib.pyplot as plt

from iwaves.utils.minmax import get_peak_window

####
#ncfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Fitted_Buoyancy.nc'
ncfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Fitted_Velocity_short.nc'
#ncfile = '/home/suntans/Share/ARCHub/DATA/FIELD/ShellCrux/KP150_Fitted_Buoyancy.nc'
#
ncgroups = [\
    #'KP150_T',
    'WP',
    'NP',
    'SP',
]
#ncfile = '/home/suntans/Share/ARCHub/DATA/FIELD/ShellPreludeRPS/Prelude_Fitted_Buoyancy.nc'
#
#ncgroups = [\
#    'F_Block_2007_CM04',\
#    'F_Block_2008a_CM04',\
#    'F_Block_2008b_CM04',\
#    'F_Block_2009a_CM04',\
#    'F_Block_2009b_CM04',\
#]

#####



plt.figure(figsize=(7,10))
for ncgroup in ncgroups:
    ds = xray.open_dataset(ncfile, group=ncgroup)


    ## Find the peak mode-one waves of depression
    #peaks, tpeakmin, ctr = get_peak_window(ds.A_n[:,0].values, ds.time.values, 360, 5, fun='min', ctr=0, vals=[], idxs=[])
    #peak_min = ds.A_n.sel(time=tpeakmin)[:,0].values

    #peaks, tpeakmax, ctr = get_peak_window(ds.A_n[:,0].values, ds.time.values, 360, 5, fun='max', ctr=0, vals=[], idxs=[])
    #peak_max = ds.A_n.sel(time=tpeakmax)[:,0].values

    ## Find the peak mode-two waves of depression
    #peaks, tpeakmin2, ctr = get_peak_window(ds.A_n[:,1].values, ds.time.values, 360, 5, fun='min', ctr=0, vals=[], idxs=[])
    #peak_min2 = ds.A_n.sel(time=tpeakmin2)[:,1].values

    #peaks, tpeakmax2, ctr = get_peak_window(ds.A_n[:,1].values, ds.time.values, 360, 5, fun='max', ctr=0, vals=[], idxs=[])
    #peak_max2 = ds.A_n.sel(time=tpeakmax2)[:,1].values



    #ds.A_n[:,1].plot(lw=0.6)
    # Plot the internal tide signal
    ax = plt.subplot(311)
    ds.Avtide[:,1].plot(color='b',lw=0.6)

    ds.vamp[:,1,0].plot(color='r')
    ds.vamp[:,1,1].plot(color='m')
    plt.ylim(-60,60)
    plt.grid(b=True)
    plt.ylabel('Amp - harmonic [m]')
    plt.legend(('Total','M2','M4',))
    plt.title('')
        
    # Plot the mode-one amplitude    
    plt.subplot(312, sharex=ax)
    ds.Av_n[:,1].plot(color='b',lw=0.5)
    #plt.plot(tpeakmin, peak_min, 'd', color='0.5')
    #plt.plot(tpeakmax, peak_max, 'd', color='k')
    plt.ylim(-60,60)
    plt.grid(b=True)
    plt.ylabel('Amp - Mode 1 [m]')
    plt.title('')

    plt.subplot(313, sharex=ax)
    ds.Av_n[:,2].plot(color='r',lw=0.5)
    #plt.plot(tpeakmin2, peak_min2, 'd', color='0.5')
    #plt.plot(tpeakmax2, peak_max2, 'd', color='k')

    plt.ylim(-60,60)
    plt.grid(b=True)
    plt.ylabel('Amp - Mode 2 [m]')
    plt.title('')


#plt.tight_layout()
plt.show()
