"""
Parse a Vemco csv file
"""

import xray
import numpy as np

from netCDF4 import num2date
from datetime import datetime,timedelta
import os

from soda.dataio import mydbase

######
# Data parsing scripts
######

def hours2sec(x):
    return datetime.strptime(x,'%H:%M:%S') - datetime(1900,1,1)

def date2num(x):
    return datetime.strptime(x,'%Y-%m-%d')

def read_vemco(infile):
    """
    Read the csv file and return an xray.DataArray
    """
    headerlines = 8

    rec = np.genfromtxt(infile,
	    delimiter=',', skip_header=headerlines,
	    converters={1:hours2sec, 0:date2num})

    time = np.array([d+dt for d,dt,T in rec])
    temp = np.array([T for d,dt,T in rec])

    attrs = {'long_name':'Water temperature',
	    'units':'degC',
	    #'filename':infile,
	    }

    da = xray.DataArray(temp,
	    dims=('time',),
	    coords={'time':time},
	    attrs=attrs)
	
    return xray.Dataset({'temperature':da})


#infile = 'Data/Vemco/Minilog-II-T_355111.csv'
#da = read_vemco(infile)


#############
# Inputs

dbfile = 'Data/KISSME_Instruments.db'
table = 'Deployments'

#condition = 'StationID LIKE "%SBE56%"'
#condition = 'StationID LIKE "%SBE39%"'
#condition = 'StationID LIKE "%SBE%"'
condition = 'InstrumentType LIKE "%Vemco%"'
#condition = 'Site LIKE "%SCR200%"'

outfile = 'Data/NetCDF/KISSME_VemcoData.nc'
#########

# Query the database and get all of the sites
varnames = ['StationID','FileName',\
	'FilePath','Latitude',\
	'Longitude','InstrumentDepth',\
	'Depth','Site']

query = mydbase.returnQuery(dbfile, varnames, table, condition)
print 'Found stations:\n', query['StationID']

nfiles = len(query['FileName'])
mode='w'
for ii in range(nfiles):
    filename = '%s\%s'%(query['FilePath'][ii],query['FileName'][ii])
    filename =filename.replace('\\','/')

    print filename

    # Get the extension
    fname, ext = os.path.splitext(filename)

    # This returns an xray dataset object
    try:
        data = read_vemco(filename)
    except:
        print 'Unable to open file %s...'%filename
	continue


    # Fill in some extra info about the station
    #data.attrs.update({'stationname':query['Site'][ii]})
    #data.attrs.update({'original_file':query['FileName'][ii]})
    for vv in varnames:
        data.attrs.update({vv:query[vv][ii]})

    #stationname = '%s_%s_%dm'%(\
    #    query['Site'][ii].split('-')[0],\
    #    query['StationID'][ii].split('-')[0],\
    #    int(query['InstrumentDepth'][ii]),\
    #    )
    stationname = query['Site'][ii]

    data.attrs.update({'StationName':stationname})

    print data

    ## Save
    data.to_netcdf(outfile,group=stationname, mode=mode)
    mode = 'a'

    print '\tWritten to group: %s'%stationname

print 72*'#'
print 'done.'
print 72*'#'
