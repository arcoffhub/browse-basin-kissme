"""
Stack mooring observations together and store
"""

from soda.dataio import netcdfio
from mycurrents import oceanmooring as om

def grid_temp(stationid, stationname, \
	varname, sampledt, endafter, outfile, mode, exclude=''):

    #clip = False # Do not clip to bounding time region	
    clip = True

    dbfile = 'KISSME2017_Obs.db'
    tablename = 'observations'

    group = '%s'%stationid
    otherquery='StationID == "%s" AND time_end > "%s"'%\
	    (stationid, endafter)
    otherquery += exclude

    # Load the data
    temp = netcdfio.load_sql_ncstation(dbfile, stationname, varname,\
	    #otherquery=None)
	    otherquery=otherquery)

    # Sort by station names 
    # THis way the SBE37/9 go first (less memory)
    names = [ii.StationName for ii in temp]
    sortnames = names.sort()

    if len(names) == 0:
	raise Exception, 'could not find any stations.'

    for ii, name in enumerate(names):
	print 'Stacking instrument: %s...'%name
	for tt in temp:
	    if tt.StationName == name:
		tmpdata = om.from_xray(tt)
		if sampledt>0.:
		    print 'Resampling from dt = %3.1f to %3.1f'%(tmpdata.dt, sampledt)
		    tmp = tmpdata.resample(sampledt)
		    # Copy it back to an ocean mooring object
		    tmpdata = tmpdata.copy_like(tmp.t, tmp.y)

		if ii == 0:
		    data = tmpdata
		    data.verbose=True
		else:
		    data.vstack( tmpdata, clip=clip)
		print data.Z
	

    #for ii, tt in enumerate(temp):
    #    print tt.StationName
    #    if ii == 0:
    #        data = om.from_xray(tt)
    #    else:
    #        data.vstack( om.from_xray(tt), clip=True )


    # Change the station name and id
    data.StationID = stationid
    data.StationName = stationname

    # Write to output netcdf
    data.to_netcdf(outfile, group=group, mode=mode)
    print 'Done'

######
# Inputs
######
#stationnames = [
#	'BRB200_Vemco', # Seabirds don't match!!
#	'SP250_SBE',
#	'WP250_SBE',
#	'NP250_SBE',
#	'SP250-Lander_SBE'
#    ]

#stationnames = [
#	#'BRB200',
#	'SP250_SBE56',
#	'WP250_SBE56',
#	'NP250_SBE56',
#	'SP250-Lander_SBE56'
#    ]

stationnames = [
	'BRB200',
	#'SP250_SBE56',
	#'WP250_SBE56',
	'NP250_SBE',
	'SP250-Lander_SBE'
    ]

stationids = [
	'BRB200',
	#'SP250',
	#'WP250',
	'NP250',
	'SP250-Lander'
    ]

varname = 'temperature'

#varname = 'pressure'

sampledt = 60.
#sampledt = 0.

#endafter = "2017-05-01"
endafter = "2017-04-01"

# Exclude these from all gridding
exclude = ''

# BRB problem
#exclude = r' AND StationName NOT LIKE "%3975%"'
#exclude += r' AND StationName NOT LIKE "%8742%"'
##exclude += r' AND StationName NOT LIKE "%989%"' # SBE56
#exclude += r' AND StationName NOT LIKE "%RDI%"'
#exclude += r' AND StationName NOT LIKE "%357688%"'

mode = 'w'

#outfile = 'Data/NetCDF/KISSME_Gridded_Mooring_T_60sec.nc'
#outfile = 'Data/NetCDF/KISSME_Gridded_Mooring_P_60sec.nc'
#outfile = 'Data/NetCDF/KISSME_Gridded_Mooring_T_SBE56.nc'
outfile = 'Data/NetCDF/KISSME_Gridded_Mooring_T_misc.nc'

#######

for stationname, stationid in zip(stationnames, stationids):
    grid_temp(stationid, stationname,\
	varname, sampledt, endafter, outfile, mode, exclude=exclude)
    mode='a'
