
from mycurrents.adcpio import ADCP_io
#from mydolfyn.adp import api
from mydolfyn.adp._readbin import adcp_loader as api
from soda.dataio import mydbase

import xray
import os
from datetime import datetime, timedelta
from collections import OrderedDict
import numpy as np
from glob import glob

from mycurrents.oceanmooring import from_netcdf

#######
## Test the oceanmooring parser
########
ncfile = 'Data/NetCDF/KISSME2017_RDIADCPData.nc'
group = 'WP250_RDI_150_kHz_Quartermaster_11795' 

om = from_netcdf(ncfile, 'u', group=group)
########
## Test the binary reader
########
#infile = '/home/suntans/Share/ARCHub/FIELDTRIPS/KISMME2017RECOVERY/Data/RDIADCP/LongRanger24613/LR75_000.000'

#infile = 'Data/RDIADCP/Monitor20089/0089_000.000'

#adcp = ADCP_io(infile, rotate=True, config_update={})
#A = api(infile)
#print A._pos
#for ff in A.cfg.keys():
#    print ff, A.cfg[ff]
##with api(infile) as A:
#adcp = A.load_data()
#print A._pos, A.f,
##A.f.close() # close the open files
##A.f.f.close()
#
##infile = 'Data/RDIADCP/LongRanger24613/LR75_000.000'
#infile2 = 'Data/RDIADCP/16752/6752_000.000'
#
##adcp = ADCP_io(infile, rotate=True, config_update={})
#A = api(infile2)
#for ff in A.cfg.keys():
#    print ff, A.cfg[ff]
#
#adcp = A.load_data()
#print B._pos
