"""
Test the NMEA livestream on the solander

Use program "NPort Windows Driver Manager" to connect the NMEA stream on board the solander
"""

import serial
import pynmea2
import pdb
import time

def read_serial(filename, baudrate=9600):
    com = None
    reader = pynmea2.NMEAStreamReader()
    while 1:
        if com is None:
            try:
                com = serial.Serial(filename,baudrate, timeout=0.0,
			parity=serial.PARITY_EVEN, rtscts=1)
            except serial.SerialException:
                print('could not connect to %s' % filename)
                time.sleep(5.0)	
	        continue

        data = com.read(16)
	#data = com.read(2)
	for msg in reader.next(data):
	    #try: 
	    #print(msg)
	    #time.sleep(1.0)
	    #print dir(msg)
	    #print msg.data, msg.fields

	    ####
	    # Dump text
	    lon=None
	    lat=None
	    time=None
	    try:
	        lon = msg.longitude
	    except:
	        lon=None
            try:
	        lat = msg.latitude
	    except:
	        lat=None
            try:
	        time = msg.datetime
	    except:
	        time=None

	    #if 'latitude' in msg.__dict__.keys():
	    #    lon = msg.latitude
	    #if 'datestamp' in msg.__dict__.keys():
	    #    time = msg.datestamp
            
            #time is not None and l
	    #if lon is not None and lat is not None:
	    outfile = 'DATA/NMEA_latest.csv'
	    if time is not None and lon is not None:
	        print time,lon,lat
		f = open(outfile,'w')
		f.write('time,X,Y\n')
		f.write('%s,%s,%s'%(time,lon,lat))
		f.close()
	    else:
	    	#print ''
	        time=time# do nothing...

	    #    

            ##except:
	    #	print 'Failed to read stream...'
	    #print msg.datestamp, msg.longitude, msg.latitude

#streamreader = pynmea2.NMEAStreamReader()
#while 1:
#    data = input.read()
#    for msg in streamreader.next(data):
#        print msg

# Open up the serial port
#print 'Opening serial port...'
#ser = serial.Serial('COM2', 9600, timeout=0,
#    parity=serial.PARITY_EVEN, rtscts=1)

#print 'Done'

# Read the serial port
read_serial('COM2')

