"""
Try out the ad2cp reader
"""

from nortek import files, structures

inpath = r'C:\Projects\ARCHub\FIELDTRIPS\KISSME2017BROWSEBASIN\Instruments\Signature_Nortek\TestData\test_20170313\100460'
infile = 'S100460A002_kissme_test.ad2cp'

print 'Reading file...'

sig = files.AD2CP(filepath='%s/%s'%(inpath, infile))
print 'done'
