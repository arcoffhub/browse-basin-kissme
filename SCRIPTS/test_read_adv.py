"""
Use the dolfyn package to read a Nortek ADV file

See instructions here:
    http://lkilcher.github.io/dolfyn/usage.html#id3

# To use this module, import it:
from dolfyn.adv import api as adv

# Then read a file containing adv data:
dat = adv.read_nortek('../../../data/vector_data01.VEC')

# Then clean the file using the Goring+Nikora method:
adv.clean.GN2002(dat)

# Then rotate that data from the instrument to earth frame:
adv.rotate.inst2earth(dat)

# Then rotate it into a 'principal axes frame':
adv.rotate.earth2principal(dat)

# Define an averaging object, and create an 'averaged' data set:
binner = adv.TurbBinner(n_bin=40000, fs=dat.fs, n_fft=4096)
dat_bin = binner(dat)

# At any point you can save the data:
dat_bin.save('adv_data_rotated2principal.h5')

# And reload the data:
dat_bin_copy = adv.load('adv_data_rotated2principal.h5')
"""

from mydolfyn.adv import api as adv
import matplotlib.pyplot as plt

#####

advfile = 'Data/NortekADV/Vector-6017/K601703.vec'
advfile = 'Data/NortekADV/Vector-6017/6017_all.vec'
#advfile = 'Data/NortekADV/Vector-6019/601904.vec'
#advfile = 'Data/NortekADV/Vector-6015/K601502.vec'

# Set to none to read all
#npings = 50000
npings = None
#####

dat = adv.read_nortek(advfile, debug=False, npings=npings)

print dir(dat)

# This is a list of 
print dat.groups

# Then clean the file using the Goring+Nikora method:
adv.clean.GN2002(dat)

# Then rotate that data from the instrument to earth frame:
adv.rotate.inst2earth(dat)

plt.figure()
plt.plot(dat.u_earth[::640])
plt.plot(dat.v_earth[::640], 'r')
plt.plot(dat.w[::640], 'k')
plt.show()


