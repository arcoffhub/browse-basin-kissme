"""
Test a time domain filter
"""

import mycurrents.oceanmooring as om

from soda.utils.timeseries import timeseries
from soda.utils.othertime import datetime64todatetime, datetimetodatetime64
from soda.utils.mysignal import filt_gaps

import xray
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime, timedelta

import pdb

def filt_gaps_butter(y, mask, dt, tau):
    """
    Differential equation based butterworth low pass filter

    See here:
        https://www.osti.gov/servlets/purl/7235596

    Allows for gaps in the data identified with the mask vector
    
    Returns a filtered vector with no gaps
    """
    
    n = y.size
    yf = np.zeros_like(y)

    if ~mask[0]:
        yf[0] = y[0]

    y1_n = yf[0]
    y1_n1 = 0
    y2_n = 0
    y2_n1 = 0


    fn = 1.0/tau
    alpha = 4*np.pi**2*fn**2
    dt /= (np.pi*2)
    #alpha = np.pi*fn**2

    for ii in range(1,n):
        #if ii == 10:
        #    break
        if ~mask[ii]:
            ynext = y[ii]
        else:
            ynext = yf[ii-1]

        # Explicit solve (unstable)
        #y2_n1 = y2_n + dt*alpha*(ynext - y1_n)
        #y1_n1 = y1_n + dt*y2_n
        y2_n1 = y2_n - dt*alpha*(y1_n-ynext)
        y1_n1 = y1_n + dt*y2_n



        # Implicit solve (stable)
        #y1_n1 = (y1_n + dt*y2_n - dt**2*alpha*ynext) / (1-dt*alpha)
        #y2_n1 = (y1_n1 - y1_n) / dt
        #y2_n1 = y2_n + dt*alpha*(y1_n1-ynext)
        #y1_n1 = (y1_n + dt*y2_n + dt**2*alpha*ynext) / (1+dt*alpha)
        #y2_n1 = y2_n + dt*alpha*(ynext - y1_n1)

        yf[ii] = y1_n1
        y1_n = y1_n1
        y2_n = y2_n1
        print(ii, y1_n, y2_n)
        if y1_n>35:
            break

    return yf

def filt_gaps_butter_old(y, mask, dt, tau):
    """
    Differential equation based butterworth low pass filter

    See here:
        https://www.osti.gov/servlets/purl/7235596

    Allows for gaps in the data identified with the mask vector
    
    Returns a filtered vector with no gaps
    """
    
    n = y.size
    yf = np.zeros_like(y)
    yi = np.zeros_like(y)
    yi_m1 = np.zeros_like(y)

    if ~mask[0]:
        yf[0:2] = y[0:2]

    fn = 1.0/tau
    alpha = 4*np.pi**2*fn**2
    cff = alpha*dt**2.

    for ii in range(1,n-1):
        if ~mask[ii+1]:
            #yf[ii+1] = 2*yi[ii] - yi_m1[ii] - cff*(yf[ii] - y[ii+1])

            ## Update the last two entries
            #yi[ii+1] = yf[ii+1]
            #yi_m1[ii+1] = yi[ii]

            yf[ii+1] = 2*yf[ii] - yf[ii-1] + cff*(yf[ii] - y[ii+1])


        else: # Masked value
            #yf[ii+1] = yf[ii] + cff*(ytmp - yf[ii])
            yf[ii+1 ] = yf[ii]

    return yf
 
#### 
# Load some data
#adcpfile = 'Data/NetCDF/KISSME2017_RDI_QAQC.nc'
#
#
#site = 'SP'
#if site=='WP':
#    group1 = 'WP250_RDI_150_kHz_Quartermaster_11795'
#    group2 = 'WP250_RDI_300_kHz_Sentinel_20092'
#    Tgroup = 'WP250'
#elif site=='SP':
#    group1 = 'SP250_RDI75_kHz_Longranger_24613'
#    group2 = None
#    Tgroup = 'SP250'
#elif site=='NP':
#    group1 = 'NP250_RDI_150_kHz_Quartermaster_16856'
#    group2 = 'NP250_RDI_300_kHz_Monitor_20089'
#    Tgroup = 'NP250'
#
#U1 = om.from_netcdf(adcpfile, 'u', group=group1)
#
#u = U1.y[10,:]
#ncfile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Gridded_Mooring_TP_60sec_linear.nc'
#ncgroup = 'SP250'
#
#varname = 'temperature'
## Load some data
#U1 = om.from_netcdf(ncfile, varname, group=ncgroup)
#U1 = U1.clip(datetime(2017,4,2), datetime(2017,4,28))
#
#u = U1.interp_z(60.)
##u[u==0.] = np.nan
#u.mask = np.isnan(u)
#u.mask = u==0.

# Create a signal
omega1 = 2*np.pi/(12.42*3600)
omega2 = 2*np.pi/(14*86400)

dt = 3600.
t = np.arange(0,30*86000,dt)
time = [datetime(1990,1,1) + timedelta(seconds=ii) for ii in t]
#u = om.OceanMooring(time, 1*np.cos(omega1*t) + 2.*np.cos(omega2*t), 0.)
u = np.ma.MaskedArray(27+1*np.cos(omega1*t) + 2.*np.cos(omega2*t),\
    mask=np.zeros((t.shape[0],),dtype=np.bool))



######
uf_0 = filt_gaps(u.data, u.mask, dt, 34*3600./3)
uf_1 = filt_gaps(uf_0, np.zeros_like(u.mask), dt, 34*3600./3)
uf = filt_gaps(uf_1, np.zeros_like(u.mask), dt, 34*3600./3)

#uf = filt_gaps(u.data, u.mask, dt, 34*3600.)

#uf_r = filt_gaps(uf_l[::-1], u.mask, U1.dt, 33*3600.)
#uf = uf_r[::-1]

uf34 = filt_gaps(u.data, u.mask, dt, 34*3600., order=6)
#uf = filt_gaps_butter(u.data, u.mask, dt, 34*3600.)
#ulow = filt_gaps(u.data, u.mask, U1.dt, 72*3600.)
ulow = om.OceanMooring(time, u, 0.).filt(34*3600, btype='low',order=3, lfilter=True)
#ufb = filt_uneven(u.data[::-1], u.mask[::-1], U1.dt, 30*3600.)

plt.plot(t, u, 'b', lw=0.2)
plt.plot(t, uf, 'r', lw=0.5)
plt.plot(t, uf34, 'g', lw=0.5)
plt.plot(t, ulow, 'k', lw=0.5)
#plt.plot(U1.t, ufb[::-1], 'k', lw=0.5)
plt.ylim(24,32)
plt.show()


