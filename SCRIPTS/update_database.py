# -*- coding: utf-8 -*-
"""
Update or create a database with point observation netcdf files
"""

from soda.dataio import netcdfio

####
# Inputs
#create = True
create = False

#basedir = '/home/suntans/Share/ARCHub/FIELDTRIPS/KISMME2017RECOVERY'
basedir = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme'

# This could be an existing database
dbfile = '%s/KISSME2017_Obs.db'%basedir
   
ncfiles = [\
    #'%s/Data/NetCDF/KISSME_VemcoData.nc'%basedir,\
    #'%s/Data/NetCDF/KISSME_SBE39Data.nc'%basedir,\
    #'%s/Data/NetCDF/KISSME_SBE37Data.nc'%basedir,\
    #'%s/Data/NetCDF/KISSME_SBE56Data.nc'%basedir,\
    #'%s/Data/NetCDF/KISSME2017_RDIADCPData_float64.nc'%basedir,\
    '%s/Data/NetCDF/KISSME_Gridded_Mooring_TP_60sec.nc'%basedir,\
    ]

# nctype = 3 is my format
nctype = 3
####

if create:
    print 'Creating database: %s'%dbfile
    netcdfio.createObsDB(dbfile)
    
for nc in ncfiles:
    print 'Inserting metadata from: %s'%nc    
    netcdfio.netcdfObs2DB(nc,dbfile, nctype=nctype)

print 'Done.'
